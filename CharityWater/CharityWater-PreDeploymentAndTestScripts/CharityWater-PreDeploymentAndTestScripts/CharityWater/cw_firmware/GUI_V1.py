# https://stackoverflow.com/questions/44404349/pyqt-showing-video-stream-from-opencv
# pip install qimage2ndarray
# pip install PySide2


# ON PI
# !!!!!!! DO NOT USE VIRTUAL ENVIRONMENT CV FOR THIS CODE !!!! 
# Need to install PySide2 and open cv outside the virtual environment 
# PySide2: sudo apt-get install python3-pyside2.qt3dcore python3-pyside2.qt3dinput python3-pyside2.qt3dlogic python3-pyside2.qt3drender python3-pyside2.qtcharts python3-pyside2.qtconcurrent python3-pyside2.qtcore python3-pyside2.qtgui python3-pyside2.qthelp python3-pyside2.qtlocation python3-pyside2.qtmultimedia python3-pyside2.qtmultimediawidgets python3-pyside2.qtnetwork python3-pyside2.qtopengl python3-pyside2.qtpositioning python3-pyside2.qtprintsupport python3-pyside2.qtqml python3-pyside2.qtquick python3-pyside2.qtquickwidgets python3-pyside2.qtscript python3-pyside2.qtscripttools python3-pyside2.qtsensors python3-pyside2.qtsql python3-pyside2.qtsvg python3-pyside2.qttest python3-pyside2.qttexttospeech python3-pyside2.qtuitools python3-pyside2.qtwebchannel python3-pyside2.qtwebsockets python3-pyside2.qtwidgets python3-pyside2.qtx11extras python3-pyside2.qtxml python3-pyside2.qtxmlpatterns python3-pyside2uic
# OpenCV: pip install opencv-contrib-python==4.1.0.25

# ON MAC 
# Need to run in virtual environment CV 
# Need to run in ternminal not VisualStudioCode or you want be able to access the camera



# from PyQt5 import QtCore
from PySide2.QtCore import *
from PySide2.QtGui import *
import cv2 # OpenCV
import qimage2ndarray # for a memory leak,see gist
import sys # for exiting
from PySide2.QtWidgets import *


from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtCore import QSize

import pandas
import serial
import time





# Determining Paths for Menu Images______________
piOrLaptop = 1 # 0 = Laptop , 1 = Pi !!!!!!!!!!!!!!!!! Crutial to change to get to work on laptop or pi! 


if(piOrLaptop == 0):
    # Paths to Images for use on computer 
# path_W = "images/W.png"

# Menu Icons 
    path_menuW = "images/menuW.png"
    path_settingsW = "images/settingsW.png"
    path_crossHairW = "images/crossHairW.png"
    path_ppModeW = "images/ppModeW.png"
    path_ncModeW = "images/ncModeW.png"
    path_eModeW = "images/eModeW.png"

    path_crossHairR = "images/crossHairR.png"
    path_crossHairBY = "images/crossHairBY.png"

    path_menuBlue = "images/menuBlue.png"
    path_settingsBlue = "images/settingsBlue.png"
    path_crossHairBlue = "images/crossHairBlue.png"
    path_ppModeBlue = "images/ppModeBlue.png"
    path_ncModeBlue = "images/ncModeBlue.png"
    path_eModeBlue = "images/eModeBlue.png"

    path_closeW = "images/closeW.png"
    path_closeR = "images/closeR.png"

    path_saveAndCloseW = "images/saveAndCloseW.png"
    path_saveAndCloseBlue = "images/saveAndCloseBlue.png"
    path_saveAndCloseGreen = "images/saveAndCloseGreen.png"


# Settings Icons 
    path_ppSettingsW = "images/ppSettingsW.png"
    path_eSettingsW = "images/eSettingsW.png"
    path_crossHairSettingsW = "images/crossHairSettingsW.png"
    path_ppSettingsBlue = "images/ppSettingsBlue.png"
    path_eSettingsBlue = "images/eSettingsBlue.png"
    path_crossHairSettingsBlue = "images/crossHairSettingsBlue.png"
    


# Cross Hair Settings Icons 
    path_thicknessW = "images/thicknessW.png"
    path_colourW = "images/colourW.png"
    path_moveW = "images/moveW.png"

    path_thicknessBlue = "images/thicknessBlue.png"
    path_colourBlue = "images/colourBlue.png"
    path_moveBlue = "images/moveBlue.png"
    path_crossHairSwitch1 = "images/crossHairSwitch1.png"
    path_crossHairSwitch2 = "images/crossHairSwitch2.png"

    path_scaleW = "images/scaleW.png"
    path_thicknessNewW = "images/thicknessNewW.png"
    path_growingCrossW = "images/growingCrossW.png"
    path_scaleBlue = "images/scaleBlue.png"
    path_thicknessNeBlueBlue = "images/thicknessNewBlue.png"
    path_growingCrossBlue = "images/growingCrossBlue.png"

# Arrow Icons 
    path_upSmallW = "images/upSmallW.png"
    path_downSmallW = "images/downSmallW.png"
    path_leftSmallW = "images/leftSmallW.png"
    path_rightSmallW = "images/rightSmallW.png"
    path_upBigW = "images/upBigW.png"
    path_downBigW = "images/downBigW.png"
    path_leftBigW = "images/leftBigW.png"
    path_rightBigW = "images/rightBigW.png"
    
   
# Increment Icons 
    path_minus1W = "images/minus1W.png"
    path_minus1B = "images/minus1B.png"
    path_plus1W = "images/plus1W.png"
    path_plus1B = "images/plus1B.png"
    path_minus10W = "images/minus10W.png"
    path_minus10B = "images/minus10B.png"
    path_plus10W = "images/plus10W.png"
    path_plus10B = "images/plus10B.png"

# Word Icons 
    path_backWordW = "images/backWordW.png"
    path_closeWordW = "images/closeWordW.png"
    path_settingsWordW = "images/settingsWordW.png"

# Colour Icons
    path_white = "images/white.png"
    path_red = "images/red.png"
    path_yellow = "images/yellow.png"
    path_green = "images/green.png"
    path_blue = "images/blue.png"
    path_purple = "images/purple.png"
    path_black = "images/black.png"

# Extrusion Sub-Modes
    path_continuousModeW = "images/continuousModeW.png"
    path_continuousSettingsW = "images/continuousSettingsW.png"

    path_smallPadModeW = "images/smallPadModeW.png"
    path_smallPadSettingsW = "images/smallPadSettingsW.png"

    path_largePadModeW = "images/largePadModeW.png"
    path_largePadSettingsW = "images/largePadSettingsW.png"

    path_continuousModeBlue = "images/continuousModeBlue.png"
    path_continuousSettingsBlue = "images/continuousSettingsBlue.png"

    path_smallPadModeBlue = "images/smallPadModeBlue.png"
    path_smallPadSettingsBlue = "images/smallPadSettingsBlue.png"

    path_largePadModeBlue = "images/largePadModeBlue.png"
    path_largePadSettingsBlue = "images/largePadSettingsBlue.png"

    path_blank = "images/blank.png"
 

elif (piOrLaptop == 1):
    # Paths to Images for use on Pi
        # /home/pi/liam/piCode/mainApplication/

# Menu Icons 
    path_menuW = "/home/pi/liam/piCode/mainApplication/images/menuW.png"
    path_settingsW = "/home/pi/liam/piCode/mainApplication/images/settingsW.png"
    path_crossHairW = "/home/pi/liam/piCode/mainApplication/images/crossHairW.png"
    path_ppModeW = "/home/pi/liam/piCode/mainApplication/images/ppModeW.png"
    path_ncModeW = "/home/pi/liam/piCode/mainApplication/images/ncModeW.png"
    path_eModeW = "/home/pi/liam/piCode/mainApplication/images/eModeW.png"

    path_crossHairR = "/home/pi/liam/piCode/mainApplication/images/crossHairR.png"
    path_crossHairBY = "/home/pi/liam/piCode/mainApplication/images/crossHairBY.png"

    path_menuBlue = "/home/pi/liam/piCode/mainApplication/images/menuBlue.png"
    path_settingsBlue = "/home/pi/liam/piCode/mainApplication/images/settingsBlue.png"
    path_crossHairBlue = "/home/pi/liam/piCode/mainApplication/images/crossHairBlue.png"
    path_ppModeBlue = "/home/pi/liam/piCode/mainApplication/images/ppModeBlue.png"
    path_ncModeBlue = "/home/pi/liam/piCode/mainApplication/images/ncModeBlue.png"
    path_eModeBlue = "/home/pi/liam/piCode/mainApplication/images/eModeBlue.png"

    path_closeW = "/home/pi/liam/piCode/mainApplication/images/closeW.png"
    path_closeR = "/home/pi/liam/piCode/mainApplication/images/closeR.png"

    path_saveAndCloseW = "/home/pi/liam/piCode/mainApplication/images/saveAndCloseW.png"
    path_saveAndCloseBlue = "/home/pi/liam/piCode/mainApplication/images/saveAndCloseBlue.png"
    path_saveAndCloseGreen = "/home/pi/liam/piCode/mainApplication/images/saveAndCloseGreen.png"


# Settings Icons 
    path_ppSettingsW = "/home/pi/liam/piCode/mainApplication/images/ppSettingsW.png"
    path_eSettingsW = "/home/pi/liam/piCode/mainApplication/images/eSettingsW.png"
    path_crossHairSettingsW = "/home/pi/liam/piCode/mainApplication/images/crossHairSettingsW.png"
    path_ppSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/ppSettingsBlue.png"
    path_eSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/eSettingsBlue.png"
    path_crossHairSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/crossHairSettingsBlue.png"



# Cross Hair Settings Icons 
    path_thicknessW = "/home/pi/liam/piCode/mainApplication/images/thicknessW.png"
    path_colourW = "/home/pi/liam/piCode/mainApplication/images/colourW.png"
    path_moveW = "/home/pi/liam/piCode/mainApplication/images/moveW.png"

    path_thicknessBlue = "/home/pi/liam/piCode/mainApplication/images/thicknessBlue.png"
    path_colourBlue = "/home/pi/liam/piCode/mainApplication/images/colourBlue.png"
    path_moveBlue = "/home/pi/liam/piCode/mainApplication/images/moveBlue.png"
    path_crossHairSwitch1 = "/home/pi/liam/piCode/mainApplication/images/crossHairSwitch1.png"
    path_crossHairSwitch2 = "/home/pi/liam/piCode/mainApplication/images/crossHairSwitch2.png"

    path_scaleW = "/home/pi/liam/piCode/mainApplication/images/scaleW.png"
    path_thicknessNewW = "/home/pi/liam/piCode/mainApplication/images/thicknessNewW.png"
    path_growingCrossW = "/home/pi/liam/piCode/mainApplication/images/growingCrossW.png"
    path_scaleBlue = "/home/pi/liam/piCode/mainApplication/images/scaleBlue.png"
    path_thicknessNeBlueBlue = "/home/pi/liam/piCode/mainApplication/images/thicknessNewBlue.png"
    path_growingCrossBlue = "/home/pi/liam/piCode/mainApplication/images/growingCrossBlue.png"

# Arrow Icons 
    path_upSmallW = "/home/pi/liam/piCode/mainApplication/images/upSmallW.png"
    path_downSmallW = "/home/pi/liam/piCode/mainApplication/images/downSmallW.png"
    path_leftSmallW = "/home/pi/liam/piCode/mainApplication/images/leftSmallW.png"
    path_rightSmallW = "/home/pi/liam/piCode/mainApplication/images/rightSmallW.png"
    path_upBigW = "/home/pi/liam/piCode/mainApplication/images/upBigW.png"
    path_downBigW = "/home/pi/liam/piCode/mainApplication/images/downBigW.png"
    path_leftBigW = "/home/pi/liam/piCode/mainApplication/images/leftBigW.png"
    path_rightBigW = "/home/pi/liam/piCode/mainApplication/images/rightBigW.png"
    
   
# Increment Icons 
    path_minus1W = "/home/pi/liam/piCode/mainApplication/images/minus1W.png"
    path_minus1B = "/home/pi/liam/piCode/mainApplication/images/minus1B.png"
    path_plus1W = "/home/pi/liam/piCode/mainApplication/images/plus1W.png"
    path_plus1B = "/home/pi/liam/piCode/mainApplication/images/plus1B.png"
    path_minus10W = "/home/pi/liam/piCode/mainApplication/images/minus10W.png"
    path_minus10B = "/home/pi/liam/piCode/mainApplication/images/minus10B.png"
    path_plus10W = "/home/pi/liam/piCode/mainApplication/images/plus10W.png"
    path_plus10B = "/home/pi/liam/piCode/mainApplication/images/plus10B.png"

# Word Icons 
    path_backWordW = "/home/pi/liam/piCode/mainApplication/images/backWordW.png"
    path_closeWordW = "/home/pi/liam/piCode/mainApplication/images/closeWordW.png"
    path_settingsWordW = "/home/pi/liam/piCode/mainApplication/images/settingsWordW.png"

# Colour Icons
    path_white = "/home/pi/liam/piCode/mainApplication/images/white.png"
    path_red = "/home/pi/liam/piCode/mainApplication/images/red.png"
    path_yellow = "/home/pi/liam/piCode/mainApplication/images/yellow.png"
    path_green = "/home/pi/liam/piCode/mainApplication/images/green.png"
    path_blue = "/home/pi/liam/piCode/mainApplication/images/blue.png"
    path_purple = "/home/pi/liam/piCode/mainApplication/images/purple.png"
    path_black = "/home/pi/liam/piCode/mainApplication/images/black.png"

# Extrusion Sub-Modes
    path_continuousModeW = "/home/pi/liam/piCode/mainApplication/images/continuousModeW.png"
    path_continuousSettingsW = "/home/pi/liam/piCode/mainApplication/images/continuousSettingsW.png"

    path_smallPadModeW = "/home/pi/liam/piCode/mainApplication/images/smallPadModeW.png"
    path_smallPadSettingsW = "/home/pi/liam/piCode/mainApplication/images/smallPadSettingsW.png"

    path_largePadModeW = "/home/pi/liam/piCode/mainApplication/images/largePadModeW.png"
    path_largePadSettingsW = "/home/pi/liam/piCode/mainApplication/images/largePadSettingsW.png"

    path_continuousModeBlue = "/home/pi/liam/piCode/mainApplication/images/continuousModeBlue.png"
    path_continuousSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/continuousSettingsBlue.png"

    path_smallPadModeBlue = "/home/pi/liam/piCode/mainApplication/images/smallPadModeBlue.png"
    path_smallPadSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/smallPadSettingsBlue.png"

    path_largePadModeBlue = "/home/pi/liam/piCode/mainApplication/images/largePadModeBlue.png"
    path_largePadSettingsBlue = "/home/pi/liam/piCode/mainApplication/images/largePadSettingsBlue.png"
# Blank 
    path_blank = "/home/pi/liam/piCode/mainApplication/images/blank.png"
# _______________________________________________


# Fixed Parameters ______________________________
menuIconWidth = 100
menuIconHeight = 100
menuXdistance =0

subMenuIndentX = 180
subMenuIndentY = 50


arrowIconSize = 80
colourIconSize = 80
crossHairSizeIconSize =80

crossMoveSmall = 1
crossMoveBig = 10

crossSizeIncrement = 10 
crossThicknessIncrement = 1

white = (244,244,244)
red = (227,6,19)
yellow = ( 237, 255, 0)
green = ( 0,150 ,64 )
blue = (0,159  ,277 )
purple = ( 102,36,131 )
black= (0,0,0)

windowTextColour = QColor(244,244,244)
windowBackgroundColour = QColor(0,0,0) 


textSize_extrusionSettingsMenu = 20

minumumCrossSize = 10 
maximimCrossSize = 200

minumumCrossThickness = 1
maximumCrossThickness = 15


# _______________________________________________


# User Variable Parameters ______________________
crossHairMode = 0 

activeCross = 1 
cross1centerX = 500
cross1centerY = 500
cross1Size = 200
cross1Thickness = 10 

cross2centerX = 400
cross2centerY = 400
cross2Size = 200
cross2Thickness = 10 

cross1ColourString = "red"
cross2ColourString = "red"

cross1Colour = (244,0,0)
cross2Colour = (244,0,0)

# To arduino parameters 
# Mode Params 
mode = 2            # Mode 0: eMode. 1: ppMode , 2:Nc Mode 
extrusionMode = 0   # Extrusion mode: 0: continuous 1: smallPad (dot1) , 2: largePad (dot2) 
# Live Params 
encoderTicksPerJump = 30
speedPP = 100
flipAngle = 90 
continuousExtrusionSpeed = 100
continuousRetractionSteps = 10
dot1ExtrusionSpeed = 100
dot1ExtrusionSteps = 30
dot1RetractionSteps = 50
dot2ExtrusionSpeed = 100
dot2ExtrusionSteps = 30
dot2RetractionSteps = 50


# Saved Parameters
savedEncoderTicksPerJump = 30
savedSpeedPP = 100
savedFlipAngle = 90 
savedContinuousExtrusionSpeed = 100
savedContinuousRetractionSteps = 10
savedDot1ExtrusionSpeed = 100
savedDot1ExtrusionSteps = 30
savedDot1RetractionSteps = 50
savedDot2ExtrusionSpeed = 100
savedDot2ExtrusionSteps = 30
savedDot2RetractionSteps = 50

# Not in use __________

extrusionSpeed = 100
# _______________________________________________

def loadAllSettings():
    # https://realpython.com/python-csv/
    global df
    global mode
    global extrusionMode

    global encoderTicksPerJump 
    global speedPP 
    global flipAngle 
    global continuousExtrusionSpeed 
    global continuousRetractionSteps 
    global dot1ExtrusionSpeed 
    global dot1ExtrusionSteps 
    global dot1RetractionSteps 
    global dot2ExtrusionSpeed 
    global dot2ExtrusionSteps 
    global dot2RetractionSteps 

    global savedEncoderTicksPerJump 
    global savedSpeedPP 
    global savedFlipAngle 
    global savedContinuousExtrusionSpeed 
    global savedContinuousRetractionSteps 
    global savedDot1ExtrusionSpeed 
    global savedDot1ExtrusionSteps 
    global savedDot1RetractionSteps 
    global savedDot2ExtrusionSpeed 
    global savedDot2ExtrusionSteps 
    global savedDot2RetractionSteps 
    global crossHairMode

    global activeCross #not in use

    global cross1centerX
    global cross1centerY 
    global cross1Size
    global cross1Thickness 
    global cross2centerX 
    global cross2centerY 
    global cross2Size 
    global cross2Thickness 

    global cross1ColourString
    global cross2ColourString 

    global cross1Colour
    global cross2Colour


    if(piOrLaptop == 0):
        df = pandas.read_csv('savedConfig.csv', index_col='param')
    elif(piOrLaptop == 1):
        df = pandas.read_csv('/home/pi/liam/piCode/mainApplication/savedConfig.csv', index_col='param')
 
    mode = int(df['value']['savedMode'])
    extrusionMode = int(df['value']['savedExtrusionMode'])
    savedSpeedPP = int(df['value']['savedSpeedPP'])
    savedFlipAngle = int(df['value']['savedFlipAngle'])
    savedEncoderTicksPerJump = int(df['value']['savedEncoderTicksPerJump'])
    savedContinuousExtrusionSpeed = int(df['value']['savedContinuousExtrusionSpeed'])
    savedContinuousRetractionSteps = int(df['value']['savedContinuousRetractionSteps'])
    savedDot1ExtrusionSpeed = int(df['value']['savedDot1ExtrusionSpeed'])
    savedDot1ExtrusionSteps = int(df['value']['savedDot1ExtrusionSteps'])
    savedDot1RetractionSteps = int(df['value']['savedDot1RetractionSteps'])
    savedDot2ExtrusionSpeed = int(df['value']['savedDot2ExtrusionSpeed'])
    savedDot2ExtrusionSteps = int(df['value']['savedDot2ExtrusionSteps'])
    savedDot2RetractionSteps = int(df['value']['savedDot2RetractionSteps'])

    encoderTicksPerJump = savedEncoderTicksPerJump
    speedPP = savedSpeedPP
    flipAngle = savedFlipAngle
    continuousExtrusionSpeed = savedContinuousExtrusionSpeed
    continuousRetractionSteps = savedContinuousRetractionSteps
    dot1ExtrusionSpeed = savedDot1ExtrusionSpeed
    dot1ExtrusionSteps = savedDot1ExtrusionSteps
    dot1RetractionSteps = savedDot1RetractionSteps
    dot2ExtrusionSpeed = savedDot2ExtrusionSpeed
    dot2ExtrusionSteps = savedDot2ExtrusionSteps
    dot2RetractionSteps = savedDot2RetractionSteps

    crossHairMode = int(df['value']['crossHairMode'])
    cross1centerX = int(df['value']['cross1centerX'])
    cross1centerY = int(df['value']['cross1centerY'])
    cross1Size = int(df['value']['cross1Size'])
    cross1Thickness  = int(df['value']['cross1Thickness'])
    cross2centerX = int(df['value']['cross2centerX'])
    cross2centerY = int(df['value']['cross2centerY'])
    cross2Size = int(df['value']['cross2Size'])
    cross2Thickness = int(df['value']['cross2Thickness'])

    cross1ColourString = str(df['value']['cross1ColourString'])
    cross2ColourString = str(df['value']['cross2ColourString'])

    if(cross1ColourString == "red"):
        cross1Colour = red 
    elif(cross1ColourString == "yellow"):
        cross1Colour = yellow 
    elif(cross1ColourString == "green"):
        cross1Colour = green 
    elif(cross1ColourString == "blue"):
        cross1Colour = blue 
    elif(cross1ColourString == "purple"):
        cross1Colour = purple 
    elif(cross1ColourString == "black"):
        cross1Colour = black 
    elif(cross1ColourString == "white"):
        cross1Colour = white 
    
    if(cross2ColourString == "red"):
        cross2Colour = red 
    elif(cross2ColourString == "yellow"):
        cross2Colour = yellow 
    elif(cross2ColourString == "green"):
        cross2Colour = green 
    elif(cross2ColourString == "blue"):
        cross2Colour = blue 
    elif(cross2ColourString == "purple"):
        cross2Colour = purple 
    elif(cross2ColourString == "black"):
        cross2Colour = black 
    elif(cross2ColourString == "white"):
        cross2Colour = white 


def saveAllSettings():
    # https://realpython.com/python-csv/
    global df

    global mode
    global extrusionMode
    global savedEncoderTicksPerJump 
    global savedSpeedPP 
    global savedFlipAngle 
    global savedContinuousExtrusionSpeed 
    global savedContinuousRetractionSteps 
    global savedDot1ExtrusionSpeed 
    global savedDot1ExtrusionSteps 
    global savedDot1RetractionSteps 
    global savedDot2ExtrusionSpeed 
    global savedDot2ExtrusionSteps 
    global savedDot2RetractionSteps 

    global activeCross #not in use

    global cross1centerX
    global cross1centerY 
    global cross1Size
    global cross1Thickness 
    global cross2centerX 
    global cross2centerY 
    global cross2Size 
    global cross2Thickness 

    global cross1ColourString
    global cross2ColourString 


    df['value']['savedMode'] = mode
    df['value']['savedExtrusionMode'] = extrusionMode
    df['value']['savedEncoderTicksPerJump'] = savedEncoderTicksPerJump
    df['value']['savedSpeedPP'] = savedSpeedPP
    df['value']['savedFlipAngle'] = savedFlipAngle
    df['value']['savedContinuousExtrusionSpeed'] = savedContinuousExtrusionSpeed
    df['value']['savedContinuousRetractionSteps'] = savedContinuousRetractionSteps
    df['value']['savedDot1ExtrusionSpeed'] = savedDot1ExtrusionSpeed
    df['value']['savedDot1ExtrusionSteps'] = savedDot1ExtrusionSteps
    df['value']['savedDot1RetractionSteps'] = savedDot1RetractionSteps
    df['value']['savedDot2ExtrusionSpeed'] = savedDot2ExtrusionSpeed
    df['value']['savedDot2ExtrusionSteps'] = savedDot2ExtrusionSteps
    df['value']['savedDot2RetractionSteps'] = savedDot2RetractionSteps

    df['value']['crossHairMode'] = crossHairMode
    df['value']['cross1centerX'] = cross1centerX
    df['value']['cross1centerY'] = cross1centerY
    df['value']['cross1Size'] = cross1Size
    df['value']['cross1Thickness'] = cross1Thickness 
    df['value']['cross2centerX'] = cross2centerX 
    df['value']['cross2centerY'] = cross2centerY 
    df['value']['cross2Size'] = cross2Size
    df['value']['cross2Thickness'] = cross2Thickness
    df['value']['cross1ColourString'] = cross1ColourString
    df['value']['cross2ColourString'] = cross2ColourString

    print(df)

    if(piOrLaptop == 0):
        df.to_csv('savedConfig.csv')
    elif(piOrLaptop == 1):
        df.to_csv('/home/pi/liam/piCode/mainApplication/savedConfig.csv')

    df.to_csv('savedConfig.csv')

    writeToArduino()


# Write To Arduino 
def writeToArduino():
    global mode
    global extrusionMode
    global savedEncoderTicksPerJump 
    global savedSpeedPP 
    global savedFlipAngle 
    global savedContinuousExtrusionSpeed 
    global savedContinuousRetractionSteps 
    global savedDot1ExtrusionSpeed 
    global savedDot1ExtrusionSteps 
    global savedDot1RetractionSteps 
    global savedDot2ExtrusionSpeed 
    global savedDot2ExtrusionSteps 
    global savedDot2RetractionSteps 

    cmd1 = str(mode)         
    cmd2 = str(extrusionMode)  
    cmd3 = str(savedEncoderTicksPerJump)
    cmd4 = str(savedSpeedPP)
    cmd5 = str(savedFlipAngle)
    cmd6 = str(savedContinuousExtrusionSpeed)
    cmd7 = str(savedContinuousRetractionSteps)
    cmd8 = str(savedDot1ExtrusionSpeed)
    cmd9 = str(savedDot1ExtrusionSteps)
    cmd10 = str(savedDot1RetractionSteps)
    cmd11 = str(savedDot2ExtrusionSpeed)
    cmd12 = str(savedDot2ExtrusionSteps)
    cmd13 = str(savedDot2RetractionSteps)

    commandString = str(cmd1 +":"+ cmd2 +":"+ cmd3 +":"+ cmd4 +":"+ cmd5 +":"+ cmd6 +":"+ cmd7 +":"+ cmd8 +":"+ cmd9 +":"+ cmd10 +":"+ cmd11 +":"+ cmd12 +":"+ cmd13 + "\n")

    print(commandString )

    if(piOrLaptop == 1): # 1 is for pi code 
        if __name__ == '__main__':
            ser = serial.Serial('/dev/ttyS0', 9600, timeout=1)
            ser.flush()
            ser.write(commandString.encode())
            # while True:
            #     # ser.write(b"0:60:100:100:100:100\n")
            #     ser.write(commandString.encode())
            #     # line = ser.readline().decode('utf-8').rstrip()
            #     # print(line)
            #     time.sleep(10)

def displayFrame():
    ret, frame = cap.read()
    frame = cv2.cvtColor(frame, cv2.COLOR_BGR2RGB)

    if(crossHairMode == 1 ):
        cv2.line(frame,(cross1centerX-cross1Size,cross1centerY),(cross1centerX+cross1Size,cross1centerY),cross1Colour,cross1Thickness)
        cv2.line(frame,(cross1centerX,cross1centerY-cross1Size),(cross1centerX,cross1centerY+cross1Size),cross1Colour,cross1Thickness)
    elif(crossHairMode == 2 ):
        cv2.line(frame,(cross1centerX-cross1Size,cross1centerY),(cross1centerX+cross1Size,cross1centerY),cross1Colour,cross1Thickness)
        cv2.line(frame,(cross1centerX,cross1centerY-cross1Size),(cross1centerX,cross1centerY+cross1Size),cross1Colour,cross1Thickness)

        cv2.line(frame,(cross2centerX-cross2Size,cross2centerY),(cross2centerX+cross2Size,cross2centerY),cross2Colour,cross2Thickness)
        cv2.line(frame,(cross2centerX,cross2centerY-cross2Size),(cross2centerX,cross2centerY+cross2Size),cross2Colour,cross2Thickness)
    image = qimage2ndarray.array2qimage(frame)
    label.setPixmap(QPixmap.fromImage(image))
    
def createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour ):
    window = QWidget()
    if(isFrameless == 1 and isStaysOnTop == 0  ):
        window.setWindowFlags(Qt.FramelessWindowHint)
    elif(isFrameless == 0 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.WindowStaysOnTopHint)
    elif(isFrameless == 1 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)

    if(isTranslucent == 1  ):
        window.setAttribute(Qt.WA_TranslucentBackground)

    if(isMaximised == 1):
        window.showMaximized()
    elif(isMaximised== 0 ):
        qr = window.frameGeometry()
        window.move(qr.topLeft().x()+ xDistance, qr.topLeft().y()+ yDistance)
    
    # Change background colour 
    palette = window.palette()
    palette.setColor(QPalette.Window, backgroundColour)
    palette.setColor(QPalette.WindowText, textColour)
    window.setPalette(palette)
    window.setAutoFillBackground(True)

    if(useStyleSheet == 1 ):
        window.setStyleSheet(
            "QWidget" 
            "{" 
            "border: 100px white;"
            "border-radius: 10px;"
            "background-color: black"
            "}"
        )

    

    # # Change background colour 
    # palette = window.palette()
    # palette.setColor(QPalette.Window, QColor(0, 0, 0))
    # palette.setColor(QPalette.WindowText, QColor(240, 240, 240))
    # window.setPalette(palette)
    # window.setAutoFillBackground(True)

    return window

def createPictureButton(functionName, imagePathName, sizeX , sizeY):
    buttonName = QPushButton("")
    buttonName.clicked.connect(functionName)
    # Set image in Push Button
    pixmap = QPixmap(imagePathName)
    buttonName_icon = QIcon(pixmap)
    buttonName.setIcon(buttonName_icon)
    buttonName.setIconSize(QSize(sizeX,sizeY))
    buttonName.setFlat(1)
    buttonName.setStyleSheet("QPushButton" "{" "background-color : rgb(0, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 
    return buttonName

def createPictureLabel(holdingText , imagePathName, sizeX , sizeY):
    labelName = QLabel("holdingText")
    pixmap = QPixmap(imagePathName)
    #  get label dimensions
    w = sizeX #settingsMenuLabel.width()
    h = sizeY #settingsMenuLabel.height()
    #  set a scaled pixmap to a w x h window keeping its aspect ratio 
    if(sizeX==0 and sizeY ==0):
        labelName.setPixmap(pixmap)
    else:
        labelName.setPixmap(pixmap.scaled(w,h))
    return labelName


def createTextLabel(text , fontSize, isBold ):
    label =  QLabel(str(text))
    font = label.font()
    font.setPointSize(fontSize)
    if(isBold == 1): 
        font.setBold(1)
    label.setFont(font)
    return label

def getCrossSizeValue():
    if(activeCross == 1 ):
        return cross1Size
    elif(activeCross == 2):
        return cross2Size

def getCrossThicknessValue():
    if(activeCross == 1 ):
        return cross1Thickness
    elif(activeCross == 2):
        return cross2Thickness

class DoubleSlider(QSlider):
# Trying to get slider to step at intervals works but outputs the index of the step rather than the actuall value when 
# getValue is called 
# https://stackoverflow.com/questions/4827885/qslider-stepping
# "This is great, thanks! I found that valueChanged still seems to emit the underlying index as opposed to self.value(). ""
# "I was able to address this by connecting a new signal to valueChanged that emits self.value(). Thanks! ""
# Changed the valueChanged function to use a lambda function WORKS! https://www.learnpyqt.com/tutorials/transmitting-extra-data-qt-signals/


    def __init__(self, *args, **kargs):
        super(DoubleSlider, self).__init__( *args, **kargs)
        self._min = 0
        self._max = 99
        self.interval = 1

    def setValue(self, value):
        # print("Value: " +  str(type(value))+ str(value))
        # print("self.interval: " + str(type(self.interval)))
        # print("self._min: " + str(type(self._min)))
        index = round((int(value) - self._min) / self.interval)
        return super(DoubleSlider, self).setValue(index)

    def value(self):
        return self.index * self.interval + self._min

    @property
    def index(self):
        return super(DoubleSlider, self).value()

    def setIndex(self, index):
        return super(DoubleSlider, self).setValue(index)

    def setMinimum(self, value):
        self._min = value
        self._range_adjusted()

    def setMaximum(self, value):
        self._max = value
        self._range_adjusted()

    def setInterval(self, value):
        # To avoid division by zero
        if not value:
            raise ValueError('Interval of zero specified')
        self.interval = value
        self._range_adjusted()

    def _range_adjusted(self):
        number_of_steps = int((self._max - self._min) / self.interval)
        super(DoubleSlider, self).setMaximum(number_of_steps)

def createSlider(functionName, startValue,  minValue , maxValue , stepValue ):
    slider = DoubleSlider(Qt.Horizontal)
    # slider.setGeometry(a, b, c, d)
    slider.setMinimum(minValue)
    slider.setMaximum(maxValue)
    slider.setInterval(stepValue)

    slider.setValue(startValue)
    slider.setTickPosition(QSlider.NoTicks)
    # slider.setTickInterval(5)
    # slider.setSingleStep(5)
    slider.setStyleSheet(
        "QSlider" 
        "{" 
        "min-height: 100px;" 
        "max-height: 100px;" 
        "min-width: 200px;" 
        "max-width: 200px;" 
        # "background: #5F4141;"
        "height: 100px;"
        "margin: -20px +20px;"
        "}"
        
        "QSlider::groove:horizontal"
        "{" 
        "border: 1px solid #262626;"
        "height: 10px;"
        "background: #009FE3;"
        "margin: 0 12px;"
        "}"

        "QSlider::handle:horizontal"
        "{"
        "background: #009FE3;"
        "border: 1px solid #B5E61D;"
        "width: 50px;"
        "height: 70px;"
        "margin: -20px -20px;" #change the marging to change the size of the slider 
        "border-radius: 10px;"
        "}"
        ) 

    # slider.valueChanged[int].connect(functionName) #minValue , maxValue , stepValue )
    slider.valueChanged.connect(lambda x : functionName(slider.value())) #minValue , maxValue , stepValue )
    return slider 


# Not in use______________________
@Slot()
def openSwitchModeMenu():
    window4.show()

@Slot()
def increaseExtrusionSpeed():
    global extrusionSpeed
    extrusionSpeed = extrusionSpeed + 1 
    print(extrusionSpeed)
    label5_2.setText(str(extrusionSpeed))

@Slot()
def decreaseExtrusionSpeed():
    global extrusionSpeed
    extrusionSpeed = extrusionSpeed - 1 
    print(extrusionSpeed)
    label5_2.setText(str(extrusionSpeed))

@Slot()
def openMenu():
    window5.show()

@Slot()
def closeMenu():
    window3.close()

# Slot Functions ___________________

# Main Window Functions_____________
@Slot()
def openSettingsMenu():
    window_settingsMenu.show()

@Slot()
def closeSettingsMenu():
    closePickAndPlaceSettingsMenu()
    window_settingsMenu.close()

@Slot()
def switchCrossHair():
    global crossHairMode
    if(crossHairMode==0):
        crossHairMode = 1 
        pixmap = QPixmap(path_crossHairBlue)
        button_switchCrossHair_icon = QIcon(pixmap)
        button_switchCrossHair.setIcon(button_switchCrossHair_icon)
    elif(crossHairMode==1):
        crossHairMode = 2 
        pixmap = QPixmap(path_crossHairBY)
        button_switchCrossHair_icon = QIcon(pixmap)
        button_switchCrossHair.setIcon(button_switchCrossHair_icon)
    elif(crossHairMode==2):
        crossHairMode = 0
        pixmap = QPixmap(path_crossHairW)
        button_switchCrossHair_icon = QIcon(pixmap)
        button_switchCrossHair.setIcon(button_switchCrossHair_icon)

@Slot()
def switchMode():
    global mode
    # Mode: 0:eMode , 1:ppMode , 2:Nc Mode 
    # Extrusion mode: 0:continuous,  1:smallPad (dot1) , 2:largePad (dot2) 
    if(mode==2):
        mode = 1 
        saveAllSettings()
        # print(mode)
        pixmap = QPixmap(path_ppModeW)
        button_switchMode_icon = QIcon(pixmap)
        button_switchMode.setIcon(button_switchMode_icon)
        button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_blank)))
    elif(mode==1):
        mode = 0
        saveAllSettings()
        # print(mode)
        pixmap = QPixmap(path_eModeW)
        button_switchMode_icon = QIcon(pixmap)
        button_switchMode.setIcon(button_switchMode_icon)
        print("HERE!!!!!")
        print("Ex mode: " + str(extrusionMode))
        if(extrusionMode==2):
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_smallPadModeW)))
        elif(extrusionMode==1):
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_continuousModeW)))
        elif(extrusionMode==0):
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_largePadModeW)))

    elif(mode==0):
        mode = 2
        saveAllSettings()
        # print(mode)
        pixmap = QPixmap(path_ncModeW)
        button_switchMode_icon = QIcon(pixmap)
        button_switchMode.setIcon(button_switchMode_icon)
        button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_blank)))

@Slot()
def switchExtrusionMode():
    # Extrusion mode: 0: continuous 1: smallPad (dot1) , 2: largePad (dot2) 
    global extrusionMode
    if(mode == 0):
        if(extrusionMode==2):
            extrusionMode = 1 
            saveAllSettings()
            # print(extrusionMode)
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_smallPadModeW)))
            # layout_mainWindow.addWidget(button_switchExtrusionMode, 3, 0)
        elif(extrusionMode==1):
            extrusionMode = 0
            saveAllSettings()
            # print(extrusionMode)
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_continuousModeW)))
            # layout_mainWindow.addWidget(button_switchExtrusionMode, 3, 0)
        elif(extrusionMode==0):
            extrusionMode = 2
            saveAllSettings()
            button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_largePadModeW)))
            # layout_mainWindow.addWidget(button_switchExtrusionMode, 3, 0)
    elif(mode == 1 or mode == 2):
        button_switchExtrusionMode.setIcon(QIcon(QPixmap(path_blank)))


#  Settins Menu Slot Functions 
@Slot()
def openCrossHairSettingsMenu():
    print("!!!!")
    global crossHairMode
    crossHairMode = 2
    windowCrossHairSettings.show()

@Slot()
def closeCrossHairSettingsMenu():
    windowCrossHairSettings.close()
    window_moveCrossHairsMenuH.close()
    window_moveCrossHairsMenuV.close()
    window_crossHairColourMenu.close()
    window_crossHairSizeMenuV.close()
    window_crossHairSizeMenuH.close()
    saveAllSettings()

@Slot()
def openExtrusionSettingsMenu():
    closePickAndPlaceSettingsMenu()
    window_extrusionSettingsMenu.show()

@Slot()
def closeExtrusionSettingsMenu():
    window_extrusionSettingsMenu.close()
    closeContinuousExtrusionSettingsMenu()
    closeExtrusionDot1SettingsMenu()
    closeExtrusionDot2SettingsMenu()

@Slot()
def openPickAndPlaceSettingsMenu():
    window_pickandPlaceSettingsMenu.show()
    button_openPickAndPlaceSettingsMenu.setIcon(QIcon(QPixmap(path_ppSettingsBlue)))

@Slot()
def closePickAndPlaceSettingsMenu():
    window_pickandPlaceSettingsMenu.close()


# Cross Hair Settings Menu Slots 
@Slot()
def openMoveCrossHairsMenu():
    pixmap = QPixmap(path_moveBlue)
    icon = QIcon(pixmap)
    button_moveCrossHairsMenu.setIcon(icon)
    closeCrossHairSizeMenu()
    closeCrossHairColourMenu()
    window_moveCrossHairsMenuH.show()
    window_moveCrossHairsMenuV.show()
@Slot()
def closeMoveCrossHairMenu():
    pixmap = QPixmap(path_moveW)
    icon = QIcon(pixmap)
    button_moveCrossHairsMenu.setIcon(icon)

    window_moveCrossHairsMenuH.close()
    window_moveCrossHairsMenuV.close()

@Slot()
def openCrossHairColourMenu():
    pixmap = QPixmap(path_colourBlue)
    icon = QIcon(pixmap)
    button_crossHairColourMenu.setIcon(icon)

    closeMoveCrossHairMenu()
    closeCrossHairSizeMenu()

    window_crossHairColourMenu.show()
    print("To do")
@Slot()
def closeCrossHairColourMenu():
    pixmap = QPixmap(path_colourW)
    icon = QIcon(pixmap)
    button_crossHairColourMenu.setIcon(icon)
    window_crossHairColourMenu.close()
    print("To do")


@Slot()
def openCrossHairSizeMenu():
    pixmap = QPixmap(path_thicknessBlue)
    icon = QIcon(pixmap)
    button_crossHairSizeMenu.setIcon(icon)

    closeMoveCrossHairMenu()
    closeCrossHairColourMenu()

    window_crossHairSizeMenuV.show()
    window_crossHairSizeMenuH.show()
    print("To do")

@Slot()
def closeCrossHairSizeMenu():
    pixmap = QPixmap(path_thicknessW)
    icon = QIcon(pixmap)
    button_crossHairSizeMenu.setIcon(icon)
    window_crossHairSizeMenuV.close()
    window_crossHairSizeMenuH.close()
    print("To do")

@Slot()
def speedSliderChangeValue(value):
    global extrusionSpeed
    extrusionSpeed = value
    print(extrusionSpeed)
    label5_2.setText(str(extrusionSpeed))
    
@Slot()
def switchCrossHairToEdit():
    global activeCross
    global cross1Thickness
    global cross1Size

    if (activeCross == 1):
        activeCross = 2 
        pixmap = QPixmap(path_crossHairSwitch2)
        icon = QIcon(pixmap)
        button_switchCrossHairs1.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch2)
        icon = QIcon(pixmap)
        button_switchCrossHairs2.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch2)
        icon = QIcon(pixmap)
        button_switchCrossHairs_colour.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch2)
        icon = QIcon(pixmap)
        button_switchCrossHairs_size1.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch2)
        icon = QIcon(pixmap)
        button_switchCrossHairs_size2.setIcon(icon)

        label_thicknessValue.setText(str(cross2Thickness))
        label_sizeValue.setText(str(cross2Size))
    

        
        
    elif (activeCross == 2):
        activeCross = 1
        pixmap = QPixmap(path_crossHairSwitch1)
        icon = QIcon(pixmap)
        button_switchCrossHairs1.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch1)
        icon = QIcon(pixmap)
        button_switchCrossHairs2.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch1)
        icon = QIcon(pixmap)
        button_switchCrossHairs_colour.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch1)
        icon = QIcon(pixmap)
        button_switchCrossHairs_size1.setIcon(icon)

        pixmap = QPixmap(path_crossHairSwitch1)
        icon = QIcon(pixmap)
        button_switchCrossHairs_size2.setIcon(icon)

        label_thicknessValue.setText(str(cross1Thickness))
        label_sizeValue.setText(str(cross1Size))
        


# Move Cross  
@Slot()
def moveCrossUpSmall():
    global cross1centerY
    global crossMoveSmall
    global cross2centerY
    global activeCross
    if (activeCross == 1):
        cross1centerY = cross1centerY - crossMoveSmall
    elif (activeCross == 2):
        cross2centerY = cross2centerY - crossMoveSmall

@Slot()
def moveCrossDownSmall():
    global cross1centerY
    global crossMoveSmall
    global cross2centerY
    global activeCross
    if (activeCross == 1):
        cross1centerY = cross1centerY + crossMoveSmall
    elif(activeCross == 2):
        cross2centerY = cross2centerY + crossMoveSmall

@Slot()
def moveCrossRightSmall():
    global cross1centerX
    global crossMoveSmall
    global cross2centerX
    global activeCross
    if (activeCross == 1):
        cross1centerX = cross1centerX + crossMoveSmall
    elif(activeCross == 2):
        cross2centerX = cross2centerX + crossMoveSmall
    
@Slot()
def moveCrossLeftSmall():
    global cross1centerX
    global crossMoveSmall
    global cross2centerX
    global activeCross
    if (activeCross == 1):
        cross1centerX = cross1centerX - crossMoveSmall
    elif(activeCross == 2):
        cross2centerX = cross2centerX - crossMoveSmall

@Slot()
def moveCrossUpBig():
    global cross1centerY
    global crossMoveBig
    global cross2centerY
    global activeCross
    if (activeCross == 1):
        cross1centerY = cross1centerY - crossMoveBig
    elif(activeCross == 2):
        cross2centerY = cross2centerY - crossMoveBig

@Slot()
def moveCrossDownBig():
    global cross1centerY
    global crossMoveBig
    global cross2centerY
    global activeCross
    if (activeCross == 1):
        cross1centerY = cross1centerY + crossMoveBig
    elif(activeCross == 2):
        cross2centerY = cross2centerY + crossMoveBig

@Slot()
def moveCrossRightBig():
    global cross1centerX
    global crossMoveBig
    global cross2centerX
    global activeCross
    if (activeCross == 1):
        cross1centerX = cross1centerX + crossMoveBig
    elif(activeCross == 2):
        cross2centerX = cross2centerX + crossMoveBig
    
@Slot()
def moveCrossLeftBig():
    global cross1centerX
    global crossMoveBig
    global cross2centerX
    global activeCross
    if (activeCross == 1):
        cross1centerX = cross1centerX - crossMoveBig
    elif(activeCross == 2):
        cross2centerX = cross2centerX - crossMoveBig


# Cross Colour 
@Slot()
def turnColourwhite():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global white
    if(activeCross==1):
        cross1Colour = white
        cross1ColourString = "white"
    elif(activeCross==2):
        cross2Colour = white
        cross2ColourString = "white"

@Slot()
def turnColourred():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global red
    if(activeCross==1):
        cross1Colour = red
        cross1ColourString = "red"
    elif(activeCross==2):
        cross2Colour = red
        cross2ColourString = "red"

@Slot()
def turnColouryellow():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global yellow
    if(activeCross==1):
        cross1Colour = yellow
        cross1ColourString = "yellow"
    elif(activeCross==2):
        cross2Colour = yellow
        cross2ColourString = "yellow"

@Slot()
def turnColourgreen():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global green
    if(activeCross==1):
        cross1Colour = green
        cross1ColourString = "green"
    elif(activeCross==2):
        cross2Colour = green
        cross2ColourString = "green"

@Slot()
def turnColourblue():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global blue
    if(activeCross==1):
        cross1Colour = blue
        cross1ColourString = "blue"
    elif(activeCross==2):
        cross2Colour = blue
        cross2ColourString = "blue"

@Slot()
def turnColourpurple():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global purple
    if(activeCross==1):
        cross1Colour = purple
        cross1ColourString = "purple"
    elif(activeCross==2):
        cross2Colour = purple
        cross2ColourString = "purple"

@Slot()
def turnColourblack():
    global cross1Colour
    global cross2Colour
    global cross1ColourString
    global cross2ColourString
    global black
    if(activeCross==1):
        cross1Colour = black
        cross1ColourString = "black"
    elif(activeCross==2):
        cross2Colour = black
        cross2ColourString = "black"


       

# Cross Size 
@Slot()
def increaseCrossSize():
    global cross1Size
    global cross2Size
    global crossSizeIncrement
    if(activeCross == 1 ):
        if(minumumCrossSize <= (cross1Size + crossSizeIncrement) <= maximimCrossSize):
            cross1Size = cross1Size + crossSizeIncrement
            label_sizeValue.setText(str(cross1Size))
        else:
            print("cant do that")
    elif(activeCross==2 ):
        if(minumumCrossSize <= (cross2Size + crossSizeIncrement) <= maximimCrossSize):
            cross2Size = cross2Size + crossSizeIncrement
            label_sizeValue.setText(str(cross2Size))
        else:
            print("cant do that")

        #  if():
            
        # else:
        #     print("cant do that")

@Slot()
def decreaseCrossSize():
    global cross1Size
    global cross2Size
    global crossSizeIncrement
    if(activeCross == 1 ):
        if(minumumCrossSize <= (cross1Size - crossSizeIncrement) <= maximimCrossSize):
            cross1Size = cross1Size - crossSizeIncrement
            label_sizeValue.setText(str(cross1Size))
        else:
            print("cant do that")
    elif(activeCross==2 ):
        if(minumumCrossSize <= (cross2Size - crossSizeIncrement) <= maximimCrossSize):
            cross2Size = cross2Size - crossSizeIncrement
            label_sizeValue.setText(str(cross2Size))
        else:
            print("cant do that")
    
@Slot()
def increaseCrossThickness():
    global cross1Thickness
    global cross2Thickness
    if(activeCross == 1 ):
        if(minumumCrossThickness <= (cross1Thickness + crossThicknessIncrement) <= maximumCrossThickness ):
            cross1Thickness = cross1Thickness + crossThicknessIncrement
            label_thicknessValue.setText(str(cross1Thickness))
        else:
            print("cant do that")
    elif(activeCross==2 ):
        if(minumumCrossThickness <= (cross2Thickness + crossThicknessIncrement) <= maximumCrossThickness ):
            cross2Thickness = cross2Thickness + crossThicknessIncrement
            label_thicknessValue.setText(str(cross2Thickness))
        else:
            print("cant do that")

@Slot()
def decreaseCrossThickness():
    global cross1Thickness
    global cross2Thickness
    if(activeCross == 1 ):
        if(minumumCrossThickness <= (cross1Thickness - crossThicknessIncrement) <= maximumCrossThickness ):
            cross1Thickness = cross1Thickness - crossThicknessIncrement
            label_thicknessValue.setText(str(cross1Thickness))
        else:
            print("cant do that")
    elif(activeCross==2 ):
        if(minumumCrossThickness <= (cross2Thickness - crossThicknessIncrement) <= maximumCrossThickness ):
            cross2Thickness = cross2Thickness - crossThicknessIncrement
            label_thicknessValue.setText(str(cross2Thickness))
        else:
            print("cant do that")


# Continuous
@Slot()
def openContinuousExtrusionSettingsMenu():
    window_continuousExtrusionSettingsMenu.show()
    closeExtrusionDot1SettingsMenu()
    closeExtrusionDot2SettingsMenu()
    button_continuousSettingsMenu.setIcon(QIcon(QPixmap(path_continuousSettingsBlue)))

@Slot()
def closeContinuousExtrusionSettingsMenu():
    global continuousExtrusionSpeed
    global continuousRetractionSteps

    global savedContinuousExtrusionSpeed
    global savedContinuousRetractionSteps

    continuousExtrusionSpeed = savedContinuousExtrusionSpeed
    continuousRetractionSteps = savedContinuousRetractionSteps

    slider_continuousExtrusionSpeed.setValue(savedContinuousExtrusionSpeed)
    slider_continuousRetractionSteps.setValue(savedContinuousRetractionSteps)

    button_continuousSettingsMenu.setIcon(QIcon(QPixmap(path_continuousSettingsW)))
    window_continuousExtrusionSettingsMenu.close()

@Slot()
def saveAndCloseContinuousExtrusionSettingsMenu():
    global continuousExtrusionSpeed
    global continuousRetractionSteps
    
    global savedContinuousExtrusionSpeed
    global savedContinuousRetractionSteps

    window_continuousExtrusionSettingsMenu.close()
    savedContinuousExtrusionSpeed = continuousExtrusionSpeed 
    savedContinuousRetractionSteps = continuousRetractionSteps 

    saveAllSettings()

@Slot()
def changeContinuousExtrusionSpeed_fnSlider(value): #value, minValue , maxValue , stepValue
    global continuousExtrusionSpeed
    continuousExtrusionSpeed = value
    label_continuousExtrusionSpeedValue.setText(str(continuousExtrusionSpeed))

@Slot()
def changeContinuousRetractionSteps_fnSlider(value): #value, minValue , maxValue , stepValue
    global continuousRetractionSteps
    continuousRetractionSteps = value
    label_continuousRetractionStepsValue.setText(str(continuousRetractionSteps))


# Dot 1 
@Slot()
def openExtrusionDot1SettingsMenu():
    window_extrusionDot1SettingsMenu.show()
    button_dot1SettingsMenu.setIcon(QIcon(QPixmap(path_smallPadSettingsBlue)))

    closeContinuousExtrusionSettingsMenu()
    closeExtrusionDot2SettingsMenu()

@Slot()
def closeExtrusionDot1SettingsMenu():
    global dot1ExtrusionSpeed
    global dot1ExtrusionSteps
    global dot1RetractionSteps

    global savedDot1ExtrusionSpeed
    global savedDot1ExtrusionSteps
    global savedDot1RetractionSteps

    dot1ExtrusionSpeed = savedDot1ExtrusionSpeed
    dot1ExtrusionSteps = savedDot1ExtrusionSteps
    dot1RetractionSteps = savedDot1RetractionSteps

    slider_dot1ExtrusionSpeed.setValue(savedDot1ExtrusionSpeed)
    slider_dot1ExtrusionSteps.setValue(savedDot1ExtrusionSteps)
    slider_dot1RetractionSteps.setValue(savedDot1RetractionSteps)

    button_dot1SettingsMenu.setIcon(QIcon(QPixmap(path_smallPadSettingsW)))
    window_extrusionDot1SettingsMenu.close()

@Slot()
def saveAndCloseExtrusionDot1SettingsMenu():
    global dot1ExtrusionSpeed
    global dot1ExtrusionSteps
    global dot1RetractionSteps
    
    global savedDot1ExtrusionSpeed
    global savedDot1ExtrusionSteps
    global savedDot1RetractionSteps

    window_extrusionDot1SettingsMenu.close()
    savedDot1ExtrusionSpeed = dot1ExtrusionSpeed 
    savedDot1ExtrusionSteps =  dot1ExtrusionSteps 
    savedDot1RetractionSteps = dot1RetractionSteps 

    saveAllSettings()

@Slot()
def changeDot1ExtrusionSpeed_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot1ExtrusionSpeed
    dot1ExtrusionSpeed = value
    label_dot1ExtrusionSpeedValue.setText(str(dot1ExtrusionSpeed))

@Slot()
def changeDot1ExtrusionSteps_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot1ExtrusionSteps
    dot1ExtrusionSteps = value
    label_dot1ExtrusionStepsValue.setText(str(dot1ExtrusionSteps))

@Slot()
def changeDot1RetractionSteps_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot1RetractionSteps
    dot1RetractionSteps = value
    label_dot1RetractionStepsValue.setText(str(dot1RetractionSteps))

# Dot 2 
@Slot()
def openExtrusionDot2SettingsMenu():
    window_extrusionDot2SettingsMenu.show()
    closeContinuousExtrusionSettingsMenu()
    closeExtrusionDot1SettingsMenu()
    button_dot2SettingsMenu.setIcon(QIcon(QPixmap(path_largePadSettingsBlue)))

@Slot()
def closeExtrusionDot2SettingsMenu():
    global dot2ExtrusionSpeed
    global dot2ExtrusionSteps
    global dot2RetractionSteps

    global savedDot2ExtrusionSpeed
    global savedDot2ExtrusionSteps
    global savedDot2RetractionSteps

    dot2ExtrusionSpeed = savedDot2ExtrusionSpeed
    dot2ExtrusionSteps = savedDot2ExtrusionSteps
    dot2RetractionSteps = savedDot2RetractionSteps

    slider_dot2ExtrusionSpeed.setValue(savedDot2ExtrusionSpeed)
    slider_dot2ExtrusionSteps.setValue(savedDot2ExtrusionSteps)
    slider_dot2RetractionSteps.setValue(savedDot2RetractionSteps)

    button_dot2SettingsMenu.setIcon(QIcon(QPixmap(path_largePadSettingsW)))
    window_extrusionDot2SettingsMenu.close()

@Slot()
def saveAndCloseExtrusionDot2SettingsMenu():
    global dot2ExtrusionSpeed
    global dot2ExtrusionSteps
    global dot2RetractionSteps
    
    global savedDot2ExtrusionSpeed
    global savedDot2ExtrusionSteps
    global savedDot2RetractionSteps

    window_extrusionDot2SettingsMenu.close()
    savedDot2ExtrusionSpeed = dot2ExtrusionSpeed 
    savedDot2ExtrusionSteps =  dot2ExtrusionSteps 
    savedDot2RetractionSteps = dot2RetractionSteps 

    saveAllSettings()

@Slot()
def changeDot2ExtrusionSpeed_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot2ExtrusionSpeed
    dot2ExtrusionSpeed = value
    label_dot2ExtrusionSpeedValue.setText(str(dot2ExtrusionSpeed))

@Slot()
def changeDot2ExtrusionSteps_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot2ExtrusionSteps
    dot2ExtrusionSteps = value
    label_dot2ExtrusionStepsValue.setText(str(dot2ExtrusionSteps))

@Slot()
def changeDot2RetractionSteps_fnSlider(value): #value, minValue , maxValue , stepValue
    global dot2RetractionSteps
    dot2RetractionSteps = value
    label_dot2RetractionStepsValue.setText(str(dot2RetractionSteps))

# Pick and place settings 
@Slot()
def closePickAndPlaceSettingsMenu():
    global encoderTicksPerJump 
    global speedPP 
    global flipAngle 

    global savedEncoderTicksPerJump 
    global savedSpeedPP 
    global savedFlipAngle 

    encoderTicksPerJump = savedEncoderTicksPerJump 
    speedPP = savedSpeedPP
    flipAngle = savedFlipAngle

    slider_dot2ExtrusionSpeed.setValue(savedDot2ExtrusionSpeed)
    slider_dot2ExtrusionSteps.setValue(savedDot2ExtrusionSteps)
    slider_dot2RetractionSteps.setValue(savedDot2RetractionSteps)

    button_openPickAndPlaceSettingsMenu.setIcon(QIcon(QPixmap(path_ppSettingsW)))
    window_pickandPlaceSettingsMenu.close()

@Slot()
def saveAndClosePickAndPlaceSettingsMenu():
    global encoderTicksPerJump 
    global speedPP 
    global flipAngle 

    global savedEncoderTicksPerJump 
    global savedSpeedPP 
    global savedFlipAngle 

    savedEncoderTicksPerJump = encoderTicksPerJump
    savedSpeedPP = speedPP
    savedFlipAngle = flipAngle

    button_openPickAndPlaceSettingsMenu.setIcon(QIcon(QPixmap(path_ppSettingsW)))
    window_pickandPlaceSettingsMenu.close()
    saveAllSettings()

@Slot()
def speedPP_fnSlider(value): #value, minValue , maxValue , stepValue
    global speedPP
    speedPP = value
    label_speedPPValue.setText(str(speedPP))

@Slot()
def flipAngle_fnSlider(value): #value, minValue , maxValue , stepValue
    global flipAngle
    flipAngle = value
    label_flipAngleValue.setText(str(flipAngle))

@Slot()
def encoderTicksPerJump_fnSlider(value): #value, minValue , maxValue , stepValue
    global encoderTicksPerJump
    encoderTicksPerJump = value
    label_encoderTicksPerJumpValue.setText(str(encoderTicksPerJump))


@Slot()
def quitFunction(): 
    print("here")
    sys.exit()

app = QApplication([])

# Quit Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
quitWindow = createWindow(0, 0 , 1,1, 0, 150, 400, windowBackgroundColour ,windowTextColour)

# Quit Button
# quitQuitbutton = QPushButton("Quitter")
# quitQuitbutton.clicked.connect(sys.exit) # quiter button 

quitQuitbutton = createPictureButton(quitFunction, path_closeR, 50  , 50 )#Added to push video to right

layout_quitWindow = QGridLayout()
layout_quitWindow.addWidget(quitQuitbutton, 0 ,0)
quitWindow.setLayout(layout_quitWindow)

# Main Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1,0, 1, menuXdistance*1, 0, windowBackgroundColour ,windowTextColour)

qr = mainWindow.frameGeometry()
mainWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()


# mainWindow.setWindowState(Qt.WindowMaximized)
# print(mainWindow.x())
# print(mainWindow.y())
# print(mainWindow.width())
# print(mainWindow.height())
# print(mainWindow.maximumSize())
# print(mainWindow.frameGeometry())
# print(screenHeight)
# print(screenWidth)
# print(mainWindow.frameGeometry())
# mainWindow.resize(1*screenWidth, 1*screenHeight)


# Open CV for Microscope Image _______________
cap = cv2.VideoCapture(0)
cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1*screenWidth ) #above 0.7 resizes to maximum 
cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1*screenHeight)  #above 0.7 resizes to maximum 

# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 480)
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 320)

# timer for getting frames
timer = QTimer()
timer.timeout.connect(displayFrame)
timer.start(60)
# ___________________________________________


# mainWindow Assets____________________________
label = QLabel('No Camera Feed')

# Quit Button
mainQuitbutton = QPushButton("Quitter")
mainQuitbutton.clicked.connect(sys.exit) # quiter button 

button_blank = createPictureButton(switchMode, path_blank, 100 , 100 )#Added to push video to right

layout_mainWindow = QGridLayout()
layout_mainWindow.addWidget(button_blank , 0, 0 )
layout_mainWindow.addWidget(label, 0, 1 , 5, 1 )#To push buttons to top set image collum spacing to number of buttons + 1 

mainWindow.setLayout(layout_mainWindow)


# # Window Main Menu ________________
# # Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainMenuWindow = createWindow(0, 0 , 1,1, 0, menuXdistance*1, 0, windowBackgroundColour ,windowTextColour) 

button_switchMode = createPictureButton(switchMode, path_ncModeW, 100 , 100 )
button_openSettingsMenu = createPictureButton(openSettingsMenu, path_settingsW, 100 , 100 )
button_switchCrossHair = createPictureButton(switchCrossHair, path_crossHairW, 100 , 100 )
button_openMenu = createPictureButton(openMenu, path_menuW, 100 , 100 )
 # Mode 1: ppMode , 0: eMode , 2:Nc Mode 
 # Extrusion mode: 0: continuous 1: smallPad (dot1) , 2: largePad (dot2) 
if(mode == 0):
    if(extrusionMode==0):
        button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_continuousModeW, 100 , 100 )
    elif(extrusionMode==1):
        button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_smallPadModeW, 100 , 100 )
    elif(extrusionMode==2):
        button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_smallPadModeW, 100 , 100 )
else: 
    button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_blank, 100 , 100 )


layout_mainMenuWindow = QGridLayout()
# layout_mainMenuWindow.addWidget(button_openMenu , 0, 0 )
layout_mainMenuWindow.addWidget(button_openSettingsMenu, 0, 0)
layout_mainMenuWindow.addWidget(button_switchCrossHair, 1, 0)
layout_mainMenuWindow.addWidget(button_switchMode , 2, 0)
layout_mainMenuWindow.addWidget(button_switchExtrusionMode, 3, 0)
# layout_mainMenuWindow.addWidget(label, 0, 1 , 5, 1 )#To push buttons to top set image collum spacing to number of buttons + 1 

mainMenuWindow.setLayout(layout_mainMenuWindow)





# Window: Settings Menu _______________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_settingsMenu = createWindow(0, 0 , 1,1, 0, menuXdistance*1, 0,windowBackgroundColour ,windowTextColour)

settingsMenuLabel = createPictureLabel('Settings Menu', path_settingsWordW, 100, 28)

# settingsMenuLabel = QLabel('Settings Menu')
# pixmap = QPixmap(path_settingsWordW)
# settingsMenuLabel_icon = pixmap
# # settingsMenuLabel.setPixmap(settingsMenuLabel_icon)

# # QPixmap p; // load pixmap
# #  get label dimensions
# w = 100 #settingsMenuLabel.width()
# h = 28 #settingsMenuLabel.height()

# #  set a scaled pixmap to a w x h window keeping its aspect ratio 
# settingsMenuLabel.setPixmap(pixmap.scaled(w,h))
# # settingsMenuLabel.setPixmapSize(QSize(100,100))
# # settingsMenuLabel.setFlat(1)
# # image = qimage2ndarray.array2qimage(frame)
# # label.setPixmap(QPixmap.fromImage(image))


button_openCrossHairSettingsMenu = createPictureButton(openCrossHairSettingsMenu, path_crossHairSettingsW, 100 , 100 )
button_openPickAndPlaceSettingsMenu = createPictureButton(openPickAndPlaceSettingsMenu, path_ppSettingsW, 100 , 100 )
button_openExtrusionSettingsMenu = createPictureButton(openExtrusionSettingsMenu, path_eSettingsW, 100 , 100 )
button_closeSettingsMenu = createPictureButton(closeSettingsMenu, path_closeR, 100 , 100 )

layout_settingsMenu = QGridLayout()
# layout_settingsMenu.addWidget(settingsMenuLabel, 0, 0 )
layout_settingsMenu.addWidget(button_openCrossHairSettingsMenu, 1, 0 )
layout_settingsMenu.addWidget(button_openPickAndPlaceSettingsMenu, 2, 0 )
layout_settingsMenu.addWidget(button_openExtrusionSettingsMenu, 3, 0 )
layout_settingsMenu.addWidget(button_closeSettingsMenu, 4, 0 )

window_settingsMenu.setLayout(layout_settingsMenu)
# __________________________




# Window CrossHairSettings Menu __________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
windowCrossHairSettings = createWindow(0,0, 1,1, 0, menuXdistance*1, 0, windowBackgroundColour ,windowTextColour)

button_moveCrossHairsMenu = createPictureButton(openMoveCrossHairsMenu, path_moveW, 100 , 100 )
button_crossHairColourMenu = createPictureButton(openCrossHairColourMenu, path_colourW, 100 , 100 )
button_crossHairSizeMenu = createPictureButton(openCrossHairSizeMenu, path_thicknessW, 100 , 100 )
button_closeCrossHairSettingsMenu = createPictureButton(closeCrossHairSettingsMenu, path_closeR, 100 , 100 )

layoutCrossHairSettings = QGridLayout()
layoutCrossHairSettings.addWidget(button_moveCrossHairsMenu , 0, 0 )
layoutCrossHairSettings.addWidget(button_crossHairColourMenu, 1, 0 )
layoutCrossHairSettings.addWidget(button_crossHairSizeMenu, 2, 0 )
layoutCrossHairSettings.addWidget(button_closeCrossHairSettingsMenu, 3, 0 )

windowCrossHairSettings.setLayout(layoutCrossHairSettings)
# _____________________________________




# Window MoveCrossHairs Menu __________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_moveCrossHairsMenuH = createWindow(0,0,  1,1, 0, 150, 0 , windowBackgroundColour ,windowTextColour)

window_moveCrossHairsMenuV = createWindow(0,0 ,1,1, 0, 150, 0,  windowBackgroundColour ,windowTextColour)

button_moveCrossHairs_upSmall = createPictureButton(moveCrossUpSmall, path_upSmallW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_downSmall = createPictureButton(moveCrossDownSmall , path_downSmallW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_leftSmall = createPictureButton(moveCrossLeftSmall , path_leftSmallW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_rightSmall = createPictureButton(moveCrossRightSmall , path_rightSmallW , arrowIconSize , arrowIconSize )

button_moveCrossHairs_upBig = createPictureButton(moveCrossUpBig, path_upBigW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_downBig = createPictureButton(moveCrossDownBig , path_downBigW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_leftBig = createPictureButton(moveCrossLeftBig , path_leftBigW , arrowIconSize , arrowIconSize )
button_moveCrossHairs_rightBig = createPictureButton(moveCrossRightBig , path_rightBigW , arrowIconSize , arrowIconSize )

button_switchCrossHairs1 = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , arrowIconSize , arrowIconSize )
# button_switchCrossHairs1.setStyleSheet("QPushButton" "{" "background-color : rgb(244, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 

button_switchCrossHairs2 = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , arrowIconSize , arrowIconSize )
# button_switchCrossHairs2.setStyleSheet("QPushButton" "{" "background-color : rgb(244, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 


layout_moveCrossHairsMenuV = QGridLayout()
layout_moveCrossHairsMenuV.addWidget(button_switchCrossHairs1, 0, 0 )
layout_moveCrossHairsMenuV.addWidget(button_moveCrossHairs_upBig, 1, 0 )
layout_moveCrossHairsMenuV.addWidget(button_moveCrossHairs_upSmall, 2, 0 )
layout_moveCrossHairsMenuV.addWidget(button_moveCrossHairs_downSmall, 3, 0 )
layout_moveCrossHairsMenuV.addWidget(button_moveCrossHairs_downBig, 4, 0 )

layout_moveCrossHairsMenuH = QGridLayout()
layout_moveCrossHairsMenuH.addWidget(button_switchCrossHairs2, 0, 0 )
layout_moveCrossHairsMenuH.addWidget(button_moveCrossHairs_leftBig, 0, 1)
layout_moveCrossHairsMenuH.addWidget(button_moveCrossHairs_leftSmall, 0, 2)
layout_moveCrossHairsMenuH.addWidget(button_moveCrossHairs_rightSmall, 0, 3 )
layout_moveCrossHairsMenuH.addWidget(button_moveCrossHairs_rightBig, 0, 4 )

window_moveCrossHairsMenuH.setLayout(layout_moveCrossHairsMenuH)
window_moveCrossHairsMenuV.setLayout(layout_moveCrossHairsMenuV)


# Window: CrossHairColourMenu Menu __________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_crossHairColourMenu = createWindow(0,0 , 1,1, 0, 150, 0 ,windowBackgroundColour ,windowTextColour)

button_switchCrossHairs_colour = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , colourIconSize , colourIconSize )
button_white_colour = createPictureButton(turnColourwhite , path_white , colourIconSize , colourIconSize )
button_red_colour = createPictureButton(turnColourred , path_red , colourIconSize , colourIconSize )
button_yellow_colour = createPictureButton(turnColouryellow , path_yellow , colourIconSize , colourIconSize )
button_green_colour = createPictureButton(turnColourgreen , path_green , colourIconSize , colourIconSize )
button_blue_colour = createPictureButton(turnColourblue , path_blue , colourIconSize , colourIconSize )
button_purple_colour = createPictureButton(turnColourpurple , path_purple , colourIconSize , colourIconSize )
button_black_colour = createPictureButton(turnColourblack , path_black , colourIconSize , colourIconSize  )

layout_crossHairColourMenu = QGridLayout()
layout_crossHairColourMenu.addWidget(button_switchCrossHairs_colour , 0, 0 )
layout_crossHairColourMenu.addWidget(button_white_colour , 0, 1 )
layout_crossHairColourMenu.addWidget(button_red_colour , 0, 2)
layout_crossHairColourMenu.addWidget(button_yellow_colour , 0, 3 )
layout_crossHairColourMenu.addWidget(button_green_colour , 0, 4 )
layout_crossHairColourMenu.addWidget(button_blue_colour , 0, 5 )
layout_crossHairColourMenu.addWidget(button_purple_colour , 0, 6 )
layout_crossHairColourMenu.addWidget(button_black_colour , 0, 7 )

window_crossHairColourMenu.setLayout(layout_crossHairColourMenu)

# Window: CrossHairSizeMenu Menu __________
window_crossHairSizeMenuV = createWindow(0, 0 ,1,1, 0, 150, 0, windowBackgroundColour ,windowTextColour)
window_crossHairSizeMenuH = createWindow(0, 0 ,1,1, 0, 300, 0, windowBackgroundColour ,windowTextColour)


button_switchCrossHairs_size1 = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , crossHairSizeIconSize  , crossHairSizeIconSize  )
button_switchCrossHairs_size2 = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , crossHairSizeIconSize  , crossHairSizeIconSize)

button_increaseCrossSize = createPictureButton(increaseCrossSize , path_plus10W , crossHairSizeIconSize/2  , crossHairSizeIconSize/2  )
button_decreaseCrossSize = createPictureButton(decreaseCrossSize , path_minus10W , crossHairSizeIconSize/2  , crossHairSizeIconSize/2  )

button_increaseCrossThickness = createPictureButton(increaseCrossThickness , path_plus1W , crossHairSizeIconSize/2  , crossHairSizeIconSize/2  )
button_decreaseCrossThickness = createPictureButton(decreaseCrossThickness , path_minus1W , crossHairSizeIconSize/2  , crossHairSizeIconSize/2  )


label_sizeIcon = createPictureLabel("Scale", path_scaleW, crossHairSizeIconSize/2, crossHairSizeIconSize/2)
label_thicknessIcon = createPictureLabel("Scale", path_thicknessNewW, crossHairSizeIconSize/2, crossHairSizeIconSize/2)


label_sizeValue = createTextLabel( str(getCrossSizeValue()), 40 , 1 )
label_thicknessValue = createTextLabel( str(getCrossThicknessValue()), 40 , 1 )

label_thicknessText = createTextLabel( "Width: ", 40 , 1 )
label_sizeText = createTextLabel( "Size: ", 40 , 1 )

layout_crossHairSizeMenuV = QGridLayout()
layout_crossHairSizeMenuV.addWidget(button_switchCrossHairs_size2, 0, 0  )

layout_crossHairSizeMenuH = QGridLayout()
layout_crossHairSizeMenuH.addWidget(label_thicknessIcon, 0, 0 , 1, 1 )
layout_crossHairSizeMenuH.addWidget(label_thicknessText, 0, 1 , 1, 1  )
layout_crossHairSizeMenuH.addWidget(button_decreaseCrossThickness, 0, 2 , 1, 1  )
layout_crossHairSizeMenuH.addWidget(label_thicknessValue, 0, 3 , 1 , 1 )
layout_crossHairSizeMenuH.addWidget(button_increaseCrossThickness, 0, 4 , 1 , 1)

layout_crossHairSizeMenuH.addWidget(label_sizeIcon, 1, 0 )
layout_crossHairSizeMenuH.addWidget(label_sizeText, 1, 1 , 1, 1  )
layout_crossHairSizeMenuH.addWidget(button_decreaseCrossSize , 1 ,2, 1, 1 )
layout_crossHairSizeMenuH.addWidget(label_sizeValue , 1, 3 ,1 ,1 )
layout_crossHairSizeMenuH.addWidget(button_increaseCrossSize , 1, 4 ,1 ,1 )

window_crossHairSizeMenuV.setLayout(layout_crossHairSizeMenuV)
window_crossHairSizeMenuH.setLayout(layout_crossHairSizeMenuH)


# Window Extrusion Settings Menu ____________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_extrusionSettingsMenu = createWindow(0,0,1,1, 0, 0, 0, windowBackgroundColour ,windowTextColour)

button_continuousSettingsMenu = createPictureButton(openContinuousExtrusionSettingsMenu , path_continuousSettingsW ,menuIconWidth,menuIconHeight )
button_dot1SettingsMenu = createPictureButton(openExtrusionDot1SettingsMenu , path_smallPadSettingsW ,menuIconWidth,menuIconHeight )
button_dot2SettingsMenu = createPictureButton(openExtrusionDot2SettingsMenu , path_largePadSettingsW ,menuIconWidth,menuIconHeight )
button_closeExtrusionSettingMenu = createPictureButton(closeExtrusionSettingsMenu , path_closeR ,menuIconWidth,menuIconHeight )
layout_extrusionSettingsMenu = QGridLayout()

layout_extrusionSettingsMenu.addWidget(button_continuousSettingsMenu , 0, 0 )
layout_extrusionSettingsMenu.addWidget(button_dot1SettingsMenu , 1, 0 )
layout_extrusionSettingsMenu.addWidget(button_dot2SettingsMenu , 2, 0 )
layout_extrusionSettingsMenu.addWidget(button_closeExtrusionSettingMenu , 3, 0 )

window_extrusionSettingsMenu.setLayout(layout_extrusionSettingsMenu)

# Window: ContinuousExtrusionSettings Menu __________
# # Create new window
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_continuousExtrusionSettingsMenu = createWindow(0, 0, 1,1, 0, subMenuIndentX, subMenuIndentY , windowBackgroundColour ,windowTextColour)

label_continuousExtrusionSettingsMenuTitle = createTextLabel("Continuous  Mode Extrusion Settings", textSize_extrusionSettingsMenu , 1 )

label_continuousExtrusionSpeedText = createTextLabel("Motor Speed:", textSize_extrusionSettingsMenu , 1 )
label_continuousExtrusionSpeedValue = createTextLabel(str(continuousExtrusionSpeed), textSize_extrusionSettingsMenu, 1 )
slider_continuousExtrusionSpeed = createSlider(changeContinuousExtrusionSpeed_fnSlider, continuousExtrusionSpeed, 0 , 100, 10 )


label_continuousRetractionStepsText = createTextLabel("Retraction Steps:", textSize_extrusionSettingsMenu , 1 )
label_continuousRetractionStepsValue = createTextLabel(str(continuousRetractionSteps), textSize_extrusionSettingsMenu, 1 )
slider_continuousRetractionSteps = createSlider(changeContinuousRetractionSteps_fnSlider, continuousRetractionSteps, 0 , 100, 10 )

button_saveAndCloseContinuousSettingsMenu = createPictureButton(saveAndCloseContinuousExtrusionSettingsMenu, path_saveAndCloseGreen , colourIconSize , colourIconSize )
button_closeContinuousSettingsMenu = createPictureButton(closeContinuousExtrusionSettingsMenu , path_closeR , colourIconSize , colourIconSize )


layout_continuousExtrusionSettingsMenu = QGridLayout()

layout_continuousExtrusionSettingsMenu.addWidget(label_continuousExtrusionSettingsMenuTitle, 0, 0 , 1, 3  )

layout_continuousExtrusionSettingsMenu.addWidget(label_continuousExtrusionSpeedText , 1, 0 )
layout_continuousExtrusionSettingsMenu.addWidget(label_continuousExtrusionSpeedValue, 1, 1 )
layout_continuousExtrusionSettingsMenu.addWidget(slider_continuousExtrusionSpeed , 1, 2 )


layout_continuousExtrusionSettingsMenu.addWidget(label_continuousRetractionStepsText , 3, 0 )
layout_continuousExtrusionSettingsMenu.addWidget(label_continuousRetractionStepsValue, 3, 1 )
layout_continuousExtrusionSettingsMenu.addWidget(slider_continuousRetractionSteps , 3, 2 )

layout_continuousExtrusionSettingsMenu.addWidget(button_saveAndCloseContinuousSettingsMenu, 4, 0 , 1 ,2  )
layout_continuousExtrusionSettingsMenu.addWidget(button_closeContinuousSettingsMenu, 4, 2, 1 ,2  )

window_continuousExtrusionSettingsMenu.setLayout(layout_continuousExtrusionSettingsMenu)
# _____________________________________________________________________


# Window: Dot1ExtrusionSettings Menu __________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_extrusionDot1SettingsMenu = createWindow(0, 0, 1,1, 0, subMenuIndentX, subMenuIndentY , windowBackgroundColour ,windowTextColour)

label_extrusionDot1SettingsMenuTitle = createTextLabel("Small Dot Mode Extrusion Settings", textSize_extrusionSettingsMenu , 1 )

label_dot1ExtrusionSpeedText = createTextLabel("Motor Speed:", textSize_extrusionSettingsMenu , 1 )
label_dot1ExtrusionSpeedValue = createTextLabel(str(dot1ExtrusionSpeed), textSize_extrusionSettingsMenu, 1 )
slider_dot1ExtrusionSpeed = createSlider(changeDot1ExtrusionSpeed_fnSlider, dot1ExtrusionSpeed, 0 , 100, 10 )

label_dot1ExtrusionStepsText = createTextLabel("Extrusion Steps:", textSize_extrusionSettingsMenu , 1 )
label_dot1ExtrusionStepsValue = createTextLabel(str(dot1ExtrusionSteps), textSize_extrusionSettingsMenu, 1 )
slider_dot1ExtrusionSteps = createSlider(changeDot1ExtrusionSteps_fnSlider, dot1ExtrusionSteps, 0 , 100, 10 )

label_dot1RetractionStepsText = createTextLabel("Retraction Steps:", textSize_extrusionSettingsMenu , 1 )
label_dot1RetractionStepsValue = createTextLabel(str(dot1RetractionSteps), textSize_extrusionSettingsMenu, 1 )
slider_dot1RetractionSteps = createSlider(changeDot1RetractionSteps_fnSlider, dot1RetractionSteps, 0 , 100, 10 )

button_saveAndCloseDot1SettingsMenu = createPictureButton(saveAndCloseExtrusionDot1SettingsMenu, path_saveAndCloseGreen, colourIconSize , colourIconSize )
button_closeDot1SettingsMenu = createPictureButton(closeExtrusionDot1SettingsMenu , path_closeR , colourIconSize , colourIconSize )


layout_extrusionDot1SettingsMenu = QGridLayout()

layout_extrusionDot1SettingsMenu.addWidget(label_extrusionDot1SettingsMenuTitle, 0, 0 , 1, 3  )

layout_extrusionDot1SettingsMenu.addWidget(label_dot1ExtrusionSpeedText , 1, 0 )
layout_extrusionDot1SettingsMenu.addWidget(label_dot1ExtrusionSpeedValue, 1, 1 )
layout_extrusionDot1SettingsMenu.addWidget(slider_dot1ExtrusionSpeed , 1, 2 )

layout_extrusionDot1SettingsMenu.addWidget(label_dot1ExtrusionStepsText , 2, 0 )
layout_extrusionDot1SettingsMenu.addWidget(label_dot1ExtrusionStepsValue, 2, 1 )
layout_extrusionDot1SettingsMenu.addWidget(slider_dot1ExtrusionSteps , 2, 2 )

layout_extrusionDot1SettingsMenu.addWidget(label_dot1RetractionStepsText , 3, 0 )
layout_extrusionDot1SettingsMenu.addWidget(label_dot1RetractionStepsValue, 3, 1 )
layout_extrusionDot1SettingsMenu.addWidget(slider_dot1RetractionSteps , 3, 2 )

layout_extrusionDot1SettingsMenu.addWidget(button_saveAndCloseDot1SettingsMenu, 4, 0 , 1 ,2  )
layout_extrusionDot1SettingsMenu.addWidget(button_closeDot1SettingsMenu, 4, 2, 1 ,2  )

window_extrusionDot1SettingsMenu.setLayout(layout_extrusionDot1SettingsMenu)
# _____________________________________________________________________

# Window: Dot2ExtrusionSettings Menu __________
# # Create new window
# useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour ):
window_extrusionDot2SettingsMenu = createWindow(0, 0, 1,1, 0, subMenuIndentX, subMenuIndentY , windowBackgroundColour ,windowTextColour)

label_extrusionDot2SettingsMenuTitle = createTextLabel("Large Dot Mode Extrusion Settings", textSize_extrusionSettingsMenu , 1 )

label_dot2ExtrusionSpeedText = createTextLabel("Motor Speed:", textSize_extrusionSettingsMenu , 1 )
label_dot2ExtrusionSpeedValue = createTextLabel(str(dot2ExtrusionSpeed), textSize_extrusionSettingsMenu, 1 )
slider_dot2ExtrusionSpeed = createSlider(changeDot2ExtrusionSpeed_fnSlider, dot2ExtrusionSpeed, 0 , 100, 10 )

label_dot2ExtrusionStepsText = createTextLabel("Extrusion Steps:", textSize_extrusionSettingsMenu , 1 )
label_dot2ExtrusionStepsValue = createTextLabel(str(dot2ExtrusionSteps), textSize_extrusionSettingsMenu, 1 )
slider_dot2ExtrusionSteps = createSlider(changeDot2ExtrusionSteps_fnSlider, dot2ExtrusionSteps, 0 , 100, 10 )

label_dot2RetractionStepsText = createTextLabel("Retraction Steps:", textSize_extrusionSettingsMenu , 1 )
label_dot2RetractionStepsValue = createTextLabel(str(dot2RetractionSteps), textSize_extrusionSettingsMenu, 1 )
slider_dot2RetractionSteps = createSlider(changeDot2RetractionSteps_fnSlider, dot2RetractionSteps, 0 , 100, 10 )

button_saveAndCloseDot2SettingsMenu = createPictureButton(saveAndCloseExtrusionDot2SettingsMenu, path_saveAndCloseGreen , colourIconSize , colourIconSize )
button_closeDot2SettingsMenu = createPictureButton(closeExtrusionDot2SettingsMenu , path_closeR , colourIconSize , colourIconSize )


layout_extrusionDot2SettingsMenu = QGridLayout()

layout_extrusionDot2SettingsMenu.addWidget(label_extrusionDot2SettingsMenuTitle, 0, 0 , 1, 3  )

layout_extrusionDot2SettingsMenu.addWidget(label_dot2ExtrusionSpeedText , 1, 0 )
layout_extrusionDot2SettingsMenu.addWidget(label_dot2ExtrusionSpeedValue, 1, 1 )
layout_extrusionDot2SettingsMenu.addWidget(slider_dot2ExtrusionSpeed , 1, 2 )

layout_extrusionDot2SettingsMenu.addWidget(label_dot2ExtrusionStepsText , 2, 0 )
layout_extrusionDot2SettingsMenu.addWidget(label_dot2ExtrusionStepsValue, 2, 1 )
layout_extrusionDot2SettingsMenu.addWidget(slider_dot2ExtrusionSteps , 2, 2 )

layout_extrusionDot2SettingsMenu.addWidget(label_dot2RetractionStepsText , 3, 0 )
layout_extrusionDot2SettingsMenu.addWidget(label_dot2RetractionStepsValue, 3, 1 )
layout_extrusionDot2SettingsMenu.addWidget(slider_dot2RetractionSteps , 3, 2 )

layout_extrusionDot2SettingsMenu.addWidget(button_saveAndCloseDot2SettingsMenu, 4, 0 , 1 ,2  )
layout_extrusionDot2SettingsMenu.addWidget(button_closeDot2SettingsMenu, 4, 2, 1 ,2  )

window_extrusionDot2SettingsMenu.setLayout(layout_extrusionDot2SettingsMenu)
# _____________________________________________________________________



# Window: Pick and Place Settings Menu __________
# # Create new window
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
window_pickandPlaceSettingsMenu = createWindow(0, 0, 1,1, 0, subMenuIndentX, subMenuIndentY, windowBackgroundColour ,windowTextColour)

label_pickandPlaceSettingsMenuTitle = createTextLabel(" Pick and Place Settings", textSize_extrusionSettingsMenu , 1 )

label_speedPPText = createTextLabel("Motor Speed: ", textSize_extrusionSettingsMenu , 1 )
label_speedPPValue = createTextLabel(str(speedPP), textSize_extrusionSettingsMenu, 1 )
slider_speedPP = createSlider(speedPP_fnSlider, speedPP, 0 , 100, 10 )

label_encoderTicksPerJumpText = createTextLabel("Encoder Ticks Per Jump: ", textSize_extrusionSettingsMenu , 1 )
label_encoderTicksPerJumpValue = createTextLabel(str(encoderTicksPerJump), textSize_extrusionSettingsMenu, 1 )
slider_encoderTicksPerJump = createSlider(encoderTicksPerJump_fnSlider, encoderTicksPerJump, 0 , 100, 10 )

label_flipAngleText = createTextLabel("Flip Angle: ", textSize_extrusionSettingsMenu , 1 )
label_flipAngleValue = createTextLabel(str(flipAngle), textSize_extrusionSettingsMenu, 1 )
slider_flipAngle = createSlider(flipAngle_fnSlider, flipAngle, 0 , 100, 10 )

button_saveAndClosePickandPlaceSettingsMenu = createPictureButton(saveAndClosePickAndPlaceSettingsMenu, path_saveAndCloseGreen , colourIconSize , colourIconSize )
button_closePickandPlaceSettingsMenu = createPictureButton(closePickAndPlaceSettingsMenu , path_closeR , colourIconSize , colourIconSize )



layout_pickandPlaceSettingsMenu = QGridLayout()

layout_pickandPlaceSettingsMenu.addWidget(label_pickandPlaceSettingsMenuTitle ,0 , 0)

layout_pickandPlaceSettingsMenu.addWidget(label_speedPPText ,1 , 0)
layout_pickandPlaceSettingsMenu.addWidget(label_speedPPValue , 1, 1 )
layout_pickandPlaceSettingsMenu.addWidget(slider_speedPP , 1, 2 )

layout_pickandPlaceSettingsMenu.addWidget(label_flipAngleText , 2, 0 )
layout_pickandPlaceSettingsMenu.addWidget(label_flipAngleValue , 2, 1 )
layout_pickandPlaceSettingsMenu.addWidget(slider_flipAngle , 2, 2 )

layout_pickandPlaceSettingsMenu.addWidget(label_encoderTicksPerJumpText , 3, 0 )
layout_pickandPlaceSettingsMenu.addWidget(label_encoderTicksPerJumpValue , 3, 1 )
layout_pickandPlaceSettingsMenu.addWidget(slider_encoderTicksPerJump , 3, 2 )



layout_pickandPlaceSettingsMenu.addWidget(button_saveAndClosePickandPlaceSettingsMenu, 4, 0 , 1 ,2  )
layout_pickandPlaceSettingsMenu.addWidget(button_closePickandPlaceSettingsMenu, 4, 2, 1 ,2  )


window_pickandPlaceSettingsMenu.setLayout(layout_pickandPlaceSettingsMenu)






# # Window 3 Menu ____________

# window3 = QWidget()
# window3.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
# qr = window3.frameGeometry()
# window3.move(qr.topLeft())
# # window_settingsMenu.setAttribute(Qt.WA_TranslucentBackground)
# # window_settingsMenu.setWindowFlags(Qt.WindowStaysOnTopHint)

# # Image Button 
#     # Create a button, connect it and show it
# button3_1 = QPushButton("")
# button3_1.clicked.connect(openMenu)
#     # Set image in Push Button
# pixmap = QPixmap("menu.png")
# button3_1_icon = QIcon(pixmap)
# button3_1.setIcon(button2_1_icon)
# button3_1.setIconSize(QSize(100,100))
# button3_1.setFlat(1)
# button3_1.clicked.connect(closeMenu)


# button3_2 = QPushButton("Switch Mode")
# button3_2.clicked.connect(openSwitchModeMenu)

# button3_3 = QPushButton("Extruder ")
# button3_3.clicked.connect(openextrusionDot1SettingsMenu)

# mainQuitbutton2 = QPushButton("Quiter")
# mainQuitbutton2.clicked.connect(sys.exit)

# layout3 = QGridLayout()
# layout3.addWidget(button3_1, 0, 0 )
# layout3.addWidget(button3_2, 1, 0 )
# layout3.addWidget(button3_3, 2, 0 )
# layout3.addWidget(mainQuitbutton2, 3, 0 )


# window3.setLayout(layout3)

# ______________________________

# Window 4 SwitchModeMenu ______
# ______________________________

# Window 5 ExtrusionSettingsMenu ______
window5 = QWidget()
window5.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)
qr5 = window5.frameGeometry()
window5.move(qr.topLeft().x()+ menuXdistance*4, qr.topLeft().y()+200)
# window5.move(qr.center())
# window5.move(qr.center().x()-(qr5.width()/2 ),qr.center().y()-(qr5.height()/2))
# window5.move(qr.topRight())
# window5.setAttribute(Qt.WA_TranslucentBackground)

label5_1 = QLabel("Extrusion SpeedXXXX: ")
label5_2 = QLabel(str(extrusionSpeed))




speedSlider = QSlider(Qt.Horizontal, window5)
speedSlider.setGeometry(30, 40, 200, 30)
speedSlider.valueChanged[int].connect(speedSliderChangeValue)
# # button_switchCrossHairs2.setStyleSheet("QPushButton" "{" "background-color : rgb(244, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 




# .QSlider {
#     min-height: 68px;
#     max-height: 68px;
#     background: #5F4141;
# }

# .QSlider::groove:horizontal {
#     border: 1px solid #262626;
#     height: 5px;
#     background: #393939;
#     margin: 0 12px;
# }

# .QSlider::handle:horizontal {
#     background: #22B14C;
#     border: 5px solid #B5E61D;
#     width: 23px;
#     height: 100px;
#     margin: -24px -12px;
# }

button5_1 = QPushButton("Close Menu")
button5_1.clicked.connect(closeExtrusionSettingsMenu)

button5_2 = QPushButton("+")
button5_2.clicked.connect(increaseExtrusionSpeed)

button5_3 = QPushButton("-")
button5_3.clicked.connect(decreaseExtrusionSpeed)

layout5 = QGridLayout()
# item, int row, int column, int rowSpan = 1, int columnSpan = 1, Qt::Alignment alignment = Qt::Alignment())

layout5.addWidget(mainQuitbutton, 0, 0 )
layout5.addWidget(label5_1, 0, 1 )
layout5.addWidget(label5_2, 0, 2 )
layout5.addWidget(button5_3, 0,3)
layout5.addWidget(button5_2, 0,4,)
layout5.addWidget(speedSlider, 2, 0 , 1 , 4 )
layout5.addWidget(button5_1, 1,0)

window5.setLayout(layout5)


# _____________________________________

# IDEA maybe its slow because the camera feed is not on the main window - switch to other option where we open the camera feed as a separate window



loadAllSettings()
mainWindow.show()
quitWindow.show()
mainMenuWindow.show()

# window_settingsMenu.show()
app.exec_()

# See also: https://gist.github.com/bsdnoobz/8464000





# # Create new window
# window_X = createWindow(1,1, 0, 150, 0, windowBackgroundColour ,windowTextColour)
# button_X = createPictureButton(switchCrossHairToEdit , path_crossHairSwitch1 , arrowIconSize , arrowIconSize )
# layout_X = QGridLayout()
# layout_X.addWidget(button_switchCrossHairs_colour , 0, 0 )
# window_X.setLayout(layout_X)







# # Main Window ________________________________
# # Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
# mainWindow = createWindow(0, 0 , 1,0, 1, menuXdistance*1, 0, windowBackgroundColour ,windowTextColour)

# qr = mainWindow.frameGeometry()
# mainWindow.move(qr.topLeft())
# screenHeight= qr.height()
# screenWidth= qr.width()


# # mainWindow.setWindowState(Qt.WindowMaximized)
# # print(mainWindow.x())
# # print(mainWindow.y())
# # print(mainWindow.width())
# # print(mainWindow.height())
# # print(mainWindow.maximumSize())
# # print(mainWindow.frameGeometry())
# # print(screenHeight)
# # print(screenWidth)
# # print(mainWindow.frameGeometry())
# # mainWindow.resize(1*screenWidth, 1*screenHeight)


# # Open CV for Microscope Image _______________
# cap = cv2.VideoCapture(0)
# cap.set(cv2.CAP_PROP_FRAME_WIDTH, 1*screenWidth ) #above 0.7 resizes to maximum 
# cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 1*screenHeight)  #above 0.7 resizes to maximum 

# # cap.set(cv2.CAP_PROP_FRAME_WIDTH, 480)
# # cap.set(cv2.CAP_PROP_FRAME_HEIGHT, 320)

# # timer for getting frames
# timer = QTimer()
# timer.timeout.connect(displayFrame)
# timer.start(60)
# # ___________________________________________


# # mainWindow Assets____________________________
# label = QLabel('No Camera Feed')

# # Quit Button
# mainQuitbutton = QPushButton("Quitter")
# mainQuitbutton.clicked.connect(sys.exit) # quiter button 

# button_switchMode = createPictureButton(switchMode, path_ncModeW, 100 , 100 )
# button_openSettingsMenu = createPictureButton(openSettingsMenu, path_settingsW, 100 , 100 )
# button_switchCrossHair = createPictureButton(switchCrossHair, path_crossHairW, 100 , 100 )
# button_openMenu = createPictureButton(openMenu, path_menuW, 100 , 100 )
#  # Mode 1: ppMode , 0: eMode , 2:Nc Mode 
#  # Extrusion mode: 0: continuous 1: smallPad (dot1) , 2: largePad (dot2) 
# if(mode == 0):
#     if(extrusionMode==0):
#         button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_continuousModeW, 100 , 100 )
#     elif(extrusionMode==1):
#         button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_smallPadModeW, 100 , 100 )
#     elif(extrusionMode==2):
#         button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_smallPadModeW, 100 , 100 )
# else: 
#     button_switchExtrusionMode = createPictureButton(switchExtrusionMode, path_blank, 100 , 100 )


# layout_mainWindow = QGridLayout()
# # layout_mainWindow.addWidget(button_openMenu , 0, 0 )
# layout_mainWindow.addWidget(button_openSettingsMenu, 0, 0)
# layout_mainWindow.addWidget(button_switchCrossHair, 1, 0)
# layout_mainWindow.addWidget(button_switchMode , 2, 0)
# layout_mainWindow.addWidget(button_switchExtrusionMode, 3, 0)
# layout_mainWindow.addWidget(label, 0, 1 , 5, 1 )#To push buttons to top set image collum spacing to number of buttons + 1 

# mainWindow.setLayout(layout_mainWindow)








# def saveAllSettings():

#     # # PP
# # encoderTicksPerJump = 30
# # speedPP = 100
# # flipAngle = 90 

# # savedEncoderTicksPerJump = 30
# # savedSpeedPP = 100
# # savedFlipAngle = 90 

#     print("mode   " + str(  mode    ))

#     print("encoderTicksPerJump    " + str( encoderTicksPerJump    ))
#     print("speedPP   " + str(  speedPP   ))
#     print("flipAngle   " + str(  flipAngle   ))

#     print("savedEncoderTicksPerJump    " + str( savedEncoderTicksPerJump    ))
#     print("savedSpeedPP   " + str(  savedSpeedPP   ))
#     print("savedFlipAngle   " + str(  savedFlipAngle   ))

#     print("extrusionMode   " + str(  extrusionMode  ))

#     print("continuousExtrusionSpeed    " + str( continuousExtrusionSpeed    ))
#     print("continuousRetractionSteps    " + str(continuousRetractionSteps     ))

#     print("dot1ExtrusionSpeed    " + str(dot1ExtrusionSpeed     ))
#     print("dot1ExtrusionSteps    " + str(dot1ExtrusionSteps     ))
#     print("dot1RetractionSteps    " + str(dot1RetractionSteps     ))

#     print("dot2ExtrusionSpeed    " + str(dot2ExtrusionSpeed     ))
#     print("dot2ExtrusionSteps    " + str(dot2ExtrusionSteps     ))
#     print("dot2RetractionSteps    " + str(dot2RetractionSteps     ))

#     print("savedContinuousExtrusionSpeed    " + str( savedContinuousExtrusionSpeed    ))
#     print("savedContinuousRetractionSteps    " + str(savedContinuousRetractionSteps     ))

#     print("savedDot1ExtrusionSpeed    " + str(savedDot1ExtrusionSpeed     ))
#     print("savedDot1ExtrusionSteps    " + str(savedDot1ExtrusionSteps     ))
#     print("savedDot1RetractionSteps    " + str(savedDot1RetractionSteps     ))

#     print("savedDot2ExtrusionSpeed    " + str(savedDot2ExtrusionSpeed     ))
#     print("savedDot2ExtrusionSteps    " + str(savedDot2ExtrusionSteps     ))
#     print("savedDot2RetractionSteps    " + str(savedDot2RetractionSteps     ))

#     print("to do save all settings")
