import serial
import time
import re

# Change this to your serial port if needed - you can see the correct port in Arduino IDE
serialPort = '/dev/ttyUSB0'
serialSpeed = 115200 # ~960 bytes per second, a good default
txString = b'app mode test\r\n'
rxString = b''

try:
    serialObj = serial.Serial( 
        serialPort,
        baudrate = serialSpeed,
        bytesize=8,
        parity = 'N',
        stopbits = 1,
        timeout=5
    ) 
except OSError:
    print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
    exit( 1 )

print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

try:
    while True:
        if serialObj.write( txString ):
            print( "[*] Sent 'PING!', waiting for reply..." ) ;
        rxString = serialObj.readline() 
        rxString = rxString.rstrip()
        # ...
except OSError:
    print( "ERROR:  on %s - exiting!" % serialPort )
    exit( 1 )