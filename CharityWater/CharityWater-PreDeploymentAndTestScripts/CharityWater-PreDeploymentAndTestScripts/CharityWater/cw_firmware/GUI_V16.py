from subprocess import run
from PySide2.QtCore import *
from PySide2.QtGui import *
import sys # for exiting
from PySide2.QtWidgets import *
from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtCore import QSize
import pandas
import serial
import time
import os
import RPi.GPIO as GPIO
import serial
import re
import multiprocessing
from csv import writer
from csv import DictWriter
import csv 
from datetime import datetime
import Adafruit_ADS1x15

import easygui
import time
from datetime import datetime
import csv
import imaplib
import email
from email.header import decode_header
import webbrowser

# ______ Variables that need attention on deployment_______
numberOfEmailsToFetch = 10 # ## Change to higher number ### The number of most resent emails that will be fetched from the inbox 
email_appWaitTime = 30  #Set between 30 and 600 -  The amount of time the board will wait after turnon to send confirmation email to the server. Brad suggested 10mins 600's 


# Testing Config Variables - Set all to 0 for real word deployment use - (See testSlot for no user input testing options)
test_ProgramBoard_off = 0
test_useFakeQRCode = 0
test_ManufacturingCompleteTests_off = 0 
test_RunOtiiTestWithOutUserInput = 0 
test_runSerialTests_off = 0 
test_alowTimeToSendEmail = 0

# __________Global Variables________________
current_csv_file_name = ''
isFirstEntry = 1 
row_dict = dict()
testStatusLableArray =['Waiting - 等待   ']*50
targetMaxSleepCurrent = 0.000130
targetMinSleepCurrent = 0.000106
otiiTestRetryCount = 0
verify_email_dict = dict()
username = "charitywater@rpdintl.com"
password = "RPDcharity:water1"



# __________Set up ADC's_______________
adc1 = Adafruit_ADS1x15.ADS1015(address=0x48, busnum=1)
adc2 = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)
adc3 = Adafruit_ADS1x15.ADS1015(address=0x4B, busnum=1)
GAIN = 2/3

# ___________Setup Serial UART's_______
# You need to connect these devices to the Pi after the Pi is turned on but before running the program.
# They need to be connected the in the right order so the USB port numbers match the list below. 
# Wait several seconds between connecting each one. Start with AM_CLI then AM_LOG then SSM_LOG. 
# Use the command 'ls /dev' on the Pi to find the usb ports and confirm they are connected correctly. 
port_AM_CLI = '/dev/ttyUSB0'
port_AM_LOG = '/dev/ttyUSB1'
port_SSM_LOG = '/dev/ttyUSB2'
COM_PI_PORT = '/dev/ttyUSB3'

# _______________Setup GPIOs_____________
in1 = 16
in2 = 18

GPIO.setmode(GPIO.BOARD)
GPIO.setup(in1, GPIO.OUT)
GPIO.setup(in2, GPIO.OUT)

# Setting GPIO Outputs to True deactiveated the Realys
GPIO.output(in1, True)
GPIO.output(in2, True)

#__________ Fixed GUI Parameters ____________________
white = (244,244,244)
red = (227,6,19)
yellow = ( 237, 255, 0)
green = ( 0,150 ,64 )
blue = (0,159  ,277 )
purple = ( 102,36,131 )
black= (0,0,0)

windowTextColour = QColor(0,0,0) 
windowBackgroundColour = QColor(244,244,244)

path_quit = "images/quit.png"
path_rpd = "images/rpd.png"
path_go = "images/go.png"
path_touchPads = "images/touchPads.png"
path_moveBoard = "images/moveBoard.png"
path_verified = "images/verified.png"
path_notVerified = "images/notVerified.png"


# ________GUI Functions_________
def createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour ):
    window = QWidget()
    if(isFrameless == 1 and isStaysOnTop == 0  ):
        window.setWindowFlags(Qt.FramelessWindowHint)
    elif(isFrameless == 0 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.WindowStaysOnTopHint)
    elif(isFrameless == 1 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)

    if(isTranslucent == 1  ):
        window.setAttribute(Qt.WA_TranslucentBackground)

    if(isMaximised == 1):
        window.showMaximized()
    elif(isMaximised== 0 ):
        qr = window.frameGeometry()
        window.move(qr.topLeft().x()+ xDistance, qr.topLeft().y()+ yDistance)
    
    # Change background colour 
    palette = window.palette()
    palette.setColor(QPalette.Window, backgroundColour)
    palette.setColor(QPalette.WindowText, textColour)
    window.setPalette(palette)
    window.setAutoFillBackground(True)

    if(useStyleSheet == 1 ):
        window.setStyleSheet(
            "QWidget" 
            "{" 
            "border: 100px white;"
            "border-radius: 10px;"
            "background-color: black"
            "}"
        )

    return window

def createPictureLabel(holdingText , imagePathName, sizeX , sizeY):
    labelName = QLabel("holdingText")
    pixmap = QPixmap(imagePathName)
    #  get label dimensions
    w = sizeX #settingsMenuLabel.width()
    h = sizeY #settingsMenuLabel.height()
    #  set a scaled pixmap to a w x h window keeping its aspect ratio 
    if(sizeX==0 and sizeY ==0):
        labelName.setPixmap(pixmap)
    else:
        labelName.setPixmap(pixmap.scaled(w,h))
    return labelName


def createPictureButton(functionName, imagePathName, sizeX , sizeY):
    buttonName = QPushButton("")
    buttonName.clicked.connect(functionName)
    # Set image in Push Button
    pixmap = QPixmap(imagePathName)
    buttonName_icon = QIcon(pixmap)
    buttonName.setIcon(buttonName_icon)
    buttonName.setIconSize(QSize(sizeX,sizeY))
    buttonName.setFlat(1)
    buttonName.setStyleSheet("QPushButton" "{" "background-color : rgb(0, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 
    return buttonName

def createTextLabel(text , fontSize, isBold ):
    label =  QLabel(str(text))
    font = label.font()
    font.setPointSize(fontSize)
    if(isBold == 1): 
        font.setBold(1)
    label.setFont(font)
    return label

def showTestStatusWindow():
    testStatusWindow.show()

def updateAllLables(): 
    global testStatusLableArray 
    label_test_status_1.setText(testStatusLableArray[1])
    label_test_status_1.repaint()

    label_test_status_2.setText(testStatusLableArray[2])
    label_test_status_2.repaint()

    label_test_status_3.setText(testStatusLableArray[3])
    label_test_status_3.repaint()
    
    label_test_status_4.setText(testStatusLableArray[4])
    label_test_status_4.repaint()
    
    label_test_status_5.setText(testStatusLableArray[5])
    label_test_status_5.repaint()
    
    label_test_status_6.setText(testStatusLableArray[6])
    label_test_status_6.repaint()
    
    label_test_status_7.setText(testStatusLableArray[7])
    label_test_status_7.repaint()
    
    label_test_status_8.setText(testStatusLableArray[8])
    label_test_status_8.repaint()
    
    label_test_status_9.setText(testStatusLableArray[9])
    label_test_status_9.repaint()
    
    label_test_status_10.setText(testStatusLableArray[10])
    label_test_status_10.repaint()
    
    label_test_status_11.setText(testStatusLableArray[11])
    label_test_status_11.repaint()
    
    label_test_status_12.setText(testStatusLableArray[12])
    label_test_status_12.repaint()
    
    label_test_status_13.setText(testStatusLableArray[13])
    label_test_status_13.repaint()
    
    label_test_status_14.setText(testStatusLableArray[14])
    label_test_status_14.repaint()
    
    label_test_status_15.setText(testStatusLableArray[15])
    label_test_status_15.repaint()
    
    label_test_status_16.setText(testStatusLableArray[16])
    label_test_status_16.repaint()
    
    label_test_status_17.setText(testStatusLableArray[17])
    label_test_status_17.repaint()
    
    label_test_status_18.setText(testStatusLableArray[18])
    label_test_status_18.repaint()
    
    label_test_status_19.setText(testStatusLableArray[19])
    label_test_status_19.repaint()
    
    label_test_status_20.setText(testStatusLableArray[20])
    label_test_status_20.repaint()

    label_test_status_21.setText(testStatusLableArray[21])
    label_test_status_21.repaint()
    
    label_test_status_22.setText(testStatusLableArray[22])
    label_test_status_22.repaint()
    
    label_test_status_23.setText(testStatusLableArray[23])
    label_test_status_23.repaint()
    
    label_test_status_24.setText(testStatusLableArray[24])
    label_test_status_24.repaint()
    
    label_test_status_25.setText(testStatusLableArray[25])
    label_test_status_25.repaint()
    
    label_test_status_26.setText(testStatusLableArray[26])
    label_test_status_26.repaint()
    
    label_test_status_27.setText(testStatusLableArray[27])
    label_test_status_27.repaint()
    
    label_test_status_28.setText(testStatusLableArray[28])
    label_test_status_28.repaint()
    
    label_test_status_29.setText(testStatusLableArray[29])
    label_test_status_29.repaint()
    
    label_test_status_30.setText(testStatusLableArray[30])
    label_test_status_30.repaint()
    
    label_test_status_31.setText(testStatusLableArray[31])
    label_test_status_31.repaint()
    
    label_test_status_32.setText(testStatusLableArray[32])
    label_test_status_32.repaint()
    
    label_test_status_33.setText(testStatusLableArray[33])
    label_test_status_33.repaint()
    
    label_test_status_34.setText(testStatusLableArray[34])
    label_test_status_34.repaint()
    
    label_test_status_35.setText(testStatusLableArray[35])
    label_test_status_35.repaint()
    
    label_test_status_36.setText(testStatusLableArray[36])
    label_test_status_36.repaint()
    
    label_test_status_37.setText(testStatusLableArray[37])
    label_test_status_37.repaint()

    label_test_status_37.setText(testStatusLableArray[37])
    label_test_status_37.repaint()
    
    label_test_status_38.setText(testStatusLableArray[38])
    label_test_status_38.repaint()

    label_test_status_39.setText(testStatusLableArray[39])
    label_test_status_39.repaint()

    label_test_status_40.setText(testStatusLableArray[40])
    label_test_status_40.repaint()

    label_test_status_41.setText(testStatusLableArray[41])
    label_test_status_41.repaint()

    label_test_status_42.setText(testStatusLableArray[42])
    label_test_status_42.repaint()

    label_test_status_43.setText(testStatusLableArray[43])
    label_test_status_43.repaint()

    label_test_status_44.setText(testStatusLableArray[44])
    label_test_status_44.repaint()
    
    
        
def updateLable(index):
    global testStatusLableArray
    global test_Success
    if(test_Success):
        testStatusLableArray[index]= 'Success 成功'
    else:
        testStatusLableArray[index]= 'Fail 失'
    updateAllLables()

# _______User Input (QR Code Functions)_________
# Will allow the user to input a QR code via a scanner or keyboard 
def enterQRCode():
    qrCodeInput, ok = QInputDialog.getText(userInputWindow, 'Text Input Dialog', 'Please Scan the QR code - 扫 QR码')
    if ok:
        if len(str(qrCodeInput))==6 :
            print('QR Code: ', qrCodeInput)
            row_dict['QR Code']= str(qrCodeInput)
        else:
            print('QR Code is the wrong length')
            enterQRCode()
    else:
        print('Error QR Code Not Logged')
        enterQRCode()
 

def enterQRCodeRandom():
    qrCodeInput = 123456
    print('QR Code: ', qrCodeInput)
    row_dict['QR Code']= str(qrCodeInput)  



# _________Serial Functions_____________
# Will connect a serial port at a given baude Speed 
def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

# Will write a txString to a serial port 
def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
            print( "Write Complete: " + str(txString) )
            print( "" )
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

# # Will continuously read the serial port - depreciated 
# def readForeverSerial(serialPort):
#     rxString = b''
#     try:
#         while True:
#             rxString = serialPort.readline() 
#             rxString = rxString.rstrip()
#             # print(rxString)
#             # ...
#     except OSError:
#         print( "ERROR:  on %s - exiting!" % serialPort )
#         exit( 1 )

# Will read the SSM Serial port until the expected response is found 
def readUnitlSerial_SSM(serialPort ,expectedResponse, return_dict, isMultilineResponse, expectedNoOfLines):
    rxString = b'xxxxyyyyxxxx' # needs to be set to something the board will never print out can't be blank ""
    multiLineReturnString = ''
    counter = 0 
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            print(rxString)
            if expectedResponse in rxString: 
                if isMultilineResponse == 0:
                    isFound =1
                    return_dict[1] = rxString
                elif(isMultilineResponse == 1):
                    multiLineReturnString = multiLineReturnString + '  -' + str(rxString).replace("b'", "").replace("'", "").replace("r", "").replace("\\", "")
                    counter = counter + 1 
                    print('counter: ', counter)
                    print('Response: ', multiLineReturnString)
            else:
                isFound = 0 
            if isMultilineResponse == 1:
                if expectedNoOfLines == counter:
                    isFound =1
                    return_dict[1] = multiLineReturnString
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# Will read the AM Serial port until the expected response is found 
def readUnitlSerial_AM(serialPort ,expectedResponse, return_dict):
    response = b'xxxxxxxx' # needs to be set to something the board will never print out can't be blank ""
    rxString = b'xxxxyyyyxxxx' # needs to be set to something the board will never print out can't be blank ""
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            print(rxString)
            rxString = rxString.rstrip()
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            if expectedResponse in rxString: 
                isFound =1
                return_dict[1] = response
            else:
                isFound = 0 

    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

# Will read the Serial port until the expected response is found 
def readUnitlSerial_otii(serialPort ,expectedResponse, return_dict):
    rxString = b'xxxxyyyyxxxx' # needs to be set to something the board will never print out can't be blank "")
    counter = 0 
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            print(rxString)
            if(rxString != b''):
                if expectedResponse in rxString:
                    return_dict[1] = rxString
                    isFound =1
                else:
                    isFound = 0 
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# ______ Serial Test Functions with timeout _________
def runTest_SSM(timeOutTime, command, expectedResponse , isMultilineResponse, expectedNoOfLines):
    # print(str(command))
    global row_dict 
    global test_Success 
    test_Success = 0 
    writeSerial(SSM_LOG, command)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    p = multiprocessing.Process(target=readUnitlSerial_SSM, args=(SSM_LOG,expectedResponse, return_dict, isMultilineResponse, expectedNoOfLines))
    p.start()
    p.join(timeOutTime) #will time out after X seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out SSM command', command)
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "").replace("\\", "")]= 'FAIL - Test Timed Out SSM command'
        test_Success = 0 
    else:
        print('Success for SSM command:', command, '  RxString :', return_dict[1] )
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "")]= (str(return_dict[1])).replace("b'", "").replace("'", "").replace("\r\n", "")
        test_Success = 1

def runTest_AM(timeOutTime, command, expectedResponse):
    # print(str(command))
    global row_dict 
    global test_Success 
    test_Success = 0 
    writeSerial(AM_CLI, command)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    # print('Check it gets here')
    p = multiprocessing.Process(target=readUnitlSerial_AM, args=(AM_LOG,expectedResponse, return_dict))
    p.start()
    p.join(timeOutTime) #will time out after X seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out AM command', command)
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "")]= 'FAIL - Test Timed Out AM command'
        test_Success = 0 
    else:
        print('Success for AM command:', command,  '  Response: ', return_dict[1] )
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "")]= (str(return_dict[1])).replace("b'", "").replace("'", "").replace("\r\n", "")
        test_Success = 1 


# Repeat function function 
def repeatTest_AM(numberOfRepeats , doRestartLeft , doRestartRight, function, timeOutTime, command, expectedResponse): # numberOfRepeats , doRestartLeft , doRestartRight):
    global test_Success
    test_Success = 0 
    # Try the finction the first time
    function(timeOutTime, command, expectedResponse)
    for i in range (numberOfRepeats-1):
        # If the function fails try again 
        if test_Success == 0 : 
            # If the board is on the left side restart it 
            if doRestartLeft == 1 : 
                powerCycleBoard()
                # Re-enable test mode and log 
                enableTestModeAndLog()
    
                for x in range (3):
                    if test_Success == 0 :
                        enableTestModeAndLog()
            # If the board is on the right side restart it 
            if doRestartRight == 1: 
                otiiOff()
                time.sleep(5)
                otiiOn()
                time.sleep(15)
                enableTestModeAndLog()
                for x in range (3):
                    if test_Success == 0 :
                        enableTestModeAndLog()
            test_Success = 0
            # Retry the function
            print('Function '+ str(command) +' Retry Attempt: ' + str(i+1))
            function(timeOutTime, command, expectedResponse)

def repeatTest_SSM(numberOfRepeats , doRestartLeft , doRestartRight, function, timeOutTime, command, expectedResponse, isMultilineResponse, expectedNoOfLines): # numberOfRepeats , doRestartLeft , doRestartRight):
    global test_Success
    test_Success = 0 
    # Try the finction the first time
    function(timeOutTime, command, expectedResponse, isMultilineResponse, expectedNoOfLines)
    for i in range (numberOfRepeats-1):
        # If the function fails try again 
        if test_Success == 0 : 
            # If the board is on the left side restart it 
            if doRestartLeft == 1 : 
                powerCycleBoard()
                # Re-enable test mode and log 
                enableTestModeAndLog()
    
                for x in range (3):
                    if test_Success == 0 :
                        enableTestModeAndLog()
            # If the board is on the right side restart it 
            if doRestartRight == 1: 
                otiiOff()
                time.sleep(5)
                otiiOn()
                time.sleep(15)
                enableTestModeAndLog()
                for x in range (3):
                    if test_Success == 0 :
                        enableTestModeAndLog()
            test_Success = 0
            # Retry the function
            print('Function '+ str(command) +' Retry Attempt: ' + str(i+1))
            function(timeOutTime, command, expectedResponse, isMultilineResponse, expectedNoOfLines)



#__________ ADC Functions __________
def readADC(testName, adc , channel, max, min):
    global row_dict
    global test_Success 
    time.sleep(0.1)
    total = 0 
    for i in range(10):
        # result = (3.295/1648)*(adc.read_adc(channel, gain=GAIN))#use if GAIN =1 
        # To measure higher than 3.8V you need to suppy ADC's with 5V and set gain to 2/3
        # Logic level converter is then needed on the I2C lines to isolate 5V from Pi GPIO's
        result = (3/1000)*(adc.read_adc(channel, gain=GAIN))#use if GAIN =2/3
        time.sleep(0.01)
        print('The voltage is: ' , result)
        total= total + result
    average = round(total/10,2)
    print(average)

    if (average > min and average < max):
        message = ('Success: '+ str(average) +'V')
        row_dict[str(testName)]= message
        print(message)
        test_Success = 1
    elif(average < min ): 
        message = ('Fail: '+ str(average) +'V is less than ' +str(min)+'V')
        row_dict[str(testName)]= message
        print(message)
        test_Success = 0 
    elif(average >max):
        message = ('Fail: '+ str(average) +'V is more than ' +str(max)+'V')
        row_dict[str(testName)]= message
        print(message)
        test_Success = 0 

# ________CSV Functions___________
def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

def append_dict_as_row(file_name, dict_of_elem, field_names):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names)
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)
        write_obj.close()

def writeFeilddNames(file_name,  field_names):
    with open(file_name, 'w', encoding='UTF8', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=field_names)
        writer.writeheader()
        f.close()  

def createNewCSVFilewithHeaders():
    global row_dict 
    global current_csv_file_name
    
    # csv_field_names = ['Time','WasProgrammed','Test1','Test2','Test3']
    # datetime object containing current date and time
    now = datetime.now()
    current_csv_file_name =  ('Program Boards - Log: ' + now.strftime("%d-%m-%Y %H:%M:%S") + '.csv')
    writeFeilddNames(current_csv_file_name, row_dict.keys())
    print("CSV File created with name", current_csv_file_name)

def appendLineToCSVFile():
    global row_dict 
    global current_csv_file_name

    print(row_dict)
    # Append a dict as a row in csv file
    append_dict_as_row(current_csv_file_name, row_dict, row_dict.keys())

def resetDictionary():
    global row_dict
    # Need to reset the dictionary values to zero to erase data from the previous board 
    row_dict = {x: 0 for x in row_dict}
    print(row_dict)

 
# ___________Board Interaction Functions___________

# Depreciated - Simple program Board 
# def simpleProgramBoard(): 
#     test = os.system("sudo openocd")#change the contents of openocd.cfg to change the files to be flashed
#     print("it ran")
#     print("Please wait approx 5 mins")
#     readUnitlSerial_AM(AM_LOG ,'Programmed SSM with image. Good to go')

# Will deactivate the board power supply unsing the realy 
def powerCycleBoard():
    powerOffBoard()
    print("Board power off - check relay is deactivated")
    time.sleep(3)
    powerOnBoard()
    print("Board power on - check relay is activated")
    runTest_AM( 15, b'Power Cycling Board\r\n', b'CLI Initialized')

# To be used if a board reaches the end of the test cycle and it has been put in manufacturing mode and needs to be reset to rerun tests
def nandEraseAll():
    runTest_AM( 5, b'nand eraseall\r\n', b'Finished Erasing FLASH')
    
def powerOffBoard():
    GPIO.output(in1, True)
    print("Board power off - check relay is deactivated")
    
def powerOnBoard():
    GPIO.output(in1, False)
    print("Board power on - check relay is activated ")

    
# Connects the short between TP87 and TP88
def connectShort():
    GPIO.output(in2, False)

def disconnectShort():
    GPIO.output(in2, True)

# ________Otii Control Functions_____
def otiiOn():
    command = b'run =otiiOn\r\n'
    writeSerial(COM_PI_SERIAL, command)

def otiiOff():
    command = b'run =otiiOff\r\n'
    writeSerial(COM_PI_SERIAL, command)

def otiiTest():
    command = b'run =otiiTest\r\n'
    writeSerial(COM_PI_SERIAL, command)


# Sends program commands using Open OCD and verifies the board has started programming by listening to AM_LOG
def verifyStartProgramming(expectedResponse, return_dict):
    # Program the Board using Open OCD
    programBoard = os.system("sudo openocd")#change the contents of openocd.cfg to change the files to be flashed
    
    # Give the board time to flash the program 
    # time.sleep(10)# To do - we could replace this with a read for *** VErified Programing *** read out from the ST Link 
    # print('Here')
    # Then remove the short and the readout will start to come through on AM_LOG 
    disconnectShort()

    # Then try to detect that the programming has happened correctly by listening for Copying internal flash to external
    response = b'xxxxxxxx'
    rxString = b'xxxxxxxx'
    isFound = 0
    # Verify programming Started Sucsessfully 
    try:
        while (isFound == 0):
            rxString = AM_LOG.readline() 
            print(rxString)
            rxString = rxString.rstrip()
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            if expectedResponse in rxString: 
                isFound =1
                return_dict[1] = response
            else:
                isFound = 0 
    except OSError:
        print( "ERROR:  on %s - exiting!" % AM_LOG )
        exit( 1 )



def programBoard():
    global row_dict 
    global test_Success
    test_Success = 0 
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    # print('Check it gets here')
    hasStarted=0
    label_test_status_1.setText('Processing')
    label_test_status_1.repaint()

    # Start Programming and Verify programming has started 
    p = multiprocessing.Process(target=verifyStartProgramming, args=(b'now writing zeros to Internal Addr', return_dict))
    p.start()
    p.join(120) #will time out after 2 mins 
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Start Programing Board Timed Out')
        row_dict['Start Programing Board']= 'FAIL - Start Programing Board Timed Out'
    else:
        print('Success - Start Programing Board: ', str(return_dict[1]) )
        row_dict['Start Programing Board']= return_dict[1]
        hasStarted = 1 

    # Verify the programming Finishes correctly
    if(hasStarted):

        # Update Verify Start Programming Label 
        test_Success=1
        updateLable(1)
        
        label_test_status_2.setText('Processing')
        label_test_status_2.repaint()
        # Verify programming is complete
        p = multiprocessing.Process(target=readUnitlSerial_AM, args=(AM_LOG, b'Programmed SSM with image. Good to go', return_dict))
        p.start()
        p.join(10*60) #will time out after 10 mins 
        # If thread is still active
        if p.is_alive():
            # print ("Test is still running - kill it...")
            # Terminate - may not work if process is stuck for good
            p.terminate()
            # OR Kill - will work for sure, no chance for process to finish nicely however
            # p.kill()
            label_test_status_2.setText('Fail')
            label_test_status_2.repaint()
            print('FAIL - Finish Program Board Timed Out')
            row_dict['Finish Program Board']= 'FAIL - Finish Programing Board Timed Out'
            print('Programming did not finish successfully, please try again') 
            test_Success = 0
            updateLable(2)
        else:
            test_Success = 1
            updateLable(2)
            print('Success - Finished Programming Board: ', str(return_dict[1]) )
            row_dict['Finish Program Board']= return_dict[1]
    else:
        print('Programming did not start successfully, please try again')
        test_Success = 0
        updateLable(1)

# ________Voltage Tests______________
def runVoltageTests():
    showTestStatusWindow()
    time.sleep(1)

    readADC('2V8_MAG', adc1 , 0, 2.9 , 2.7 )
    updateLable(3)

    readADC('3v6_BAT', adc1 , 1,3.7 , 3.5)
    updateLable(4)
    
    readADC('2V8_DIST', adc1 , 2,2.9 , 2.7)
    updateLable(5)
    
    readADC('2V8', adc1 , 3,2.9 , 2.7)
    updateLable(6)
    
    readADC('2V8_AP_FLASH', adc2 , 0,2.9 , 2.7)
    updateLable(7)
    
    readADC('2V8_EEPROM', adc2 , 1,2.9 , 2.7)
    updateLable(8)
    
    readADC('2V8_MSP', adc2 , 2,2.9 , 2.7)
    updateLable(9)
     
    readADC('2V8_GPS', adc3 , 0, 0.3 , 0)
    updateLable(10)
    
    readADC('4_GPRS - OFF', adc3 , 1, 0.3 , 0)
    updateLable(11)
    
    readADC('2V8_STM', adc3 , 2,2.9 , 2.7)
    updateLable(12)
     
    readADC('2V8_TEMP', adc3 , 3,2.9 , 2.7)
    updateLable(13)
    
# _________Serial Tests_______________
def enableTestModeAndLog():
    # runTest_AM(10, b'app mode test\r\n' , b'Test mode on' )
    # runTest_SSM(10,  b'log enable\r\n' , b'Enabled logging'  )
    global test_Success
    runTest_AM(5, b'app mode test\r\n', b'Test mode on' )
    AM_Success = test_Success
    runTest_SSM(5, b'log enable\r\n', b'Enabled logging',0 ,0 )
    SSM_Success = test_Success

    if(AM_Success and SSM_Success):
        test_Success = 1
        updateLable(14)
    else:
        test_Success = 0 
        updateLable(14)

def runAllSerialTests():
    allSerialTestsSuccessful = 1
    # Flash
    # repeatTest_AM(3, 1, 0 , runTest_AM, 25, b'conn on primary\r\n' , b'primary antenna')
    # repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'eep pattern write\r\n' , b'Write Xcomplete',0 ,0)
    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'nand pattern write\r\n' , b'Done writing pattern' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0 
    updateLable(15)

    repeatTest_AM(3, 1, 0,runTest_AM, 5,  b'nand pattern read\r\n' , b'Pass' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0 
    updateLable(16)

    # EEPROM
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'eep pattern write\r\n' , b'Write complete',0 ,0)
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(17)

    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'eep pattern read\r\n' , b'Pass',0 ,0)
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(18)

    # Fule Gauge
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'batt serial\r\n' , b'0x' ,0 ,0) #0x3639A975030000EC
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(19)

    # Battery Voltage Measurement 
    repeatTest_SSM(3, 1, 0, runTest_SSM,  5 ,  b'batt read\r\n' , b'Current Batt Voltage',0 ,0  ) #Current Batt Voltage: 3625 mV
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(20)

    # RTC 
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'rtc st 0 0 0 1 1 1 1 2021\r\n' ,b'HW_RTC: Time set' ,0 ,0 ) # Need to create the correct date and time roughly
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(21)

    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'rtc rt\r\n' , b'HW_RTC Time' ,0 ,0 ) #HW_RTC Time: Sunday, January 1, 2021 - 01:01:53.15
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(22)

    # Cellular Modem
    time.sleep(5)
    repeatTest_AM(3, 1, 0, runTest_AM, 30,  b'conn on primary\r\n' , b'primary antenna' ) #seems to cause a restart
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(23)

    readADC('4_GPRS - ON', adc3 ,1 , 4.1, 3.9 )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(24)

    # time.sleep(4)

    repeatTest_AM(3, 1, 0, runTest_AM, 15,  b'conn iccid\r\n' , b'CCID:' ) 
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(25)

    repeatTest_AM(3, 1, 0, runTest_AM, 15,  b'conn imei\r\n' , b'IMEI' )# Found IMEI: 358887095625834
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(26)

    repeatTest_AM(3, 1, 0, runTest_AM, 15,  b'conn version\r\n' , b'23.60' )# Needs to match the version number of the modem 
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(27)
    
    repeatTest_AM(3, 1, 0, runTest_AM, 30,  b'conn tx 9612 22\r\n' , b'+UTEST:' )# There are other options we may need to try
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(28)

    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'conn off\r\n' , b'Turned off modem' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(29)

    time.sleep(20)

    repeatTest_AM(3, 1, 0, runTest_AM, 30,  b'conn on secondary\r\n' , b'secondary antenna' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(30)

    repeatTest_AM(3, 1, 0, runTest_AM, 30,  b'conn tx 9612 21\r\n' , b'+UTEST:' )# There are other options we may need to try
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(31)

    # GPS
    repeatTest_AM(3, 1, 0, runTest_AM, 20,  b'gps on\r\n' , b'Gps is ON - stay tuned for location info' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(32)

    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'gps print\r\n' , b'Received GPGGA message from GPS' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(33)

    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'gps off\r\n' , b'Gps is OFF' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(34)
    
    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'ssm status\r\n' , b'SSM-AM SPI test pass' )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(35)

    # Temp and Humidity
    # To do - multiline response 
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'env sample\r\n' , b'HW_ENV',1 ,2 ) # HW_ENV: Temperature: 27øC /new line/ HW_ENV: Humidity: 52%
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(36)

    # Magnetometer
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'mag on\r\n' , b'Magnetometer is on' ,0 ,0 )
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(37)
    
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'mag sample\r\n' , b'MAG' ,0 ,0  ) #MAG X: -233, MAG Y: -89, MAG Z: 537 MAG TEMP: 25 C
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(38)

    # Cap sense pads 
    repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'algo rawdata\r\n' , b'Pad' ,1 ,8  ) # are there certain values we need to check? 
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(39)
   
    # Crypto Chip 
    repeatTest_AM(3, 1, 0, runTest_AM, 5,  b'cryp serial\r\n' , b'Crypto ID' )# Crypto ID: 01235d52e7fe319301
    if test_Success == 0: 
        allSerialTestsSuccessful = 0
    updateLable(40)

    if allSerialTestsSuccessful == 1 : 
        print('All serial tests were successful ')
        # Turn off Power to board 
        powerOffBoard() 

        otiiTestRetryCount = 0 
        # Instruct the user to move the board to run Otii Tests
        if (test_RunOtiiTestWithOutUserInput != 1):
            moveBoardWindow.show()
        else: 
            prepare_runTest_otii()
    else: 
        print('At lest one serial test has failed')
        reRunSerialTestsWindow.show()

# Prepares the Otii and puts the board into sleep mode (After Board has been moved to other side of the Jig)
def prepare_runTest_otii():
    global test_Success
    # Make sure the Left hand side Jig is turned off - Essencial for Liams setup, not important for Jig 
    powerOffBoard()
    # Make sure the otii is turned off to start with 
    otiiOff()
    # Wait 5's for Otti to discharge fully 
    time.sleep(5)
    # Tell Otii to start 
    otiiOn()
    # Wait 10's for Otti to start up the board 
    time.sleep(5)
    # Put the Otii in sleep mode
    test_Success = 0 

    for x in range (3):
        if test_Success == 0: 
            repeatTest_AM(3, 0, 1, runTest_AM,5, b'pwr sleep\r\n' , b'Putting AM to sleep' )

    # From this window the user can confirm they have moved the board 
    # The slot will then call the runTest_otii function
    if (test_RunOtiiTestWithOutUserInput != 1):
        touchPadsWindow.show() 
    else: 
        runTest_otii(20, b'run =otiiTest\r\n',  b'average current') 



# Gets a current reading for the board from the Otii 
def runTest_otii(timeOutTime, command, expectedResponse):
    global test_Success 
    global targetSleepCurrent
    global targetMaxSleepCurrent
    global targetMinSleepCurrent
    global otiiTestRetryCount
    test_Success = 0 
    # Send the start power test command to the Otii 
    otiiTest() 
    # Listen for the response on the serial
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    p = multiprocessing.Process(target=readUnitlSerial_otii, args=(COM_PI_SERIAL,expectedResponse, return_dict))
    p.start()
    p.join(timeOutTime) #will time out after X seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out SSM command', command)
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "").replace("\\", "")]= 'FAIL - Test Timed Out SSM command'
        test_Success = 0 
    else:
        print('Success for Retriving Current Value from Otii:', command, '  RxString :', return_dict[1] )
        split_list = str(return_dict[1] ).split("=")
        currentValue= float(split_list[1].replace(" ", "").replace("'", ""))
        # print('currentValue = ', str(currentValue))
        if(currentValue<targetMinSleepCurrent or currentValue>targetMaxSleepCurrent ):
            print('Fail - current is outside the required range = ', str(currentValue) )
            row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "").replace("\\", "")]= ('Fail - current is outside the required range = '+ str(currentValue) + '(A)' )
            test_Success = 0 
        else:
            print('Success - current is within the required range = ', str(currentValue) )
            row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "").replace("\\", "")]= ('Success - current is within the required range = '+ str(currentValue) + '(A)')
            test_Success = 1 
            updateLable(41)

    # If current is wrong, prepate test 3 times  
    if otiiTestRetryCount<3: 
        print('Otii Test Attempt number:', otiiTestRetryCount)
        otiiTestRetryCount = otiiTestRetryCount + 1 
        if test_Success == 1: 
            updateLable(40)
            prepareOtiiForManufacturingCompleteTests()
        else: 
            print('need to repeat Otii Test')
            prepare_runTest_otii()
    else: 
        updateLable(40)
        prepareOtiiForManufacturingCompleteTests()
        

def prepareOtiiForManufacturingCompleteTests():
    global isFirstEntry 
    global row_dict 
    global testStatusLableArray
    global test_Success

    global test_ManufacturingCompleteTests_off

    enableTestModeAndLog()
    for x in range (3):
        if(test_Success == 0):
            otiiOff()
            time.sleep(5)
            otiiOn()
            time.sleep(15)
            enableTestModeAndLog()
            print('Enable Test Mode and Log Attempt number:', x)
        

    # Run Final Verification Tests  
    if test_ManufacturingCompleteTests_off != 1: 
        runManufacturingCompleteTests()


def runManufacturingCompleteTests():
    global isFirstEntry 
    global row_dict 
    global testStatusLableArray
    global test_Success
    global test_alowTimeToSendEmail
    global email_appWaitTime 

    allManufacturingCompleteTestsSuccessful = 1
    # Wathdog
    repeatTest_SSM(3, 0, 1, runTest_SSM, 5 ,  b'kick off\r\n' , b'Disabling wd',0 ,0  ) #To do - Verify board reboots in 2 seconds by watching the AM logging terminal (read 'CLI Initialized' on AM CLI)
    runTest_AM(20, b'Verify Reboot After Kick Off\r\n' , b'CLI Initialized' ) # Run test to check if the board Reboots its self
    if test_Success == 0: 
        allManufacturingCompleteTestsSuccessful = 0
    updateLable(42)
    #____End-of-line Tests____
    appWaitTimeCommandString = bytes(('app waitTime '+ str(email_appWaitTime) +'\r\n'), encoding='utf-8' )
    # b'app waitTime 30\r\n'
    repeatTest_AM(3, 0, 1, runTest_AM, 5, appWaitTimeCommandString , b'Setting wait time in seconds to' )
    if test_Success == 0: 
        allManufacturingCompleteTestsSuccessful = 0
    updateLable(43)
    repeatTest_AM(3, 0, 1, runTest_AM, 5,  b'app mfgComplete\r\n' , b'Set manufacturing complete & starting timer' )
    if test_Success == 0: 
        allManufacturingCompleteTestsSuccessful = 0
    updateLable(44)

    if allManufacturingCompleteTestsSuccessful == 1 : 
        # Add Finish time to CSV
        now = datetime.now()
        row_dict['Finish Date Time '] = now.strftime("%d-%m-%Y %H:%M:%S")
        # Save line to CSV
        if(isFirstEntry):
            createNewCSVFilewithHeaders()
            appendLineToCSVFile()
            isFirstEntry = 0 
            
        else:
            # Turn the Otii Off -  We want the email to be sent when the board is completly assembled 
            appendLineToCSVFile()
            otiiOff()
    else: 
         # Turn the Otii Off -  We want the email to be sent when the board is completly assembled 
        otiiOff()
        reRunManufacturingCompleteTestsWindow.show()


    
    if test_alowTimeToSendEmail == 1: 
    # Allow the device to send message to AWS 
        otiiOff()
        time.sleep(5)
        otiiOn()
        runTest_AM(email_appWaitTime + 120, b'Started Email Timer\r\n' , b'Time to send a manufacturing complete message' ) # not a real command, just added for CSV
        runTest_AM((email_appWaitTime + 120), b'Verified Email Sent\r\n' , b'Turned off Modem' ) # not a real command, just added for CSV 
        time.sleep(5)
        otiiOff()
    
# How the main Program and Test Process works: 
# mainProgramAndTest() is called:
# 1. Board is programmed 
# 2. Voltage Tests Are Run 
# 2. Serial Tests Are Run 
# 3. If serial tests are Successful the Main Program and Test Function calls:           (If unsuccessul the reRunSerialTests Window is opened)
# 4. Move Board Window. The user moves board to the other side of the jig and clicks the confirm button which calls:  
# 5. prepare_runTest_otii() This sets up the Otii and then calls: 
# 6. touchPadsWindow, the user touches the capacitive touch pads and presses the button which calls: 
# 7. runTest_otii(timeOutTime, command, expectedResponse): this gets the result and verifies it. If ok it then it calls: 
# 8. prepareOtiiForManufacturingCompleteTests. This sets up the OttiSide power and then calls: 
# 9. runManufacturingCompleteTests. If this is successul it creates and appends a line to CSV (If unsuccessful it opens the reRunManufacturingCompleteTestsWindo )
# __________Main Program and Test Function______________
def mainProgramAndTest():
    global isFirstEntry 
    global row_dict 
    global testStatusLableArray
    global test_Success
    global otiiTestRetryCount

    showTestStatusWindow()

    # Make sure the Otii is turned off - Essencial for Liams setup, not important for Jig 
    otiiOff()

    # Need to reset the dictionary values to zero to erase data from the previous board 
    row_dict = {x: 'waiting' for x in row_dict}
    print(row_dict)
    # Need to reset test status window lables 
    testStatusLableArray =['Waiting - 等待   ']*50
    updateAllLables()

    
    # Process:
    # Add start time to CSV
    now = datetime.now()
    row_dict['Start Date Time'] = now.strftime("%d-%m-%Y %H:%M:%S")

    time.sleep(2)

    # Enter the QR Code of the Board 
    if (test_useFakeQRCode != 1 ):
        enterQRCode()
    else:
        enterQRCodeRandom() 

    
    # Connect the short 
    showTestStatusWindow()
    
    connectShort()
    # Power Cycle the board 
    powerOffBoard()
    time.sleep(5)
    powerOnBoard()
    time.sleep(10)
    
    # Program the board 
    if (test_ProgramBoard_off != 1 ): 
        programBoard()
        # The board needs time after programming
        time.sleep(120) #TO DO - Verify if this is nessesary
    else: 
        test_Success = 1 # used to bypass programming the board for testing 

    if(test_Success):
        # Power Cycle the board 
        powerCycleBoard()
        
        time.sleep(5)
        # Run Voltage Tests 
        runVoltageTests()
        # Power Cycle the Board
        powerCycleBoard()
        time.sleep(10) # To do Verify what this time should be
        # powerCycleBoard()
        # Sometimes it will fail to enable test mode and logging, retry serveral times
        # Enable Test Mode and Logging 
        enableTestModeAndLog()
        for x in range (3):
            if(test_Success == 0):
                powerCycleBoard()
                time.sleep(10)
                enableTestModeAndLog()
                print('Enable Test Mode and Log Attempt number:', x)
            
        if(test_Success):
            # Run Serail Tests 
            if (test_runSerialTests_off != 1 ):
                time.sleep(10)
                runAllSerialTests()  
        else:
            
            print('Could not enable test mode and logging, please check hardware. Try reconnecting FTDI cables in the correct order')
    else: 
        
        print('Could not verify programming has finished, please check hardware. Try reconnecting FTDI cables in the correct order ')

        
# ________Verify Email CSV Functions___________
def verifyEmailCreateNewCSVFilewithHeaders(file):
    global verify_email_dict 
    global current_verify_emails_csv_file_name
    
    # csv_field_names = ['Time','WasProgrammed','Test1','Test2','Test3']
    # datetime object containing current date and time
    # now = datetime.now()

    # Get the file name of the csv file we read from to get the serials
    stringList = file.split("/")
    index = len(stringList)-1

    current_verify_emails_csv_file_name = 'Verify Emails - Log: ' + stringList[index]  #now.strftime("%d-%m-%Y %H:%M:%S")
    writeFeilddNames(current_verify_emails_csv_file_name, verify_email_dict.keys())
    print("CSV File created with name", current_verify_emails_csv_file_name)

def verifyEmailAppendLineToCSVFile():
    global verify_email_dict 
    global current_verify_emails_csv_file_name

    # print(verify_email_dict)
    # Append a dict as a row in csv file
    append_dict_as_row(current_verify_emails_csv_file_name, verify_email_dict, verify_email_dict.keys())

def verifyEmailResetDictionary():
    global verify_email_dict
    # Need to reset the dictionary values to zero to erase data from the previous board 
    verify_email_dict = {x: 0 for x in verify_email_dict}
    print(verify_email_dict)

def openAndReadCSVforCyrpSerialNumbers():
    file = easygui.fileopenbox()
    # print('here')
    crypSerialIDCSVArray = {}

    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                line_count += 1
            # print(f'\t{row["cryp serial"]}')
            crypSerialIDCSVArray[line_count] = row["cryp serial"] 
            line_count += 1
        print(f'Processed {line_count} lines.')
        # print(crypSerialIDCSVArray) 
        return crypSerialIDCSVArray


def clean(text):
    # clean text for creating a folder
    return "".join(c if c.isalnum() else "_" for c in text)

def fetchEmails(numberOfMostResentEmails):
    # number of top emails to fetch
    N = numberOfMostResentEmails
    crypSerialIDEmailArray = {} #  [0]*N

    # create an IMAP4 class with SSL, use your email provider's IMAP server
    imap = imaplib.IMAP4_SSL("imap.gmail.com")
    # authenticate
    imap.login(username, password)

    # select a mailbox (in this case, the inbox mailbox)
    # use imap.list() to get the list of mailboxes
    status, messages = imap.select("INBOX")
    print(str(numberOfEmailsToFetch) + 'Emails Fetched')
    # total number of emails
    messages = int(messages[0])
    # print(messages)
    index = 0 
    for i in range(messages, messages-N, -1): 
    # for i in range(1, messages, +1):
        # fetch the email message by ID
        res, msg = imap.fetch(str(i), "(RFC822)")
        for response in msg:
            if isinstance(response, tuple):
                # parse a bytes email into a message object
                msg = email.message_from_bytes(response[1])
                # decode the email subject
                subject, encoding = decode_header(msg["Subject"])[0]
                if isinstance(subject, bytes):
                    # if it's a bytes, decode to str
                    subject = subject.decode(encoding)
                # decode email sender
                From, encoding = decode_header(msg.get("From"))[0]
                if isinstance(From, bytes):
                    From = From.decode(encoding)
                # print("Subject:", subject)
                # print("From:", From)
                # if the email message is multipart
                if msg.is_multipart():
                    # Currently the message is not multipart so this code can not be tested 
                    print('This was not a valid email ')

                    # # iterate over email parts
                    # for part in msg.walk():
                    #     # extract content type of email
                    #     content_type = part.get_content_type()
                    #     content_disposition = str(part.get("Content-Disposition"))
                    #     try:
                    #         # get the email body
                    #         body = part.get_payload(decode=True).decode()
                    #     except:
                    #         pass
                    #     if content_type == "text/plain" and "attachment" not in content_disposition:
                    #         # print text/plain emails and skip attachments
                    #         # print(body)
                    #         if '{"' in body: 
                    #             print('!!!!!!!!!!!!!!!!!!!!')
                    #     elif "attachment" in content_disposition:
                    #         # download attachment
                    #         filename = part.get_filename()
                    #         if filename:
                    #             folder_name = clean(subject)
                    #             if not os.path.isdir(folder_name):
                    #                 # make a folder for this email (named after the subject)
                    #                 os.mkdir(folder_name)
                    #             filepath = os.path.join(folder_name, filename)
                    #             # download attachment and save it
                    #             open(filepath, "wb").write(part.get_payload(decode=True))
                else:
                    # extract content type of email
                    content_type = msg.get_content_type()
                    # get the email body
                    body = msg.get_payload(decode=True).decode()
                    if content_type == "text/plain":
                        # print only text email parts
                        # print(body)
                        if '{"' in body: 
                                stringlistSplitStart = str(body).split('{"')
                                stringlistSplitEnd = stringlistSplitStart[1].split('"}')
                                emailText = stringlistSplitEnd[0]
                                emailTextList = emailText.split('"')
                                crypSerialID = emailTextList[4]
                                print('Message number:' + str(i) + ' CrypID = '+crypSerialID)
                                crypSerialIDEmailArray[index]= crypSerialID
                                # print(crypSerialIDEmailArray)
                                index = index + 1
                                
                # if content_type == "text/html":
                #     # if it's HTML, create a new HTML file and open it in browser
                #     folder_name = clean(subject)
                #     if not os.path.isdir(folder_name):
                #         # make a folder for this email (named after the subject)
                #         os.mkdir(folder_name)
                #     filename = "index.html"
                #     filepath = os.path.join(folder_name, filename)
                #     # write the file
                #     open(filepath, "w").write(body)
                #     # open in the default browser
                #     webbrowser.open(filepath)
                # print("="*100)
                # print("@"*100)
    # close the connection and logout
    imap.close()
    imap.logout()
    return crypSerialIDEmailArray

def verifyAllCrypSeials():
    global verify_email_dict
    global verify_email_dictArray
    global current_verify_emails_csv_file_name
    global numberOfEmailsToFetch

    verify_email_dictArray = {}
    # Fetch and process Emails into Array
    crypSerialIDEmailArray = fetchEmails(numberOfEmailsToFetch)

    file = easygui.fileopenbox()
    # print('here')

    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                line_count += 1
            verify_email_dict["Start Date Time"] = row["Start Date Time"]
            verify_email_dict['QR Code'] = row["QR Code"] 
            verify_email_dict['conn iccid'] = row["conn iccid"]
            verify_email_dict['conn imei'] = row["conn imei"]
            verify_email_dict['cryp serial'] = row["cryp serial"].replace(" ", "")
            verify_email_dict['Verified Email Recieved'] = 'Fail - Email Not Verified'
            
            if line_count == 1: 
                verifyEmailCreateNewCSVFilewithHeaders(file)

            for i in range(len(crypSerialIDEmailArray)):
                if row["cryp serial"].replace(" ", "") == crypSerialIDEmailArray[i]:
                    # print('Match for ID ')
                    verify_email_dict['Verified Email Recieved'] = 'Success - Email Verified'
                    break
            print('CSV: ' + row["cryp serial"] + '  Result: ' + verify_email_dict['Verified Email Recieved'])
            verifyEmailAppendLineToCSVFile()
            verify_email_dictArray[line_count] = verify_email_dict
                
            line_count += 1
        # print(f'Processed {line_count} lines.')
         
        # print(verify_email_dictArray) 
        # print('Here') 
        # print(verify_email_dictArray[1]['cryp serial']) 


    
def verifyEmail_EnterQRCode():
    global verify_email_dictArray
    while(1):
        qrCodeInput, ok = QInputDialog.getText(userInputWindow, 'Text Input Dialog', 'Scan the QR code to Verify Email Recieved - 扫 QR码')
        if ok:
            emailVerifiedWindow.close()
            emailNotVerifiedWindow.close()
            if len(str(qrCodeInput))==6 :
                print('QR Code: ', qrCodeInput)
            else:
                print('QR Code is the wrong length')
                verifyEmail_EnterQRCode()
        else:
            emailVerifiedWindow.close()
            emailNotVerifiedWindow.close()
            break
        for x in range (len(verify_email_dictArray)):
            if qrCodeInput == verify_email_dictArray[x+1]['QR Code']:
                isVerified = 1 
                break 
            else: 
                isVerified = 0 
        if isVerified == 1:
            print ('QR Code ' + verify_email_dictArray[x+1]['QR Code'] + '  '+verify_email_dictArray[x+1]['Verified Email Recieved'])
            emailVerifiedWindow.show()
           
        else:
            print ('QR Code ' + qrCodeInput + ' Fail - Email was not verified  ')
            emailNotVerifiedWindow.show()
# __________Slots for GUI______________
# Slots allow buttons to call specific functions 
@Slot()
def mainProgramAndTestSlot(): 
    mainProgramAndTest()

@Slot()
def programBoardSlot(): 
    programBoard()

@Slot()
def runAllSerialTestsSlot(): 
    runAllSerialTests()

@Slot()
def runManufacturingCompleteTestsSlot():
    runManufacturingCompleteTests()

@Slot()
def powerCycleBoardSlot():
    powerCycleBoard()

@Slot()
def powerOnBoardSlot():
    powerOnBoard()

@Slot()
def powerOffBoardSlot():
    powerOffBoard()
    

@Slot()
def connectShortSlot():
    connectShort()
    
@Slot()
def disconnectShortSlot():
    disconnectShort()

@Slot()
def quitSlot(): 
    print("Quitting")
    GPIO.cleanup()
    sys.exit()

@Slot()
def enableTestModeAndLogSlot():
    enableTestModeAndLog() 

@Slot()
def createNewCSVFilewithHeadersSlot():
    createNewCSVFilewithHeaders()

@Slot()
def appendLineToCSVFileSlot():
    appendLineToCSVFile()

@Slot()
def runVoltageTestsSlot():
    runVoltageTests()

@Slot()
def enterQRCodeSlot():
    enterQRCode()

@Slot()
def nandEraseAllSlot():
    nandEraseAll()

@Slot()
def showDeveloperWindowSlot():
    developerWindow.show()

@Slot()
def closeDeveloperWindowSlot():
    developerWindow.close()

@Slot()
def showTestStatusWindowSlot():
    # testStatusWindow.show()
    showTestStatusWindow()

@Slot()
def closeTestStatusWindowSlot():
    testStatusWindow.close()

@Slot()
def updateLabelSlot():
    label_test_status_1.setText('Success')

@Slot()
def connOnSlot():
    runTest_AM(25, b'conn on primary\r\n' , b'primary antenna' )

@Slot()
def connOffSlot():
  runTest_AM(25, b'conn off\r\n' , b'Turned off modem' )

@Slot()
def GPRS4VSlot():
  readADC('4_GPRS', adc3 , 1, 0.3 , 0)

@Slot()
def resetDictionarySlot():
    resetDictionary()

@Slot()
def powerSleepSlot():
    runTest_AM(10, b'pwr sleep\r\n' , b'Putting AM to sleep' )
    

@Slot()
def runTest_otiiSlot():
    global otiiTestRetryCount
    otiiTestRetryCount = 0 
    moveBoardWindow.show()


@Slot()
def otiiOnSlot():
    otiiOn()

@Slot()
def otiiOffSlot():
    otiiOff()


@Slot()
def moveBoardSlot():
    moveBoardWindow.close()
    prepare_runTest_otii()
    

@Slot()
def touchPadsSlot():
    touchPadsWindow.close()
    runTest_otii(20, b'run =otiiTest\r\n',  b'average current')  

@Slot()
def reRunSerialTestsYesSlot():
    reRunSerialTestsWindow.close()
    runAllSerialTests()
    

@Slot()
def reRunSerialTestsNoSlot():
    reRunSerialTestsWindow.close()

@Slot()
def reRunSerialTestsContinueAnywaySlot():
    reRunSerialTestsWindow.close()
    # Turn off Power to board 
    powerOffBoard() 

    otiiTestRetryCount = 0 
    # Instruct the user to move the board to run Otii Tests
    if (test_RunOtiiTestWithOutUserInput != 1):
        moveBoardWindow.show()
    else: 
        prepare_runTest_otii()

@Slot()
def reRunManufacturingCompleteTestsYesSlot():
    reRunManufacturingCompleteTestsWindow.close()
    prepareOtiiForManufacturingCompleteTests()


@Slot()
def reRunManufacturingCompleteTestsNoSlot():
    reRunManufacturingCompleteTestsWindow.close()

@Slot()
def amCheckSlot():
    runTest_SSM( 1,  b'am check\r\n' , b'XXXXXXXX' ,1 ,8  ) # are there certain values we need to check? 
    runTest_AM(600, b'XXXX\r\n' , b'XXXXX' )

@Slot()
def verifyEmailsSlot():
    verifyAllCrypSeials()
    verifyEmail_EnterQRCode()

# @Slot()
# def testSlot():
    # repeatFunction(function(*args), numberOfRepeats , doRestartLeft , doRestartRight)
    # repeatTest_AM(3, 1, 0 , runTest_AM, 25, b'conn on primary\r\n' , b'primary antenna')
    # repeatTest_SSM(3, 1, 0, runTest_SSM, 5 ,  b'eep pattern write\r\n' , b'Write Xcomplete',0 ,0)
    # runTest_AM( 5,  b'cryp serial\r\n' , b'Crypto ID' )# Crypto ID: 01235d52e7fe319301
    # prepareOtiiForManufacturingCompleteTests()
    # verifyEmail_EnterQRCode()
    # emailNotVerifiedWindow.show()
    
@Slot()
def testSlot():
    # repeatFunction(function(*args), numberOfRepeats , doRestartLeft , doRestartRight)
    # repeatFunction(runTest_AM(25, b'conn on primary\r\n' , b'primary antenna' ), 3 , 1 , 1)
    # runTest_AM( 5,  b'cryp serial\r\n' , b'Crypto ID' )# Crypto ID: 01235d52e7fe319301
    # runTest_SSM( 5 ,  b'kick off\r\n' , b'Disabling wd',0 ,0  ) #To do - Verify board reboots in 2 seconds by watching the AM logging terminal (read 'CLI Initialized' on AM CLI)
    # runTest_AM(20, b'Reboot After Kick Off\r\n' , b'CLI Initialized' ) # Run test to check if the board Reboots its self
    
    
    global test_ProgramBoard_off 
    global test_useFakeQRCode 
    global test_ManufacturingCompleteTests_off 
    global test_RunOtiiTestWithOutUserInput 
    global test_runSerialTests_off 
    global test_alowTimeToSendEmail

    test_ProgramBoard_off = 0
    test_useFakeQRCode = 1
    test_ManufacturingCompleteTests_off = 0 
    test_RunOtiiTestWithOutUserInput = 1 
    test_runSerialTests_off = 0 
    test_alowTimeToSendEmail = 1

    for x in range (10):
        mainProgramAndTest()
        nandEraseAll()


    

# Create QApplication___________________________________________
app = QApplication([])

# # # ___________Prompt User Input Window (QR Code) _________________
# # Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
userInputWindow = createWindow(0, 0 , 1,1, 0, 300, 200, windowBackgroundColour ,windowTextColour)

# # label_scanQRCode = createTextLabel( 'Please Scan the QR code - 扫 QR码' , 40 , 1 )
# # layout_userInputWindow = QGridLayout()
# # layout_userInputWindow.addWidget(label_scanQRCode)
# # userInputWindow.setLayout(layout_userInputWindow)

# ____________________Email Verified Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
emailVerifiedWindow = createWindow(0, 0 , 1 , 1, 0, 800, 200, windowBackgroundColour ,windowTextColour)

qr = emailVerifiedWindow.frameGeometry()
emailVerifiedWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Touch Pads Image
emailVerifiedLabel = createPictureLabel('Please tocuh the gold pads', path_verified,350 ,400 )

layout_emailVerifiedWindow = QGridLayout()
layout_emailVerifiedWindow.addWidget(emailVerifiedLabel, 0, 0  ) 

emailVerifiedWindow.setLayout(layout_emailVerifiedWindow)

# ____________________Email Not Verified Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
emailNotVerifiedWindow = createWindow(0, 0 , 1 , 1, 0, 800, 200, windowBackgroundColour ,windowTextColour)

qr = emailNotVerifiedWindow.frameGeometry()
emailNotVerifiedWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Touch Pads Image
emailNotVerifiedLabel = createPictureLabel('Please tocuh the gold pads', path_notVerified,350 ,400 )

layout_emailNotVerifiedWindow = QGridLayout()
layout_emailNotVerifiedWindow.addWidget(emailNotVerifiedLabel, 0, 0  ) 

emailNotVerifiedWindow.setLayout(layout_emailNotVerifiedWindow)

# ____________________Promt User Move-Board Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
moveBoardWindow = createWindow(0, 0 , 1 , 0, 0, 500, 200, windowBackgroundColour ,windowTextColour)

qr = moveBoardWindow.frameGeometry()
moveBoardWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Move Board Image
moveBoardLabel = createPictureLabel('Please move board to other side of jig', path_moveBoard,300 ,400 )

# Move Board Ok Button 
moveBoardButton = QPushButton("OK - I have Moved the Board")
moveBoardButton.clicked.connect(moveBoardSlot)

layout_moveBoardWindow = QGridLayout()
layout_moveBoardWindow.addWidget(moveBoardLabel, 0, 0  ) 
layout_moveBoardWindow.addWidget(moveBoardButton, 1, 0  )

moveBoardWindow.setLayout(layout_moveBoardWindow)

# ____________________Promt User Touch Pads Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
touchPadsWindow = createWindow(0, 0 , 1 , 1, 0, 500, 200, windowBackgroundColour ,windowTextColour)

qr = touchPadsWindow.frameGeometry()
touchPadsWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Touch Pads Image
touchPadsLabel = createPictureLabel('Please tocuh the gold pads', path_touchPads,300 ,400 )

# Touch Pads Ok Button 
touchPadsButton = QPushButton("OK - I have touched the Pads")
touchPadsButton.clicked.connect(touchPadsSlot)

layout_touchPadsWindow = QGridLayout()
layout_touchPadsWindow.addWidget(touchPadsLabel, 0, 0  ) 
layout_touchPadsWindow.addWidget(touchPadsButton, 1, 0  ) 

touchPadsWindow.setLayout(layout_touchPadsWindow)

# ____________________Promt User Re- Run Serial Tests Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
reRunSerialTestsWindow = createWindow(0, 0 , 1 , 1, 0, 800, 200, windowBackgroundColour ,windowTextColour)

qr = reRunSerialTestsWindow.frameGeometry()
reRunSerialTestsWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

reRunSerialTestsLabel1 = createTextLabel( 'At least one Serial Test has failed' , 12 , 0 )
reRunSerialTestsLabel2 = createTextLabel( 'Re-run Serial Tests?' , 12 , 0 )

# Touch Pads Ok Button 
reRunSerialTestsYesButton = QPushButton("Yes - Re-run Serial Tests")
reRunSerialTestsYesButton.clicked.connect(reRunSerialTestsYesSlot)

# Touch Pads Ok Button 
reRunSerialTestsContinueAnywayButton = QPushButton("No - Continue With Remaining Tests")
reRunSerialTestsContinueAnywayButton.clicked.connect(reRunSerialTestsContinueAnywaySlot)

# Touch Pads Ok Button 
reRunSerialTestsNoButton = QPushButton("No - Cancel Programming this board")
reRunSerialTestsNoButton.clicked.connect(reRunSerialTestsNoSlot)


layout_reRunSerialTestsWindow = QGridLayout() 
layout_reRunSerialTestsWindow.addWidget(reRunSerialTestsLabel1 , 1, 0  ) 
layout_reRunSerialTestsWindow.addWidget(reRunSerialTestsLabel2 , 2, 0  ) 
layout_reRunSerialTestsWindow.addWidget(reRunSerialTestsYesButton, 3, 0  ) 
layout_reRunSerialTestsWindow.addWidget(reRunSerialTestsContinueAnywayButton, 4, 0  ) 
layout_reRunSerialTestsWindow.addWidget(reRunSerialTestsNoButton, 5, 0  ) 

reRunSerialTestsWindow.setLayout(layout_reRunSerialTestsWindow)

# ____________________Promt User Re- Run ManufacturingComplete Tests Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
reRunManufacturingCompleteTestsWindow = createWindow(0, 0 , 1 , 1, 0, 800, 200, windowBackgroundColour ,windowTextColour)

qr = reRunManufacturingCompleteTestsWindow.frameGeometry()
reRunManufacturingCompleteTestsWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

reRunManufacturingCompleteTestsLabel1 = createTextLabel( 'At least one Manufacturing Complete Test has failed' , 12, 0 )
reRunManufacturingCompleteTestsLabel2 = createTextLabel( 'Re-run Manufacturing Complete Tests?' , 12 , 0 )

# Touch Pads Ok Button 
reRunManufacturingCompleteTestsYesButton = QPushButton("Yes - Re-run ManufacturingComplete Tests")
reRunManufacturingCompleteTestsYesButton.clicked.connect(reRunManufacturingCompleteTestsYesSlot)

# Touch Pads Ok Button 
reRunManufacturingCompleteTestsNoButton = QPushButton("No - Cancel Programming this board")
reRunManufacturingCompleteTestsNoButton.clicked.connect(reRunManufacturingCompleteTestsNoSlot)


layout_reRunManufacturingCompleteTestsWindow = QGridLayout() 
layout_reRunManufacturingCompleteTestsWindow.addWidget(reRunManufacturingCompleteTestsLabel1 , 1, 0  ) 
layout_reRunManufacturingCompleteTestsWindow.addWidget(reRunManufacturingCompleteTestsLabel2 , 2, 0  ) 
layout_reRunManufacturingCompleteTestsWindow.addWidget(reRunManufacturingCompleteTestsYesButton, 3, 0  )  
layout_reRunManufacturingCompleteTestsWindow.addWidget(reRunManufacturingCompleteTestsNoButton, 4, 0  ) 

reRunManufacturingCompleteTestsWindow.setLayout(layout_reRunManufacturingCompleteTestsWindow)

# reRunManufacturingCompleteTests

# ____________________Main Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1 , 0, 0, 10, 50, windowBackgroundColour ,windowTextColour)

qr = mainWindow.frameGeometry()
mainWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Main Window Assets____________________________

# Main Window Quit Button
mainQuitButton = QPushButton("Quit Program")
mainQuitButton.clicked.connect(quitSlot) # quiter button 

# RPD Logo
mainWindowRPDLabel = createPictureLabel('RPD International', path_rpd,300 ,100 )

# Main Program and Test
mainProgramAndTestButton = QPushButton("Main Program And Test ")
mainProgramAndTestButton.clicked.connect(mainProgramAndTestSlot)

mainProgramAndTestPictureButton = createPictureButton(mainProgramAndTestSlot, path_go, 200  , 200 )#Added to push video to right

# Show Developer Menu
showDeveloperWindowButton = QPushButton(" Developer Menu ")
showDeveloperWindowButton.clicked.connect(showDeveloperWindowSlot)

# Show Test Status Menu 
showTestStatusWindowButton = QPushButton(" Test Status Window ")
showTestStatusWindowButton.clicked.connect(showTestStatusWindowSlot)

# Verify Emails 
verifyEmailsButton = QPushButton("Verify Emails")
verifyEmailsButton.clicked.connect(verifyEmailsSlot)



layout_mainWindow = QGridLayout()
layout_mainWindow.addWidget(mainWindowRPDLabel, 0, 0  ) 
layout_mainWindow.addWidget(mainProgramAndTestPictureButton, 1, 0 ) 
layout_mainWindow.addWidget(showDeveloperWindowButton, 2, 0 ) 
layout_mainWindow.addWidget(showTestStatusWindowButton, 3, 0 )
layout_mainWindow.addWidget(verifyEmailsButton, 4, 0 )
layout_mainWindow.addWidget(mainQuitButton, 15, 0 ) 

mainWindow.setLayout(layout_mainWindow)


# ______________________Test Status Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
testStatusWindow = createWindow(0, 0 , 1 , 0, 0, 350, 50, windowBackgroundColour ,windowTextColour)

qr = testStatusWindow.frameGeometry()
testStatusWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# Test Status Window Assets____________________________
# RPD Logo
testStatusWindowRPDLabel = createPictureLabel('RPD International', path_rpd,300 ,100 )

test_name_1 = 'Verify Programming has Started' 
test_name_2 = 'Verify Programming had Finished'
test_name_3 = 'Voltage Test - 2V8 MAG'
test_name_4 = 'Voltage Test - 3V6_BAT'
test_name_5 = 'Voltage Test - 2V8_DIST'
test_name_6 = 'Voltage Test - 2V8'
test_name_7 = 'Voltage Test - 2V8_AP_FLASH'
test_name_8 = 'Voltage Test- 2V8_EEPROM'
test_name_9 = 'Voltage Test - 2V8_MSP'
test_name_10 = 'Voltage Test - 2V8_GPS'
test_name_11 = 'Voltage Test - 4_GPRS'
test_name_12 = 'Voltage Test - 2V8_STM'
test_name_13 = 'Voltage Test - 2V8_TEMP'
test_name_14 = 'Enabled Test Mode and Logging'
test_name_15 = 'Serial Test - nand pattern write  '
test_name_16 = 'Serial Test - nand pattern read '
test_name_17 = 'Serial Test - eep pattern write'
test_name_18 = 'Serial Test - eep pattern read '
test_name_19 = 'Serial Test - batt serial '
test_name_20 = 'Serial Test - batt read'
test_name_21 = 'Serial Test - rtc st 0 0 0 1 1 1 1 2021 '
test_name_22 = 'Serial Test - rtc rt '
test_name_23 = 'Serial Test - conn on primary'
test_name_24 = 'Voltage Test - 4V _GPRS'
test_name_25 = 'Serial Test - conn iccid'
test_name_26 = 'Serial Test - conn imei'
test_name_27 = 'Serial Test - conn version'
test_name_28 = 'Serial Test - conn tx 9612 22 '
test_name_29 = 'Serial Test - conn off'

test_name_30 = 'Serial Test - conn on secondary'
test_name_31 = 'Serial Test - conn tx 9612 21 '


test_name_32 = 'Serial Test - gps on '
test_name_33 = 'Serial Test - gps print '
test_name_34 = 'Serial Test - gps off'
test_name_35 = 'Serial Test - ssm status'
test_name_36 = 'Serial Test - env sample'
test_name_37 = 'Serial Test - mag on'
test_name_38 = 'Serial Test - mag sample'
test_name_39 = 'Serial Test - algo rawdata'
test_name_40 = 'Serial Test - cryp serial'
test_name_41 = 'Otii Test - Seep Mode Current Test'
test_name_42 = 'Serial Test - kick off'
test_name_43 = 'Serial Test - app waitTime '
test_name_44 = 'Serial Test - app mfgComplete'



layout_testStatusWindow = QGridLayout()

layout_testStatusWindow.addWidget(testStatusWindowRPDLabel, 0, 0  )

label_test_name_1 = createTextLabel( test_name_1 , 7 , 0 )
label_test_status_1 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_1, 1, 0  )
layout_testStatusWindow.addWidget(label_test_status_1, 1, 1) 

label_test_name_2 = createTextLabel( test_name_2 , 7 , 0 )
label_test_status_2 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_2, 2, 0  )
layout_testStatusWindow.addWidget(label_test_status_2, 2, 1) 

label_test_name_3 = createTextLabel( test_name_3 , 7 , 0 )
label_test_status_3 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_3, 3, 0  )
layout_testStatusWindow.addWidget(label_test_status_3, 3, 1) 

label_test_name_4 = createTextLabel( test_name_4 , 7 , 0 )
label_test_status_4 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_4, 4, 0  )
layout_testStatusWindow.addWidget(label_test_status_4, 4, 1) 

label_test_name_5 = createTextLabel( test_name_5 , 7 , 0 )
label_test_status_5 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_5, 5, 0  )
layout_testStatusWindow.addWidget(label_test_status_5, 5, 1) 

label_test_name_6 = createTextLabel( test_name_6 , 7 , 0 )
label_test_status_6 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_6, 6, 0  )
layout_testStatusWindow.addWidget(label_test_status_6, 6, 1) 

label_test_name_7 = createTextLabel( test_name_7 , 7 , 0 )
label_test_status_7 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_7, 7, 0  )
layout_testStatusWindow.addWidget(label_test_status_7, 7, 1) 

label_test_name_8 = createTextLabel( test_name_8 , 7 , 0 )
label_test_status_8 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_8, 8, 0  )
layout_testStatusWindow.addWidget(label_test_status_8, 8, 1) 

label_test_name_9 = createTextLabel( test_name_9 , 7 , 0 )
label_test_status_9 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_9, 9, 0  )
layout_testStatusWindow.addWidget(label_test_status_9, 9, 1) 

label_test_name_10 = createTextLabel( test_name_10 , 7 , 0 )
label_test_status_10 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_10, 10, 0  )
layout_testStatusWindow.addWidget(label_test_status_10, 10, 1) 

label_test_name_11 = createTextLabel( test_name_11 , 7 , 0 )
label_test_status_11 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_11, 11, 0  )
layout_testStatusWindow.addWidget(label_test_status_11, 11, 1) 

label_test_name_12 = createTextLabel( test_name_12 , 7 , 0 )
label_test_status_12 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_12, 12, 0  )
layout_testStatusWindow.addWidget(label_test_status_12, 12, 1) 

label_test_name_13 = createTextLabel( test_name_13 , 7 , 0 )
label_test_status_13 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_13, 13, 0  )
layout_testStatusWindow.addWidget(label_test_status_13, 13, 1) 

label_test_name_14 = createTextLabel( test_name_14 , 7 , 0 )
label_test_status_14 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_14, 14, 0  )
layout_testStatusWindow.addWidget(label_test_status_14, 14, 1) 

label_test_name_15 = createTextLabel( test_name_15 , 7 , 0 )
label_test_status_15 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_15, 15, 0  )
layout_testStatusWindow.addWidget(label_test_status_15, 15, 1) 

label_test_name_16 = createTextLabel( test_name_16 , 7 , 0 )
label_test_status_16 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_16, 16, 0  )
layout_testStatusWindow.addWidget(label_test_status_16, 16, 1) 

label_test_name_17 = createTextLabel( test_name_17 , 7 , 0 )
label_test_status_17 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_17, 17, 0  )
layout_testStatusWindow.addWidget(label_test_status_17, 17, 1) 

label_test_name_18 = createTextLabel( test_name_18 , 7 , 0 )
label_test_status_18 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_18, 18, 0  )
layout_testStatusWindow.addWidget(label_test_status_18, 18, 1) 

label_test_name_19 = createTextLabel( test_name_19 , 7 , 0 )
label_test_status_19 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_19, 19, 0  )
layout_testStatusWindow.addWidget(label_test_status_19, 19, 1) 

label_test_name_20 = createTextLabel( test_name_20 , 7 , 0 )
label_test_status_20 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_20, 20, 0  )
layout_testStatusWindow.addWidget(label_test_status_20, 20, 1) 

label_test_name_21 = createTextLabel( test_name_21 , 7 , 0 )
label_test_status_21 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_21, 21, 0  )
layout_testStatusWindow.addWidget(label_test_status_21, 21, 1) 

label_test_name_22 = createTextLabel( test_name_22 , 7 , 0 )
label_test_status_22 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_22, 22, 0  )
layout_testStatusWindow.addWidget(label_test_status_22, 22, 1) 

label_test_name_23 = createTextLabel( test_name_23 , 7 , 0 )
label_test_status_23 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_23, 23, 0  )
layout_testStatusWindow.addWidget(label_test_status_23, 23, 1) 

label_test_name_24 = createTextLabel( test_name_24 , 7 , 0 )
label_test_status_24 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_24, 24, 0  )
layout_testStatusWindow.addWidget(label_test_status_24, 24, 1) 

label_test_name_25 = createTextLabel( test_name_25 , 7 , 0 )
label_test_status_25 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_25, 25, 0  )
layout_testStatusWindow.addWidget(label_test_status_25, 25, 1) 

label_test_name_26 = createTextLabel( test_name_26 , 7 , 0 )
label_test_status_26 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_26, 26, 0  )
layout_testStatusWindow.addWidget(label_test_status_26, 26, 1) 

label_test_name_27 = createTextLabel( test_name_27 , 7 , 0 )
label_test_status_27 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_27, 27, 0  )
layout_testStatusWindow.addWidget(label_test_status_27, 27, 1) 

label_test_name_28 = createTextLabel( test_name_28 , 7 , 0 )
label_test_status_28 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_28, 28, 0  )
layout_testStatusWindow.addWidget(label_test_status_28, 28, 1) 

label_test_name_29 = createTextLabel( test_name_29 , 7 , 0 )
label_test_status_29 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_29, 29, 0  )
layout_testStatusWindow.addWidget(label_test_status_29, 29, 1) 

label_test_name_30 = createTextLabel( test_name_30 , 7 , 0 )
label_test_status_30 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_30, 30, 0  )
layout_testStatusWindow.addWidget(label_test_status_30, 30, 1) 

label_test_name_31 = createTextLabel( test_name_31 , 7 , 0 )
label_test_status_31 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_31, 31, 0  )
layout_testStatusWindow.addWidget(label_test_status_31, 31, 1) 

label_test_name_32 = createTextLabel( test_name_32 , 7 , 0 )
label_test_status_32 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_32, 32, 0  )
layout_testStatusWindow.addWidget(label_test_status_32, 32, 1) 

label_test_name_33 = createTextLabel( test_name_33 , 7 , 0 )
label_test_status_33 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_33, 33, 0  )
layout_testStatusWindow.addWidget(label_test_status_33, 33, 1) 

label_test_name_34 = createTextLabel( test_name_34 , 7 , 0 )
label_test_status_34 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_34, 34, 0  )
layout_testStatusWindow.addWidget(label_test_status_34, 34, 1) 

label_test_name_35 = createTextLabel( test_name_35 , 7 , 0 )
label_test_status_35 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_35, 35, 0  )
layout_testStatusWindow.addWidget(label_test_status_35, 35, 1) 

label_test_name_36 = createTextLabel( test_name_36 , 7 , 0 )
label_test_status_36 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_36, 36, 0  )
layout_testStatusWindow.addWidget(label_test_status_36, 36, 1) 

label_test_name_37 = createTextLabel( test_name_37 , 7 , 0 )
label_test_status_37 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_37, 37, 0  )
layout_testStatusWindow.addWidget(label_test_status_37, 37, 1) 

label_test_name_38 = createTextLabel( test_name_38 , 7 , 0 )
label_test_status_38 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_38, 38, 0  )
layout_testStatusWindow.addWidget(label_test_status_38, 38, 1) 

label_test_name_39 = createTextLabel( test_name_39 , 7 , 0 )
label_test_status_39 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_39, 39, 0  )
layout_testStatusWindow.addWidget(label_test_status_39, 39, 1) 

label_test_name_40 = createTextLabel( test_name_40 , 7 , 0 )
label_test_status_40 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_40, 40, 0  )
layout_testStatusWindow.addWidget(label_test_status_40, 40, 1) 

label_test_name_41 = createTextLabel( test_name_41 , 7 , 0 )
label_test_status_41 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_41, 41, 0  )
layout_testStatusWindow.addWidget(label_test_status_41, 41, 1) 

label_test_name_42 = createTextLabel( test_name_42 , 7 , 0 )
label_test_status_42 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_42, 42, 0  )
layout_testStatusWindow.addWidget(label_test_status_42, 42, 1) 

label_test_name_43 = createTextLabel( test_name_43 , 7 , 0 )
label_test_status_43 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_43, 43, 0  )
layout_testStatusWindow.addWidget(label_test_status_43, 43, 1) 

label_test_name_44 = createTextLabel( test_name_44 , 7 , 0 )
label_test_status_44 = createTextLabel( 'Waiting - 等待' , 7 , 0 )
layout_testStatusWindow.addWidget(label_test_name_44, 44, 0  )
layout_testStatusWindow.addWidget(label_test_status_44, 44, 1) 

closeTestStatusWindowButton = QPushButton("Close Test Status Window")
closeTestStatusWindowButton.clicked.connect(closeTestStatusWindowSlot)
 
layout_testStatusWindow.addWidget(closeTestStatusWindowButton, 45, 0  ) 

testStatusWindow.setLayout(layout_testStatusWindow)

# _____________________Developer Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
developerWindow = createWindow(0, 0 , 1 , 0, 0, 10, 50, windowBackgroundColour ,windowTextColour)

qr = developerWindow.frameGeometry()
developerWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# DeveloperWindow Assets____________________________

# Developer Window Quit Button
developerQuitButton = QPushButton("Quit Program")
developerQuitButton.clicked.connect(quitSlot) # quiter button 

# RPD Logo
developerWindowRPDLabel = createPictureLabel('RPD International', path_rpd,300 ,100 )

# PowerCycle Board Button 
powerCycleBoardButton = QPushButton("Power Cycle Board")
powerCycleBoardButton.clicked.connect(powerCycleBoardSlot)

# Power On Board Board Button 
powerOnBoardButton = QPushButton("Power On Board")
powerOnBoardButton.clicked.connect(powerOnBoardSlot)

# Power Off Board Board ButtOff 
powerOffBoardButton = QPushButton("Power Off Board")
powerOffBoardButton.clicked.connect(powerOffBoardSlot)

# Connect Short Button 
connectShortButton = QPushButton("Connect Short")
connectShortButton.clicked.connect(connectShort)

# Connect Short Button 
disconnectShortButton = QPushButton("Disconnect Short")
disconnectShortButton.clicked.connect(disconnectShort)

# Enable Test Mode and Log Button 
enableTestModeAndLogButton = QPushButton("Enable Test Mode and SSM Logging")
enableTestModeAndLogButton.clicked.connect(enableTestModeAndLogSlot)

# Run all Serial Tests Button 

runAllSerialTestsButton = QPushButton("Run all Serial Tests")
runAllSerialTestsButton.clicked.connect(runAllSerialTestsSlot)

# Creat new CSV File with Headers 
createNewCSVFilewithHeadersButton = QPushButton("Create CSV File With Headers")
createNewCSVFilewithHeadersButton.clicked.connect(createNewCSVFilewithHeadersSlot)

# Append Line To Current CSV File
appendLineToCSVFileButton = QPushButton("Append Line to CSV File")
appendLineToCSVFileButton.clicked.connect(appendLineToCSVFileSlot)

# Run Voltage Tests
runVoltageTestsButton = QPushButton("Run Voltage Tests")
runVoltageTestsButton.clicked.connect(runVoltageTestsSlot)

# Enter QR Code
enterQRCodeButton = QPushButton("Enter QR Code Now ")
enterQRCodeButton.clicked.connect(enterQRCodeSlot)

# Enter QR Code

nandEraseAllButton = QPushButton("nand Erase All")
nandEraseAllButton.clicked.connect(nandEraseAllSlot)

# Main Program and Test
mainProgramAndTestButton = QPushButton("Main Program And Test ")
mainProgramAndTestButton.clicked.connect(mainProgramAndTestSlot)

# Close Developer Window 
closeDeveloperWindowButton = QPushButton("Close Developer Window ")
closeDeveloperWindowButton.clicked.connect( closeDeveloperWindowSlot)

# Conn on
connOnButton = QPushButton("Conn on")
connOnButton.clicked.connect( connOnSlot)

# Conn off
connOffButton = QPushButton("Conn Off  ")
connOffButton.clicked.connect(connOffSlot)

# Test 4V GPRS
GPRS4VButton = QPushButton("Voltage Test 4v_GPRS  ")
GPRS4VButton.clicked.connect(GPRS4VSlot)

# Manufacuting Complete Tests
runManufacturingCompleteTestsButton = QPushButton("Run manufacturing complete tests  ")
runManufacturingCompleteTestsButton.clicked.connect(runManufacturingCompleteTestsSlot)

# Just program board 
programBoardButton = QPushButton("Just Program Board")
programBoardButton.clicked.connect(programBoardSlot)

# Resets the dictionary for starting the new board
resetDictionaryButton = QPushButton("Reset Dictionary ")
resetDictionaryButton.clicked.connect(resetDictionarySlot)

# Puts the board into power sleep mode for testing the power on the otii 
powerSleepButton = QPushButton("Power Sleep Mode")
powerSleepButton.clicked.connect(powerSleepSlot)

# Run the Otii Power Test 
runTest_otiiButton = QPushButton("Run Power Test - Otii")
runTest_otiiButton.clicked.connect(runTest_otiiSlot)

# Turn Otii On 
otiiOnButton = QPushButton("Otii On")
otiiOnButton.clicked.connect(otiiOnSlot)

# Turn Otii off 
otiiOffButton = QPushButton("Otii Off ")
otiiOffButton.clicked.connect(otiiOffSlot)

# Uesed to check server coms 
amCheckButton = QPushButton("am Check")
amCheckButton.clicked.connect(amCheckSlot)

# Generic Button Used for Testing 
testButton = QPushButton(" Dev Test")
testButton.clicked.connect(testSlot)




layout_developerWindow = QGridLayout()
layout_developerWindow.addWidget(developerWindowRPDLabel, 0, 0  ) 
layout_developerWindow.addWidget(mainProgramAndTestButton, 1, 0 ) 
layout_developerWindow.addWidget(powerOnBoardButton, 2, 0 ) 
layout_developerWindow.addWidget(powerOffBoardButton, 3, 0 ) 
layout_developerWindow.addWidget(powerCycleBoardButton, 4, 0 ) 
layout_developerWindow.addWidget(connectShortButton, 5, 0 ) 
layout_developerWindow.addWidget(disconnectShortButton, 6, 0 )
layout_developerWindow.addWidget(enterQRCodeButton, 7, 0 )  
layout_developerWindow.addWidget(programBoardButton, 8, 0 )
layout_developerWindow.addWidget(runVoltageTestsButton, 9, 0 )
layout_developerWindow.addWidget(enableTestModeAndLogButton, 10, 0 ) 
layout_developerWindow.addWidget(runAllSerialTestsButton, 11, 0 ) 
layout_developerWindow.addWidget(runManufacturingCompleteTestsButton, 12, 0 )
layout_developerWindow.addWidget(createNewCSVFilewithHeadersButton, 13, 0 ) 
layout_developerWindow.addWidget(appendLineToCSVFileButton, 14, 0 )
layout_developerWindow.addWidget(resetDictionaryButton, 15, 0 ) 
layout_developerWindow.addWidget(nandEraseAllButton, 16, 0 ) 
layout_developerWindow.addWidget(connOnButton, 17, 0 ) 
layout_developerWindow.addWidget(connOffButton, 18, 0 ) 
# layout_developerWindow.addWidget(GPRS4VButton, 19, 0 )  
layout_developerWindow.addWidget(powerSleepButton, 20, 0 ) 
layout_developerWindow.addWidget(runTest_otiiButton, 21, 0 ) 
layout_developerWindow.addWidget(otiiOnButton, 22, 0 ) 
layout_developerWindow.addWidget(otiiOffButton, 23, 0 ) 
layout_developerWindow.addWidget(amCheckButton, 24, 0 ) 
layout_developerWindow.addWidget(testButton, 25, 0 ) 


layout_developerWindow.addWidget( closeDeveloperWindowButton, 26, 0 ) 
# layout_developerWindow.addWidget(developerQuitButton, 22, 0 ) 

developerWindow.setLayout(layout_developerWindow)

#__________________ Execute App_________________
try:
    AM_CLI = connectSerial(port_AM_CLI, 115200)
    AM_LOG = connectSerial(port_AM_LOG, 9600) 
    SSM_LOG = connectSerial(port_SSM_LOG, 9600)
    COM_PI_SERIAL = connectSerial(COM_PI_PORT, 9600) 
except:
    print('Could not find serial connections')


# Show the main window
mainWindow.show()
testStatusWindow.show()

app.exec_()