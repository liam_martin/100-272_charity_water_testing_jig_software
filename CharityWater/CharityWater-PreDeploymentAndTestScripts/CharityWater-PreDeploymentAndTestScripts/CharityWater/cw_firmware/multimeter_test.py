# testing multimeter setup
import pyvisa as visa
# import visa
from time import sleep # for delays
import os # for datalogging

# rm = pyvisa.ResourceManager()
rm = visa.ResourceManager('@py')
# List all connected resources
print("Resources detected\n{}\n".format(rm.list_resources()))

supply = rm.open_resource(u'USB0::0x1AB1::0x09C4::DM3R223601458::INSTR') # Put your device IDs here
dmm = rm.open_resource(u'USB0::0x1AB1::0x09C4::DM3R223601458::INSTR')


# Setup Digital MultiMeter in DC Voltage mode
# dmm.write(':FUNCtion:VOLTage:DC')
dmm.write(':FUNCtion:CURRent:DC')

# vMeasured = float( dmm.query(':MEASure:VOLTage:DC?') )  # measure the voltage
cMeasured = float( dmm.query(':MEASure:CURRent:DC?') )  # measure the voltage

print(cMeasured)