import os
import serial
import time
import re
# import multiprocessing

COM_PI_PORT = 'COM4'


#  _________Serial Functions_____________
# Will connect a serial port at a given baude Speed 
def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

# Will write a txString to a serial port 
def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
            print( "Write Complete" )
            print( "" )
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# Will read the Serial port until the expected response is found 
def readUnitlSerial_otii(serialPort ,expectedResponse):
    rxString = b'xxxxyyyyxxxx' # needs to be set to something the board will never print out can't be blank "")
    counter = 0 
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            print(rxString)
            if(rxString != b''):
                if expectedResponse in rxString: 
                    isFound =1
                else:
                    isFound = 0 
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )



# ______ Serial Test Functions with timeout _________
def runTest_otii(timeOutTime, command, expectedResponse):
    global test_Success 
    test_Success = 0 
    writeSerial(COM_PI_SERIAL, command)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    p = multiprocessing.Process(target=readUnitlSerial_otii, args=(COM_PI_PORT,expectedResponse))
    p.start()
    p.join(timeOutTime) #will time out after X seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out SSM command', command)
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "").replace("\\", "")]= 'FAIL - Test Timed Out SSM command'
        test_Success = 0 
    else:
        print('Success for SSM command:', command, '  RxString :', return_dict[1] )
        row_dict[str(command).replace("b'", "").replace("'", "").replace("\\n", "").replace("\\r", "")]= (str(return_dict[1])).replace("b'", "").replace("'", "").replace("\r\n", "")
        test_Success = 1

# _________________________________________________________


try:
    COM_PI_SERIAL = connectSerial(COM_PI_PORT, 9600)
except:
    print('Could not find serial connections')
    # To do - Force the user to restart the app after connecting the serial connections properly 
command = b'run otii test\r\n'

while(1):
    writeSerial(COM_PI_SERIAL, command)
    readUnitlSerial_otii(COM_PI_SERIAL, b'average current')
    # To do get test working for time out on pi 
    # runTest_otii(30, b'run otii test\r\n' , 'average current')

    


# # result = os.system("otiicli cl.lua data5.csv")
# result =os.popen('otiicli cl.lua data5.csv').read()
# print("result", result)

# import serial.tools.list_ports as port_list
# ports = list(port_list.comports())
# for p in ports:
#     print (p)


# import serial

# s = serial.Serial('COM3')
# res = s.read()
# print(res)