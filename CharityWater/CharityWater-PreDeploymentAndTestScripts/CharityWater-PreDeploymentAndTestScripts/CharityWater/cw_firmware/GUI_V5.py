


# from PyQt5 import QtCore
from subprocess import run
from PySide2.QtCore import *
from PySide2.QtGui import *
# import cv2 # OpenCV
# import qimage2ndarray # for a memory leak,see gist
import sys # for exiting
from PySide2.QtWidgets import *


from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtCore import QSize

import pandas
import serial
import time
import os

import RPi.GPIO as GPIO
import time

import serial
import time
import re

import multiprocessing

from csv import writer
from csv import DictWriter
import csv 

from datetime import datetime

import Adafruit_ADS1x15

# __________Global Variables________________
current_csv_file_name = ''
isFirstEntry = 1 
row_dict = dict()

# __________Set up ADC's_______________
adc1 = Adafruit_ADS1x15.ADS1015(address=0x48, busnum=1)
adc2 = Adafruit_ADS1x15.ADS1015(address=0x49, busnum=1)
adc3 = Adafruit_ADS1x15.ADS1015(address=0x4B, busnum=1)
GAIN = 1
# ___________Setup Serial UART's_______
# You need to connect these devices to the pi in the right order so the USB port numbers match 
# use ls /dev to find usb ports
# Connect them in this order to make them appear correctly 
port_AM_CLI = '/dev/ttyUSB0'
port_AM_LOG = '/dev/ttyUSB1'
port_SSM_LOG = '/dev/ttyUSB2'

# _______________Setup GPIOs_____________
in1 = 16
in2 = 18

GPIO.setmode(GPIO.BOARD)
GPIO.setup(in1, GPIO.OUT)
GPIO.setup(in2, GPIO.OUT)

# Setting GPIO Outputs to True deactiveated the Realys
GPIO.output(in1, True)
GPIO.output(in2, True)

#__________ Fixed GUI Parameters ____________________
white = (244,244,244)
red = (227,6,19)
yellow = ( 237, 255, 0)
green = ( 0,150 ,64 )
blue = (0,159  ,277 )
purple = ( 102,36,131 )
black= (0,0,0)

windowTextColour = QColor(0,0,0) 
windowBackgroundColour = QColor(244,244,244)

# ______Determining Paths for Menu Images______________
piOrLaptop = 0 # 0 = Laptop , 1 = Pi !!!!!!!!!!!!!!!!! Crutial to change to get to work on laptop or pi! 

if(piOrLaptop == 0):
    path_quit = "images/quit.png"
    path_rpd = "images/rpd.png"
if(piOrLaptop == 1):
    path_quit = "/home/pi/..../images/quit.png"
    path_rpd = "/home/pi/..../images/quit.png"

# ________GUI Functions_________

def createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour ):
    window = QWidget()
    if(isFrameless == 1 and isStaysOnTop == 0  ):
        window.setWindowFlags(Qt.FramelessWindowHint)
    elif(isFrameless == 0 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.WindowStaysOnTopHint)
    elif(isFrameless == 1 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)

    if(isTranslucent == 1  ):
        window.setAttribute(Qt.WA_TranslucentBackground)

    if(isMaximised == 1):
        window.showMaximized()
    elif(isMaximised== 0 ):
        qr = window.frameGeometry()
        window.move(qr.topLeft().x()+ xDistance, qr.topLeft().y()+ yDistance)
    
    # Change background colour 
    palette = window.palette()
    palette.setColor(QPalette.Window, backgroundColour)
    palette.setColor(QPalette.WindowText, textColour)
    window.setPalette(palette)
    window.setAutoFillBackground(True)

    if(useStyleSheet == 1 ):
        window.setStyleSheet(
            "QWidget" 
            "{" 
            "border: 100px white;"
            "border-radius: 10px;"
            "background-color: black"
            "}"
        )

    # # Change background colour 
    # palette = window.palette()
    # palette.setColor(QPalette.Window, QColor(0, 0, 0))
    # palette.setColor(QPalette.WindowText, QColor(240, 240, 240))
    # window.setPalette(palette)
    # window.setAutoFillBackground(True)

    return window

def createPictureLabel(holdingText , imagePathName, sizeX , sizeY):
    labelName = QLabel("holdingText")
    pixmap = QPixmap(imagePathName)
    #  get label dimensions
    w = sizeX #settingsMenuLabel.width()
    h = sizeY #settingsMenuLabel.height()
    #  set a scaled pixmap to a w x h window keeping its aspect ratio 
    if(sizeX==0 and sizeY ==0):
        labelName.setPixmap(pixmap)
    else:
        labelName.setPixmap(pixmap.scaled(w,h))
    return labelName


def createPictureButton(functionName, imagePathName, sizeX , sizeY):
    buttonName = QPushButton("")
    buttonName.clicked.connect(functionName)
    # Set image in Push Button
    pixmap = QPixmap(imagePathName)
    buttonName_icon = QIcon(pixmap)
    buttonName.setIcon(buttonName_icon)
    buttonName.setIconSize(QSize(sizeX,sizeY))
    buttonName.setFlat(1)
    buttonName.setStyleSheet("QPushButton" "{" "background-color : rgb(0, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 
    return buttonName

def createTextLabel(text , fontSize, isBold ):
    label =  QLabel(str(text))
    font = label.font()
    font.setPointSize(fontSize)
    if(isBold == 1): 
        font.setBold(1)
    label.setFont(font)
    return label

# _______User Input (QR Code Functions)_________

# def userInputWindowShow():
#     userInputWindow.show()

# def userInputWindowClose():
#     userInputWindow.close()

def enterQRCode():
    qrCodeInput, ok = QInputDialog.getText(userInputWindow, 'Text Input Dialog', 'Enter your name:')
    if ok:
        print('QR Code: ', qrCodeInput)
        row_dict['QR Code']= str(qrCodeInput)
    
    # userInputWindowShow()
    
    # qrCodeInput = input("Scan the QR Code:")
    # print("Username is: " + qrCodeInput)
    # # row_dict['QR Code']= str(qrCodeInput)
    # # userInputWindowClose()

# _________Serial Functions_____________
def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
            # print( "Write Complete" )
             print( "" )
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

def readForeverSerial(serialPort):
    rxString = b''
    try:
        while True:
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

def readUnitlSerial_SSM(serialPort ,expectedResponse, return_dict ):
    rxString = b'xxxxyyyyxxxx'
    isFound = 0
    try:
        # while rxString != expectedResponse :
        while (isFound == 0):
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            if expectedResponse in rxString: 
                isFound =1
                return_dict[1] = rxString
            else:
                isFound = 0 
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

    # print('!!!!! SSM Test complete:', expectedResponse)

def readUnitlSerial_AM(serialPort ,expectedResponse, return_dict):
    response = b'xxxxxxxx'
    rxString = b'xxxxyyyyxxxx'
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            print(rxString)
            rxString = rxString.rstrip()
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            if expectedResponse in rxString: 
                isFound =1
                return_dict[1] = response
            else:
                isFound = 0 

    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# ______ Serial Test Functions with timeout _________

def runTest_SSM(command, expectedResponse):
    # print(str(command))
    global row_dict 
    writeSerial(SSM_LOG, command)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    p = multiprocessing.Process(target=readUnitlSerial_SSM, args=(SSM_LOG,expectedResponse, return_dict))
    p.start()
    p.join(20) #will time out after 5 seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out SSM command', command)
        row_dict[str(command)]= 'FAIL - Test Timed Out SSM command'
    else:
        print('Sucess for SSM command:', command, '  RxString :', return_dict[1] )
        row_dict[str(command)]= return_dict[1]



def runTest_AM(command, expectedResponse):
    # print(str(command))
    global row_dict 
    writeSerial(AM_CLI, command)
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    # print('Check it gets here')
    p = multiprocessing.Process(target=readUnitlSerial_AM, args=(AM_LOG,expectedResponse, return_dict))
    p.start()
    p.join(20) #will time out after 5 seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Test Timed Out AM command', command)
        row_dict[str(command)]= 'FAIL - Test Timed Out AM command'
    else:
        print('Sucess for AM command:', command,  '  Response: ', return_dict[1] )
        row_dict[str(command)]= return_dict[1]

#__________ ADC Functions __________
def readADC(testName, adc , channel, max, min):
    global row_dict
    time.sleep(0.5)
    total = 0 
    for i in range(10):
        result = (3.295/1648)*(adc.read_adc(channel, gain=GAIN))
        time.sleep(0.1)
        print('The voltage is: ' , result)
        total= total + result
    average = round(total/10,2)
    print(average)

    if (average > min and average < max):
        message = ('Sucess: '+ str(average) +'V')
        row_dict[str(testName)]= message
        print(message)
    elif(average < min ): 
        message = ('Fail: '+ str(average) +'V is less than ' +str(min)+'V')
        row_dict[str(testName)]= message
        print(message)
    elif(average >max):
        message = ('Fail: '+ str(average) +'V is more than ' +str(max)+'V')
        row_dict[str(testName)]= message
        print(message)

# ________CSV Functions___________

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

def append_dict_as_row(file_name, dict_of_elem, field_names):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names)
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)

def writeFeilddNames(file_name,  field_names):
    with open(file_name, 'w', encoding='UTF8', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=field_names)
        writer.writeheader()
        f.close()  

def createNewCSVFilewithHeaders():
    global row_dict 
    global current_csv_file_name
    
    # csv_field_names = ['Time','WasProgrammed','Test1','Test2','Test3']
    # datetime object containing current date and time
    now = datetime.now()
    current_csv_file_name = now.strftime("%d-%m-%Y %H:%M:%S")
    writeFeilddNames(current_csv_file_name, row_dict.keys())
    print("CSV File created with name", current_csv_file_name)

def appendLineToCSVFile():
    global row_dict 
    global current_csv_file_name

    print(row_dict)
    # Append a dict as a row in csv file
    append_dict_as_row(current_csv_file_name, row_dict, row_dict.keys())
    # try:
    #     row_contents = [32,'Shaun','Java','Tokyo','Morning']
    #     # Append a list as new line to an old csv file
    #     append_list_as_row(current_csv_file_name, row_contents)
    # except NameError:
    #     print('Error - You need to create a new CSV file before you can add to it')

# ___________Board Interaction Functions___________
# def programBoard(): 
#     test = os.system("sudo openocd")#change the contents of openocd.cfg to change the files to be flashed
#     print("it ran")
#     print("Please wait approx 5 mins")
#     readUnitlSerial_AM(AM_LOG ,'Programmed SSM with image. Good to go')

def verifyStartProgramming(expectedResponse, return_dict):
    # Program the Board using Open OCD
    programBoard = os.system("sudo openocd")#change the contents of openocd.cfg to change the files to be flashed
    
    response = b'xxxxxxxx'
    rxString = b'xxxxxxxx'
    isFound = 0
    print('!!!!!!!!!!!!!!!!!!!!!HERE')
    # Verify programming Started Sucsessfully 
    try:
        while (isFound == 0):
            rxString = AM_LOG.readline() 
            print(rxString)
            rxString = rxString.rstrip()
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            if expectedResponse in rxString: 
                isFound =1
                return_dict[1] = response
            else:
                isFound = 0 
    except OSError:
        print( "ERROR:  on %s - exiting!" % AM_LOG )
        exit( 1 )



def programBoard():
    global row_dict 
    manager = multiprocessing.Manager()
    return_dict = manager.dict()
    # print('Check it gets here')
    hasStarted=0

    # Start Programming and Verify programming has started 
    p = multiprocessing.Process(target=verifyStartProgramming, args=(b'Copying internal flash to external', return_dict))
    p.start()
    p.join(20) #will time out after 1 mins 
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('FAIL - Start Programing Board Timed Out')
        row_dict['Start Programing Board']= 'FAIL - Start Programing Board Timed Out'
    else:
        print('Sucess - Start Programing Board: ', str(return_dict[1]) )
        row_dict['Start Programing Board']= return_dict[1]
        hasStarted = 1 

    # Verify the programming Finishes correctly
    if(hasStarted):
        # Verify programming is complete
        p = multiprocessing.Process(target=readUnitlSerial_AM, args=(AM_LOG, b'Programmed SSM with image. Good to go', return_dict))
        p.start()
        p.join(10*60) #will time out after 10 mins 
        # If thread is still active
        if p.is_alive():
            # print ("Test is still running - kill it...")
            # Terminate - may not work if process is stuck for good
            p.terminate()
            # OR Kill - will work for sure, no chance for process to finish nicely however
            # p.kill()
            print('FAIL - Finish Program Board Timed Out')
            row_dict['Finish Program Board']= 'FAIL - Finish Programing Board Timed Out'
        else:
            print('Sucess - Finished Programming Board: ', str(return_dict[1]) )
            row_dict['Finish Program Board']= return_dict[1]




def powerCycleBoard():
    GPIO.output(in1, False)
    print("Board power off - check relay is activate")
    time.sleep(2)
    GPIO.output(in1, True)
    print("Board power on - check relay is dectivated")
    runTest_AM(b'\r\n', b'CLI Initialized')

# To be used if a board reaches the end of the test cycle and it has been put in manufacturing mode and needs to be reset to re run tests
def nandEraseAll():
    runTest_AM(b'nand eraseall\r\n', b'Finished Erasing FLASH')
    
    

def powerOffBoard():
    GPIO.output(in1, False)
    print("Board power off - check relay is activate")
    
def powerOnBoard():
    GPIO.output(in1, True)
    print("Board power on - check relay is dectivated")
    

def connectShort():
    GPIO.output(in2, False)


def disconnectShort():
    GPIO.output(in2, True)

# ________Voltage Tests______________
def runVoltageTests():
    readADC('2V8_MAG', adc1 , 0, 2.9 , 2.7 )
    readADC('3v6_BAT', adc1 , 1,3.7 , 3.5)
    readADC('2V8_DIST', adc1 , 2,2.9 , 2.7)
    readADC('2V8', adc1 , 3,2.9 , 2.7)
    
    readADC('2V8_AP_FLASH', adc2 , 0,2.9 , 2.7)
    readADC('2V8_EEPROM', adc2 , 1,2.9 , 2.7)
    readADC('2V8_MSP', adc2 , 2,2.9 , 2.7)
    
    readADC('2V8_GPS', adc3 , 0, 0.3 , 0)
    readADC('4_GPRS', adc3 , 1, 0.3 , 0)
    readADC('2V8_STM', adc3 , 2,2.9 , 2.7)
    readADC('2V8_TEMP', adc3 , 3,2.9 , 2.7)

# _________Serial Tests_______________

def enableTestModeAndLog():
    runTest_AM( b'app mode test\r\n' , b'Test mode on' )
    runTest_SSM( b'log enable\r\n' , b'Enabled logging'  )

def runAllSerialTests():
    # Flash
    runTest_AM( b'nand pattern write\r\n' , b'Done writing pattern' )
    runTest_AM( b'nand pattern read\r\n' , b'Pass' )

    # EEPROM
    runTest_SSM( b'eep pattern write\r\n' , b'Write complete')
    runTest_SSM( b'eep pattern read\r\n' , b'Pass')

    # Fule Gauge
    runTest_SSM( b'batt serial\r\n' , b'0x'  ) #0x3639A975030000EC

    # Battery Voltage Measurement 
    runTest_SSM( b'batt read\r\n' , b'Current Batt Voltage'  ) #Current Batt Voltage: 3625 mV

    # RTC 
    runTest_SSM( b'rtc st 0 0 0 1 1 1 1 2021\r\n' ,b'HW_RTC: Time set'  ) # Need to create the correct date and time roughly
    runTest_SSM( b'rtc rt\r\n' , b'HW_RTC Time'  ) #HW_RTC Time: Sunday, January 1, 2021 - 01:01:53.15
    
    # Cellular Modem
    time.sleep(1)
    runTest_AM( b'conn on primary\r\n' , b'primary antenna' ) #seems to cause a restart
    runTest_AM( b'conn on secondary\r\n' , b'secondary antenna' )
    runTest_AM( b'conn iccid\r\n' , b'CCID:' ) # error no sim inserted
    runTest_AM( b'conn imei\r\n' , b'IMEI' )# Found IMEI: 358887095625834
    runTest_AM( b'conn version\r\n' , b'23.60' )# Needs to match the version number of the modem 
    runTest_AM( b'conn tx 9612 22\r\n' , b'+UTEST:' )# There are other options we may need to try
    runTest_AM( b'conn off\r\n' , b'Turned off modem' )

    # GPS
    runTest_AM( b'gps on\r\n' , b'Gps is ON - stay tuned for location info' )
    runTest_AM( b'gps print\r\n' , b'Received GPGGA message from GPS' )
    runTest_AM( b'gps off\r\n' , b'Gps is OFF' )

    # SPI
    runTest_AM( b'ssm status\r\n' , b'SSM-AM SPI test pass' )

    # Temp and Humidity
    # To do - multiline response 
    runTest_SSM( b'env sample\r\n' , b'HW_ENV'  ) # HW_ENV: Temperature: 27øC /new line/ HW_ENV: Humidity: 52%

    # Magnetometer
    runTest_SSM( b'mag on\r\n' , b'Magnetometer is on'  )
    runTest_SSM( b'mag sample\r\n' , b'MAG'  ) #MAG X: -233, MAG Y: -89, MAG Z: 537 MAG TEMP: 25 C
    
    # Cap sense pads 
    # To do -  multiline response 
    runTest_SSM( b'algo rawdata \r\n' , b'Pad 8'  ) # are there certain values we need to check? 
    # Pad 1:   692
    # Pad 2:   695
    # Pad 3:   701
    # Pad 4:   691
    # Pad 5:   687
    # Pad 6:   679
    # Pad 7:   671
    # Pad 8:   692

    # Cyrpto Chip 
    runTest_AM( b'cryp serial\r\n' , b'Crypto ID' )# Crypto ID: 01235d52e7fe319301

    # Wathdog
    runTest_SSM( b'kick off\r\n' , b'Disabling wd'  ) #Verify board reboots in 2 seconds by watching the AM logging terminal (read 'CLI Initialized' on AM CLI)
    
    #____End-of-line Tests____
    # Power cycle the board now
    runTest_AM( b'app waitTime 30\r\n' , b'Setting wait time in seconds to' )
    runTest_AM( b'app mfgComplete\r\n' , b'Set manufacturing complete & starting timer' )
    
# __________Main Program and Test Function______________
def mainProgramAndTest():
    global isFirstEntry
    # Process:
    # Add start time to CSV
    now = datetime.now()
    row_dict['Start Date Time '] = now.strftime("%d-%m-%Y %H:%M:%S")
    # Enter the QR Code of the Board 
    enterQRCode()
    # Connect the short 
    connectShort()
    # Power Cycle the board 
    powerCycleBoard()
    # Program the board 
    programBoard()
    # Wait 5 mins - Pop up window
    # Disconnect the short 
    disconnectShort()
    # Power Cycle the board 
    powerCycleBoard()
    # Run Voltage Tests 
    runVoltageTests()
    # Enable Test Mode and Logging 
    enableTestModeAndLog()
    # Run Serail Tests 
    runAllSerialTests()
    # Run Final Verification Tests 

    # Add Finish time to CSV
    now = datetime.now()
    row_dict['Finish Date Time '] = now.strftime("%d-%m-%Y %H:%M:%S")

    # Save line to CSV (To do - reset all values before next board)
    if(isFirstEntry):
        createNewCSVFilewithHeaders()
        appendLineToCSVFile()
        isFirstEntry = 0 
    else:
        appendLineToCSVFile()
        
    



# __________Slots for GUI______________

@Slot()
def mainProgramAndTestSlot(): 
    mainProgramAndTest()

@Slot()
def programBoardSlot(): 
    programBoard()

@Slot()
def runAllSerialTestsSlot(): 
    runAllSerialTests()

@Slot()
def powerCycleBoardSlot():
    powerCycleBoard()

@Slot()
def powerOnBoardSlot():
    powerOnBoard()

@Slot()
def powerOffBoardSlot():
    powerOffBoard()
    

@Slot()
def connectShortSlot():
    connectShort()

@Slot()
def disconnectShortSlot():
    disconnectShort()

@Slot()
def quitSlot(): 
    print("Quitting")
    GPIO.cleanup()
    sys.exit()

@Slot()
def enableTestModeAndLogSlot():
    enableTestModeAndLog() 

@Slot()
def createNewCSVFilewithHeadersSlot():
    createNewCSVFilewithHeaders()

@Slot()
def appendLineToCSVFileSlot():
    appendLineToCSVFile()

@Slot()
def runVoltageTestsSlot():
    runVoltageTests()

@Slot()
def enterQRCodeSlot():
    enterQRCode()

@Slot()
def nandEraseAllSlot():
    nandEraseAll()

    

# Create QApplication___________________________________________
app = QApplication([])

# Prompt User Input Window________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
userInputWindow = createWindow(0, 0 , 1,1, 0, 150, 400, windowBackgroundColour ,windowTextColour)

label_scanQRCode = createTextLabel( 'Please Scan the QR code', 40 , 1 )
layout_userInputWindow = QGridLayout()
layout_userInputWindow.addWidget(label_scanQRCode)
userInputWindow.setLayout(layout_userInputWindow)

# # Quit Window ________________________________
# # Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
# quitWindow = createWindow(0, 0 , 1,1, 0, 150, 400, windowBackgroundColour ,windowTextColour)

# # Quit Button
# # quitQuitbutton = QPushButton("Quitter")
# # quitQuitbutton.clicked.connect(sys.exit) # quiter button 

# quitQuitbutton = createPictureButton(quitFunction, path_quit, 50  , 50 )#Added to push video to right

# layout_quitWindow = QGridLayout()
# layout_quitWindow.addWidget(quitQuitbutton, 0 ,0)
# quitWindow.setLayout(layout_quitWindow)

# Main Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1 , 0, 5, 50*1, 0, windowBackgroundColour ,windowTextColour)

qr = mainWindow.frameGeometry()
mainWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# mainWindow Assets____________________________

# Main Window Quit Button
mainQuitButton = QPushButton("Quit Program")
mainQuitButton.clicked.connect(quitSlot) # quiter button 

# RPD Logo
mainWindowRPDLabel = createPictureLabel('RPD International', path_rpd,300 ,100 )

# Main Window Program Button 
mainProgramButton = QPushButton("Program")
mainProgramButton.clicked.connect(programBoardSlot)

# PowerCycle Board Button 
powerCycleBoardButton = QPushButton("Power Cycle Board")
powerCycleBoardButton.clicked.connect(powerCycleBoardSlot)

# Power On Board Board Button 
powerOnBoardButton = QPushButton("Power On Board")
powerOnBoardButton.clicked.connect(powerOnBoardSlot)

# Power Off Board Board ButtOff 
powerOffBoardButton = QPushButton("Power Off Board")
powerOffBoardButton.clicked.connect(powerOffBoardSlot)



# Connect Short Button 
connectShortButton = QPushButton("Connect Short")
connectShortButton.clicked.connect(connectShort)

# Connect Short Button 
disconnectShortButton = QPushButton("Disconnect Short")
disconnectShortButton.clicked.connect(disconnectShort)

# Enable Test Mode and Log Button 
enableTestModeAndLogButton = QPushButton("Enable Test Mode and SSM Logging")
enableTestModeAndLogButton.clicked.connect(enableTestModeAndLogSlot)

# Run all Serial Tests Button 

runAllSerialTestsButton = QPushButton("Run all Serial Tests")
runAllSerialTestsButton.clicked.connect(runAllSerialTestsSlot)

# Creat new CSV File with Headers 
createNewCSVFilewithHeadersButton = QPushButton("Create CSV File With Headers")
createNewCSVFilewithHeadersButton.clicked.connect(createNewCSVFilewithHeadersSlot)

# Append Line To Current CSV File
appendLineToCSVFileButton = QPushButton("Append Line to CSV File")
appendLineToCSVFileButton.clicked.connect(appendLineToCSVFileSlot)

# Run Voltage Tests
runVoltageTestsButton = QPushButton("Run Voltage Tests")
runVoltageTestsButton.clicked.connect(runVoltageTestsSlot)

# Enter QR Code
enterQRCodeButton = QPushButton("Enter QR Code")
enterQRCodeButton.clicked.connect(enterQRCodeSlot)

# Enter QR Code

nandEraseAllButton = QPushButton("nand Erase All")
nandEraseAllButton.clicked.connect(nandEraseAllSlot)

# Main Program and Test
mainProgramAndTestButton = QPushButton("Main Program And Test ")
mainProgramAndTestButton.clicked.connect(mainProgramAndTestSlot)

layout_mainWindow = QGridLayout()
layout_mainWindow.addWidget(mainWindowRPDLabel, 0, 0  )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(connectShortButton, 1, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(disconnectShortButton, 2, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(powerCycleBoardButton, 3, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(powerOnBoardButton, 4, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(powerOffBoardButton, 5, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 

layout_mainWindow.addWidget(mainProgramButton, 6, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(enableTestModeAndLogButton, 7, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(runAllSerialTestsButton, 8, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(createNewCSVFilewithHeadersButton, 9, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(appendLineToCSVFileButton, 10, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(runVoltageTestsButton, 11, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(enterQRCodeButton, 12, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(nandEraseAllButton, 13, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 

layout_mainWindow.addWidget(mainProgramAndTestButton, 14, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 

layout_mainWindow.addWidget(mainQuitButton, 15, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 



mainWindow.setLayout(layout_mainWindow)




# Execute App_____________________________________
# AM_CLI = connectSerial(port_AM_CLI, 115200)
# AM_LOG = connectSerial(port_AM_LOG, 9600) 
# SSM_LOG = connectSerial(port_SSM_LOG, 9600) 

try:
    AM_CLI = connectSerial(port_AM_CLI, 115200)
    AM_LOG = connectSerial(port_AM_LOG, 9600) 
    SSM_LOG = connectSerial(port_SSM_LOG, 9600) 
except:
    print('Could not find serial connections')


mainWindow.show()
# quitWindow.show()
# mainMenuWindow.show()

# window_settingsMenu.show()
app.exec_()