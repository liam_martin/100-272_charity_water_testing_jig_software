import easygui
import time
from datetime import datetime
import csv
import imaplib
import email
from email.header import decode_header
import webbrowser
import os
from csv import writer
from csv import DictWriter
import csv
from subprocess import run
import sys # for exiting
import pandas
import serial
import time
import os



verify_email_dict = dict()

# account credentials
username = "charitywater@rpdintl.com"
password = "RPDcharity:water1"

def append_list_as_row(file_name, list_of_elem):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        csv_writer = writer(write_obj)
        # Add contents of list as last row in the csv file
        csv_writer.writerow(list_of_elem)

def append_dict_as_row(file_name, dict_of_elem, field_names):
    # Open file in append mode
    with open(file_name, 'a+', newline='') as write_obj:
        # Create a writer object from csv module
        dict_writer = DictWriter(write_obj, fieldnames=field_names)
        # Add dictionary as wor in the csv
        dict_writer.writerow(dict_of_elem)
        write_obj.close()

def writeFeilddNames(file_name,  field_names):
    with open(file_name, 'w', encoding='UTF8', newline='') as f:
        writer = csv.DictWriter(f, fieldnames=field_names)
        writer.writeheader()
        f.close()  
        
# ________Verify Email CSV Functions___________
def verifyEmailCreateNewCSVFilewithHeaders():
    global verify_email_dict 
    global current_csv_file_name
    
    # csv_field_names = ['Time','WasProgrammed','Test1','Test2','Test3']
    # datetime object containing current date and time
    now = datetime.now()
    current_csv_file_name = 'Verify CSV with Emails'
    writeFeilddNames(current_csv_file_name, verify_email_dict.keys())
    print("CSV File created with name", current_csv_file_name)

def verifyEmailAppendLineToCSVFile():
    global verify_email_dict 
    global current_csv_file_name

    print(verify_email_dict)
    # Append a dict as a row in csv file
    append_dict_as_row(current_csv_file_name, verify_email_dict, verify_email_dict.keys())

def verifyEmailResetDictionary():
    global verify_email_dict
    # Need to reset the dictionary values to zero to erase data from the previous board 
    verify_email_dict = {x: 0 for x in verify_email_dict}
    print(verify_email_dict)



def openAndReadCSVforCyrpSerialNumbers():
    file = easygui.fileopenbox()
    print('here')
    crypSerialIDCSVArray = {}

    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                line_count += 1
            # print(f'\t{row["cryp serial"]}')
            crypSerialIDCSVArray[line_count] = row["cryp serial"] 
            line_count += 1
        print(f'Processed {line_count} lines.')
        # print(crypSerialIDCSVArray) 
        return crypSerialIDCSVArray


def clean(text):
    # clean text for creating a folder
    return "".join(c if c.isalnum() else "_" for c in text)

def fetchEmails(numberOfMostResentEmails):
    # number of top emails to fetch
    N = numberOfMostResentEmails
    crypSerialIDEmailArray = {} #  [0]*N

    # create an IMAP4 class with SSL, use your email provider's IMAP server
    imap = imaplib.IMAP4_SSL("imap.gmail.com")
    # authenticate
    imap.login(username, password)

    # select a mailbox (in this case, the inbox mailbox)
    # use imap.list() to get the list of mailboxes
    status, messages = imap.select("INBOX")
    print('Emails Fetched')
    # total number of emails
    messages = int(messages[0])
    # print(messages)
    index = 0 
    for i in range(messages, messages-N, -1): 
    # for i in range(1, messages, +1):
        # fetch the email message by ID
        res, msg = imap.fetch(str(i), "(RFC822)")
        for response in msg:
            if isinstance(response, tuple):
                # parse a bytes email into a message object
                msg = email.message_from_bytes(response[1])
                # decode the email subject
                subject, encoding = decode_header(msg["Subject"])[0]
                if isinstance(subject, bytes):
                    # if it's a bytes, decode to str
                    subject = subject.decode(encoding)
                # decode email sender
                From, encoding = decode_header(msg.get("From"))[0]
                if isinstance(From, bytes):
                    From = From.decode(encoding)
                # print("Subject:", subject)
                # print("From:", From)
                # if the email message is multipart
                if msg.is_multipart():
                    # Currently the message is not multipart so this code can not be tested 
                    print('This was not a valid email ')

                    # # iterate over email parts
                    # for part in msg.walk():
                    #     # extract content type of email
                    #     content_type = part.get_content_type()
                    #     content_disposition = str(part.get("Content-Disposition"))
                    #     try:
                    #         # get the email body
                    #         body = part.get_payload(decode=True).decode()
                    #     except:
                    #         pass
                    #     if content_type == "text/plain" and "attachment" not in content_disposition:
                    #         # print text/plain emails and skip attachments
                    #         # print(body)
                    #         if '{"' in body: 
                    #             print('!!!!!!!!!!!!!!!!!!!!')
                    #     elif "attachment" in content_disposition:
                    #         # download attachment
                    #         filename = part.get_filename()
                    #         if filename:
                    #             folder_name = clean(subject)
                    #             if not os.path.isdir(folder_name):
                    #                 # make a folder for this email (named after the subject)
                    #                 os.mkdir(folder_name)
                    #             filepath = os.path.join(folder_name, filename)
                    #             # download attachment and save it
                    #             open(filepath, "wb").write(part.get_payload(decode=True))
                else:
                    # extract content type of email
                    content_type = msg.get_content_type()
                    # get the email body
                    body = msg.get_payload(decode=True).decode()
                    if content_type == "text/plain":
                        # print only text email parts
                        # print(body)
                        if '{"' in body: 
                                stringlistSplitStart = str(body).split('{"')
                                stringlistSplitEnd = stringlistSplitStart[1].split('"}')
                                emailText = stringlistSplitEnd[0]
                                emailTextList = emailText.split('"')
                                crypSerialID = emailTextList[4]
                                print('Message number:' + str(i) + ' CrypID = '+crypSerialID)
                                crypSerialIDEmailArray[index]= crypSerialID
                                # print(crypSerialIDEmailArray)
                                index = index + 1
                                
                # if content_type == "text/html":
                #     # if it's HTML, create a new HTML file and open it in browser
                #     folder_name = clean(subject)
                #     if not os.path.isdir(folder_name):
                #         # make a folder for this email (named after the subject)
                #         os.mkdir(folder_name)
                #     filename = "index.html"
                #     filepath = os.path.join(folder_name, filename)
                #     # write the file
                #     open(filepath, "w").write(body)
                #     # open in the default browser
                #     webbrowser.open(filepath)
                # print("="*100)
                # print("@"*100)
    # close the connection and logout
    imap.close()
    imap.logout()
    return crypSerialIDEmailArray

# def verifyAllCrypSeials(numberOfEmailsToFetch):
#     # Fetch and process Emails into Array
#     crypSerialIDEmailArray = fetchEmails(numberOfEmailsToFetch)

#     file = easygui.fileopenbox()
#     print('here')
#     crypSerialIDCSVArray = {}

#     with open(file, mode='r') as csv_file:
#         csv_reader = csv.DictReader(csv_file)
#         line_count = 0
#         for row in csv_reader:
#             if line_count == 0:
#                 # print(f'Column names are {", ".join(row)}')
#                 line_count += 1
#             # print(f'\t{row["cryp serial"]}')
#             crypSerialIDCSVArray[line_count] = row["cryp serial"] 
#             line_count += 1
#         print(f'Processed {line_count} lines.')
#         # print(crypSerialIDCSVArray) 
#         return crypSerialIDCSVArray

# def openAndReadCSV():
#     global csv_reader
#     file = easygui.fileopenbox()
#     print('here')
#     crypSerialIDCSVArray = {}

#     with open(file, mode='r') as csv_file:
#         csv_reader = csv.DictReader(csv_file)

# def readCyrpSerialNumbersCSV():
#     global csv_reader
#     line_count = 0
#     for row in csv_reader:
#         if line_count == 0:
#             # print(f'Column names are {", ".join(row)}')
#             line_count += 1
#         # print(f'\t{row["cryp serial"]}')
#         crypSerialIDCSVArray[line_count] = row["cryp serial"] 
#         line_count += 1
#     print(f'Processed {line_count} lines.')
#     # print(crypSerialIDCSVArray) 
#     return crypSerialIDCSVArray

    


def verifyAllCrypSeials(numberOfEmailsToFetch):
    global verify_email_dict
    global verify_email_dictArray
    global current_csv_file_name

    verify_email_dictArray = {}
    # Fetch and process Emails into Array
    crypSerialIDEmailArray = fetchEmails(numberOfEmailsToFetch)

    file = easygui.fileopenbox()
    print('here')

    with open(file, mode='r') as csv_file:
        csv_reader = csv.DictReader(csv_file)
        line_count = 0
        for row in csv_reader:
            if line_count == 0:
                # print(f'Column names are {", ".join(row)}')
                line_count += 1
            print(row["cryp serial"])
        
            verify_email_dict["Start Date Time"] = row["Start Date Time"]
            verify_email_dict['QR Code'] = row["QR Code"] 
            verify_email_dict['conn iccid'] = row["conn iccid"]
            verify_email_dict['conn imei'] = row["conn imei"]
            verify_email_dict['cryp serial'] = row["cryp serial"]
            verify_email_dict['Verified Email Recieved'] = 'Fail - Email Not Verified'

            if line_count == 1: 
                verifyEmailCreateNewCSVFilewithHeaders()

            for i in range(len(crypSerialIDEmailArray)):
                if row["cryp serial"] == crypSerialIDEmailArray[i]:
                    print('Match for ID ')
                    verify_email_dict['Verified Email Recieved'] = 'Success - Email Verified'
                    break
            verifyEmailAppendLineToCSVFile()
            verify_email_dictArray[line_count] = verify_email_dict
                
            line_count += 1
        print(f'Processed {line_count} lines.')
         
        print(verify_email_dictArray) 
        print('Here') 
        print(verify_email_dictArray[1]['cryp serial']) 


    
def verifyEmail_EnterQRCode():
    global verify_email_dictArray
    # qrCodeInput, ok = QInputDialog.getText(userInputWindow, 'Text Input Dialog', 'Please Scan the QR code - 扫 QR码')
    # if ok:
    #     if len(str(qrCodeInput))==6 :
    #         print('QR Code: ', qrCodeInput)
    #         row_dict['QR Code']= str(qrCodeInput)
    #     else:
    #         print('QR Code is the wrong length')
    #         enterQRCode()
    qrCodeInput = '123456' 
    for x in range (len(verify_email_dictArray)):
        if qrCodeInput == verify_email_dictArray[x+1]['QR Code']:
            print ('QR Code ' + verify_email_dictArray[x+1]['QR Code'] + '  '+verify_email_dictArray[x+1]['Verified Email Recieved'])
        

verifyAllCrypSeials(10)
verifyEmail_EnterQRCode()
    
    

    



# verifyEmailCreateNewCSVFilewithHeaders()


# openAndReadCSV()
# crypSerialIDCSVArray = readCyrpSerialNumbersCSV()

# print(crypSerialIDCSVArray)
# # print(crypSerialIDEmailArray)