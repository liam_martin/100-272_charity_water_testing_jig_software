# from PyQt5 import QtCore
from PySide2.QtCore import *
from PySide2.QtGui import *
# import cv2 # OpenCV
import qimage2ndarray # for a memory leak,see gist
import sys # for exiting
from PySide2.QtWidgets import *


from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtCore import QSize

import pandas
import serial
import time

# Determining Paths for Menu Images______________
piOrLaptop = 0 # 0 = Laptop , 1 = Pi !!!!!!!!!!!!!!!!! Crutial to change to get to work on laptop or pi! 


# Fixed Parameters ______________________________
white = (244,244,244)
red = (227,6,19)
yellow = ( 237, 255, 0)
green = ( 0,150 ,64 )
blue = (0,159  ,277 )
purple = ( 102,36,131 )
black= (0,0,0)

windowTextColour = QColor(244,244,244)
windowBackgroundColour = QColor(0,0,0) 

if(piOrLaptop == 0):
    path_quit = "images/quit.png"
if(piOrLaptop == 1):
    path_quit = "/home/pi/..../images/quit.png"

def createPictureLabel(holdingText , imagePathName, sizeX , sizeY):
    labelName = QLabel("holdingText")
    pixmap = QPixmap(imagePathName)
    #  get label dimensions
    w = sizeX #settingsMenuLabel.width()
    h = sizeY #settingsMenuLabel.height()
    #  set a scaled pixmap to a w x h window keeping its aspect ratio 
    if(sizeX==0 and sizeY ==0):
        labelName.setPixmap(pixmap)
    else:
        labelName.setPixmap(pixmap.scaled(w,h))
    return labelName

def createPictureButton(functionName, imagePathName, sizeX , sizeY):
    buttonName = QPushButton("")
    buttonName.clicked.connect(functionName)
    # Set image in Push Button
    pixmap = QPixmap(imagePathName)
    buttonName_icon = QIcon(pixmap)
    buttonName.setIcon(buttonName_icon)
    buttonName.setIconSize(QSize(sizeX,sizeY))
    buttonName.setFlat(1)
    buttonName.setStyleSheet("QPushButton" "{" "background-color : rgb(0, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 
    return buttonName

@Slot()
def quitFunction(): 
    sys.exit()

app = QApplication([])

# Quit Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1,0, 1, 50*1, 0, windowBackgroundColour ,windowTextColour)


# Quit Button
quitQuitbutton = QPushButton("Quitter")
quitQuitbutton.clicked.connect(sys.exit) # quiter button 
quitQuitbutton = createPictureButton(quitFunction, path_quit, 50  , 50 )#Added to push video to right

label = QLabel('No Camera Feed')

layout_quitWindow = QGridLayout()
layout_quitWindow.addWidget(label, 0 ,0)
layout_quitWindow.addWidget(quitQuitbutton, 1 ,0)
quitWindow.setLayout(layout_quitWindow)

# Main Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1,0, 1, 50*1, 0, windowBackgroundColour ,windowTextColour)

qr = mainWindow.frameGeometry()
mainWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()


# mainWindow Assets____________________________
label = QLabel('No Camera Feed')

# Quit Button
mainQuitbutton = QPushButton("Quitter")
mainQuitbutton.clicked.connect(sys.exit) # quiter button 

# button_blank = createPictureButton(switchMode, path_blank, 100 , 100 )#Added to push video to right

layout_mainWindow = QGridLayout()
# layout_mainWindow.addWidget(button_blank , 0, 0 )
layout_mainWindow.addWidget(label, 0, 1 , 5, 1 )#To push buttons to top set image collum spacing to number of buttons + 1 

mainWindow.setLayout(layout_mainWindow)


quitWindow.show()
app.exec_()

