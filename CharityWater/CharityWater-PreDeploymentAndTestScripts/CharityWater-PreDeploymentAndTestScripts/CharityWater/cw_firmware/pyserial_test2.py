import serial
import time
import re

# You need to connect these devices to the pi in the right order so the USB port numbers match 
# use ls /dev to find usb ports
# Connect them in this order to make them appear correctly 
port_AM_CLI = '/dev/ttyUSB0'
port_SSM_LOG = '/dev/ttyUSB1'
port_AM_LOG = '/dev/ttyUSB2'


# serialSpeed = 115200 # ~960 bytes per second, a good default
# txString = b'app mode test\r\n'


def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
                print( "[*] Write Complete" ) ;
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

def readForeverSerial(serialPort):
    rxString = b''
    try:
        while True:
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            print(rxString)
            # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

# # To do add some time out functionality so if it doesn't get the responce it can log it and move on 
# def readUnitlSerial(serialPort ,expectedResponse):
#     rxString = b'xxxxxxxx'
#     try:
#         while rxString != expectedResponse :
#             rxString = serialPort.readline() 
#             rxString = rxString.rstrip()
#             print(rxString)
#             # ...
#     except OSError:
#         print( "ERROR:  on %s - exiting!" % serialPort )
#         exit( 1 )

# def runTest(txSerialPort, rxSerialPort, command, expectedResponse):
#     writeSerial(txSerialPort, command)
#     readUnitlSerial(rxSerialPort ,expectedResponse)

# To do add some time out functionality so if it doesn't get the responce it can log it and move on 
def readUnitlSerial_SSM(serialPort ,expectedResponse):
    rxString = b'xxxxxxxx'
    try:
        while rxString != expectedResponse :
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

    print('SSM Test complete:', expectedResponse)

def runTest(txSerialPort, rxSerialPort, command, expectedResponse):
    writeSerial(txSerialPort, command)
    readUnitlSerial(rxSerialPort ,expectedResponse)

# To do add some time out functionality so if it doesn't get the responce it can log it and move on 
def readUnitlSerial_AM(serialPort ,expectedResponse):
    rxString = b'xxxxxxxx'
    response = b'xxxxxxxx'
    try:
        while response != expectedResponse :
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            # print(response)
            # print(expectedResponse)

    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

    print(' AM Test complete:', expectedResponse)

def runTest_SSM(command, expectedResponse):
    writeSerial(SSM_LOG, command)
    readUnitlSerial_SSM(SSM_LOG,expectedResponse)

def runTest_AM(command, expectedResponse):
    writeSerial(AM_CLI, command)
    readUnitlSerial_AM(AM_LOG,expectedResponse)

AM_CLI = connectSerial(port_AM_CLI, 115200)
AM_LOG = connectSerial(port_AM_LOG, 9600) 
SSM_LOG = connectSerial(port_SSM_LOG, 9600) 


# writeSerial(AM_CLI, b'app mode test\r\n')
# readForeverSerial(AM_LOG)
# writeSerial(SSM_LOG, b'log enable\r\n')
# readForeverSerial(SSM_LOG)
runTest_AM( b'app mode test\r\n' , 'Test mode on' )
runTest_SSM( b'log enable\r\n' , b'Enabled logging'  )

runTest_AM( b'nand pattern write\r\n' , 'Done writing pattern' )
runTest_SSM( b'eep pattern write\r\n' , b'Write complete'  )