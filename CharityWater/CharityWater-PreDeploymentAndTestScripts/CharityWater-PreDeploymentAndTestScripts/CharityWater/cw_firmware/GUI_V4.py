


# from PyQt5 import QtCore
from subprocess import run
from PySide2.QtCore import *
from PySide2.QtGui import *
# import cv2 # OpenCV
# import qimage2ndarray # for a memory leak,see gist
import sys # for exiting
from PySide2.QtWidgets import *


from PySide2.QtWidgets import QApplication, QPushButton
from PySide2.QtGui import QPixmap, QIcon
from PySide2.QtCore import QSize

import pandas
import serial
import time
import os

import RPi.GPIO as GPIO
import time

import serial
import time
import re

import multiprocessing

# ___________Setup Serial UART's_______
# You need to connect these devices to the pi in the right order so the USB port numbers match 
# use ls /dev to find usb ports
# Connect them in this order to make them appear correctly 
port_AM_CLI = '/dev/ttyUSB0'
port_AM_LOG = '/dev/ttyUSB1'
port_SSM_LOG = '/dev/ttyUSB2'

# _______________Setup GPIOs_____________
in1 = 16
in2 = 18

GPIO.setmode(GPIO.BOARD)
GPIO.setup(in1, GPIO.OUT)
GPIO.setup(in2, GPIO.OUT)

# Setting GPIO Outputs to True deactiveated the Realys
GPIO.output(in1, True)
GPIO.output(in2, True)

#__________ Fixed GUI Parameters ____________________
white = (244,244,244)
red = (227,6,19)
yellow = ( 237, 255, 0)
green = ( 0,150 ,64 )
blue = (0,159  ,277 )
purple = ( 102,36,131 )
black= (0,0,0)

windowTextColour = QColor(0,0,0) 
windowBackgroundColour = QColor(244,244,244)

# ______Determining Paths for Menu Images______________
piOrLaptop = 0 # 0 = Laptop , 1 = Pi !!!!!!!!!!!!!!!!! Crutial to change to get to work on laptop or pi! 

if(piOrLaptop == 0):
    path_quit = "images/quit.png"
    path_rpd = "images/rpd.png"
if(piOrLaptop == 1):
    path_quit = "/home/pi/..../images/quit.png"
    path_rpd = "/home/pi/..../images/quit.png"

# ________GUI Functions_________

def createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour ):
    window = QWidget()
    if(isFrameless == 1 and isStaysOnTop == 0  ):
        window.setWindowFlags(Qt.FramelessWindowHint)
    elif(isFrameless == 0 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.WindowStaysOnTopHint)
    elif(isFrameless == 1 and isStaysOnTop == 1  ):
        window.setWindowFlags(Qt.FramelessWindowHint | Qt.WindowStaysOnTopHint)

    if(isTranslucent == 1  ):
        window.setAttribute(Qt.WA_TranslucentBackground)

    if(isMaximised == 1):
        window.showMaximized()
    elif(isMaximised== 0 ):
        qr = window.frameGeometry()
        window.move(qr.topLeft().x()+ xDistance, qr.topLeft().y()+ yDistance)
    
    # Change background colour 
    palette = window.palette()
    palette.setColor(QPalette.Window, backgroundColour)
    palette.setColor(QPalette.WindowText, textColour)
    window.setPalette(palette)
    window.setAutoFillBackground(True)

    if(useStyleSheet == 1 ):
        window.setStyleSheet(
            "QWidget" 
            "{" 
            "border: 100px white;"
            "border-radius: 10px;"
            "background-color: black"
            "}"
        )

    # # Change background colour 
    # palette = window.palette()
    # palette.setColor(QPalette.Window, QColor(0, 0, 0))
    # palette.setColor(QPalette.WindowText, QColor(240, 240, 240))
    # window.setPalette(palette)
    # window.setAutoFillBackground(True)

    return window

def createPictureLabel(holdingText , imagePathName, sizeX , sizeY):
    labelName = QLabel("holdingText")
    pixmap = QPixmap(imagePathName)
    #  get label dimensions
    w = sizeX #settingsMenuLabel.width()
    h = sizeY #settingsMenuLabel.height()
    #  set a scaled pixmap to a w x h window keeping its aspect ratio 
    if(sizeX==0 and sizeY ==0):
        labelName.setPixmap(pixmap)
    else:
        labelName.setPixmap(pixmap.scaled(w,h))
    return labelName


def createPictureButton(functionName, imagePathName, sizeX , sizeY):
    buttonName = QPushButton("")
    buttonName.clicked.connect(functionName)
    # Set image in Push Button
    pixmap = QPixmap(imagePathName)
    buttonName_icon = QIcon(pixmap)
    buttonName.setIcon(buttonName_icon)
    buttonName.setIconSize(QSize(sizeX,sizeY))
    buttonName.setFlat(1)
    buttonName.setStyleSheet("QPushButton" "{" "background-color : rgb(0, 0, 0);" "}" "QPushButton::pressed" "{" "background-color : rgb(0, 0, 0);" "}" ) 
    return buttonName

# _________Serial Functions_____________
def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
            # print( "Write Complete" )
             print( "" )
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

def readForeverSerial(serialPort):
    rxString = b''
    try:
        while True:
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

def readUnitlSerial_SSM(serialPort ,expectedResponse):
    rxString = b'xxxxxxxx'
    try:
        while rxString != expectedResponse :
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            # print(rxString)
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

    # print('!!!!! SSM Test complete:', expectedResponse)

def readUnitlSerial_AM(serialPort ,expectedResponse):
    rxString = b'xxxxxxxx'
    response = b'xxxxxxxx'
    try:
        while response != expectedResponse :
            rxString = serialPort.readline() 
            # print(rxString)
            rxString = rxString.rstrip()
            # print(rxString)
            # We need the result after the final ':' and need to remove trailing "'"
            stringlist = str(rxString).split(":")
            response = (stringlist[len(stringlist)-1]).replace("'", "")
            # print(response)
            # print(expectedResponse)

    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )

    # print('!!!!!!! AM Test complete:', expectedResponse)

# ______ Serial Test Functions with timeout _________
def runTest_SSM(command, expectedResponse):
    writeSerial(SSM_LOG, command)
    p = multiprocessing.Process(target=readUnitlSerial_SSM, args=(SSM_LOG,expectedResponse))
    p.start()
    p.join(10) #will time out after 5 seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('Test Timed Out SSM command', command)
    else:
        print('Sucess for SSM command:', command)


def runTest_AM(command, expectedResponse):
    writeSerial(AM_CLI, command)
    # print('Check it gets here')
    p = multiprocessing.Process(target=readUnitlSerial_AM, args=(AM_LOG,expectedResponse))
    p.start()
    p.join(10) #will time out after 5 seconds
    # If thread is still active
    if p.is_alive():
        # print ("Test is still running - kill it...")
        # Terminate - may not work if process is stuck for good
        p.terminate()
        # OR Kill - will work for sure, no chance for process to finish nicely however
        # p.kill()
        print('Test Timed Out AM command', command)
    else:
        print('Sucess for AM command:', command)


# ___________Action Functions___________

def programBoard(): 
    test = os.system("sudo openocd")#change the contents of openocd.cfg to change the files to be flashed
    print("it ran")
    print("Please wait approx 5 mins")
    readUnitlSerial_AM(AM_LOG ,'Programmed SSM with image. Good to go')


def powerCycleBoard():
    GPIO.output(in1, False)
    print("board power off - check relay is activate")
    time.sleep(2)
    GPIO.output(in1, True)

def connectShort():
    GPIO.output(in2, False)


def disconnectShort():
    GPIO.output(in2, True)

# _________Serial Tests_______________
def enableTestModeAndLog():
    runTest_AM( b'app mode test\r\n' , 'Test mode on' )
    runTest_SSM( b'log enable\r\n' , b'Enabled logging'  )

def runAllSerialTests():
    # Flash
    runTest_AM( b'nand pattern write\r\n' , 'Done writing pattern' )
    runTest_AM( b'nand pattern read\r\n' , 'Pass' )

    # EEPROM
    runTest_SSM( b'eep pattern write\r\n' , b'Write complete')
    runTest_SSM( b'eep pattern read\r\n' , b'Pass')

    # Fule Gauge
    # runTest_SSM( b'batt serial\r\n' , b'XXXXX'  ) #0x3639A975030000EC

    # Battery Voltage Measurement 
    # runTest_SSM( b'batt read\r\n' , b'XXXXX'  ) #Current Batt Voltage: 3625 mV

    # RTC 
    # runTest_SSM( b'rtc st 0 0 0 1 1 1 1 2021\r\n' ,b'HW_RTC: Time set'  ) # Need to create the correct date and time roughly
    # runTest_SSM( b'rtc rt\r\n' , b'xxxx'  ) #HW_RTC Time: Sunday, January 1, 2021 - 01:01:53.15
    
    # Cellular Modem
    runTest_AM( b'conn on primary\r\n' , 'primary antenna' ) #seems to cause a restart
    runTest_AM( b'conn on secondary\r\n' , 'secondary antenna' )
    # runTest_AM( b'conn iccid\r\n' , 'XXXXX' ) # error no sim inserted
    # runTest_AM( b'conn imei\r\n' , 'XXXXX' )# Found IMEI: 358887095625834
    runTest_AM( b'conn version\r\n' , '23.60' )# Needs to match the version number of the modem 
    runTest_AM( b'conn tx 9612 22\r\n' , '+UTEST: 9612,22,5,1,0' )# There are other options we may need to try
    runTest_AM( b'conn off\r\n' , 'Turned off modem' )

    runTest_AM( b'gps on\r\n' , 'Gps is ON - stay tuned for location info' )
    runTest_AM( b'gps print\r\n' , 'Received GPGGA message from GPS' )
    runTest_AM( b'gps off\r\n' , 'Gps is OFF' )

    runTest_AM( b'ssm status\r\n' , 'SSM-AM SPI test pass' )

    runTest_SSM( b'env sample\r\n' , b'XXXX'  ) # HW_ENV: Temperature: 27øC /new line/ HW_ENV: Humidity: 52%

    runTest_SSM( b'mag on\r\n' , b'Magnetometer is on'  )
    runTest_SSM( b'mag sample\r\n' , b'XXXX'  ) #MAG X: -233, MAG Y: -89, MAG Z: 537 MAG TEMP: 25 C
    runTest_SSM( b'algo rawdata \r\n' , b'xxxx'  )
    # Pad 1:   692
    # Pad 2:   695
    # Pad 3:   701
    # Pad 4:   691
    # Pad 5:   687
    # Pad 6:   679
    # Pad 7:   671
    # Pad 8:   692


    runTest_AM( b'cryp serial\r\n' , 'XXXX' )# Crypto ID: 01235d52e7fe319301

    runTest_SSM( b'kick off\r\n' , b'Disabling wd'  ) #Verify board reboots in 2 seconds by watching the AM logging terminal (read 'CLI Initialized' on AM CLI)
    
    #____Final Tests_____________
    # Power cycle the board now
    runTest_AM( b'app waitTime 30\r\n' , 'Setting wait time in seconds to 300' )
    runTest_AM( b'app mfgComplete\r\n' , 'Set manufacturing complete & starting timer' )
    

# __________Slots for GUI______________

@Slot()
def programBoardSlot(): 
    programBoard()

@Slot()
def runAllSerialTestsSlot(): 
    runAllSerialTests()

@Slot()
def powerCycleBoardSlot():
    powerCycleBoard()
    

@Slot()
def connectShortSlot():
    connectShort()

@Slot()
def disconnectShortSlot():
    disconnectShort()

@Slot()
def quitSlot(): 
    print("Quitting")
    GPIO.cleanup()
    sys.exit()

@Slot()
def enableTestModeAndLogSlot():
    enableTestModeAndLog() 

    

# Create QApplication___________________________________________
app = QApplication([])

# # Quit Window ________________________________
# # Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
# quitWindow = createWindow(0, 0 , 1,1, 0, 150, 400, windowBackgroundColour ,windowTextColour)

# # Quit Button
# # quitQuitbutton = QPushButton("Quitter")
# # quitQuitbutton.clicked.connect(sys.exit) # quiter button 

# quitQuitbutton = createPictureButton(quitFunction, path_quit, 50  , 50 )#Added to push video to right

# layout_quitWindow = QGridLayout()
# layout_quitWindow.addWidget(quitQuitbutton, 0 ,0)
# quitWindow.setLayout(layout_quitWindow)


# Main Window ________________________________
# Useage: createWindow(useStyleSheet, isTranslucent, isFrameless, isStaysOnTop, isMaximised, xDistance, yDistance , backgroundColour , textColour )
mainWindow = createWindow(0, 0 , 1 , 0, 5, 50*1, 0, windowBackgroundColour ,windowTextColour)

qr = mainWindow.frameGeometry()
mainWindow.move(qr.topLeft())
screenHeight= qr.height()
screenWidth= qr.width()

# mainWindow Assets____________________________
# label = QLabel('No Camera Feed')

# Main Window Quit Button
mainQuitButton = QPushButton("Quit Program")
mainQuitButton.clicked.connect(quitSlot) # quiter button 

# RPD Logo
mainWindowRPDLabel = createPictureLabel('RPD International', path_rpd,300 ,100 )

# Main Window Program Button 
mainProgramButton = QPushButton("Program")
mainProgramButton.clicked.connect(programBoardSlot)

# PowerCycle Board Button 
powerCycleBoardButton = QPushButton("Power Cycle Board")
powerCycleBoardButton.clicked.connect(powerCycleBoardSlot)

# Connect Short Button 
connectShortButton = QPushButton("Connect Short")
connectShortButton.clicked.connect(connectShort)

# Connect Short Button 
disconnectShortButton = QPushButton("Disconnect Short")
disconnectShortButton.clicked.connect(disconnectShort)

# Enable Test Mode and Log Button 
enableTestModeAndLogButton = QPushButton("Enable Test Mode and SSM Logging")
enableTestModeAndLogButton.clicked.connect(enableTestModeAndLogSlot)

# Run all Serial Tests Button 

runAllSerialTestsButton = QPushButton("Run all Serial Tests")
runAllSerialTestsButton.clicked.connect(runAllSerialTestsSlot)

layout_mainWindow = QGridLayout()
layout_mainWindow.addWidget(mainWindowRPDLabel, 0, 0  )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(connectShortButton, 1, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(disconnectShortButton, 2, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(powerCycleBoardButton, 3, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(mainProgramButton, 4, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(enableTestModeAndLogButton, 5, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 
layout_mainWindow.addWidget(runAllSerialTestsButton, 6, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 

layout_mainWindow.addWidget(mainQuitButton, 10, 0 )#To push buttons to top set image collum spacing to number of buttons + 1 



mainWindow.setLayout(layout_mainWindow)




# Execute App_____________________________________
AM_CLI = connectSerial(port_AM_CLI, 115200)
AM_LOG = connectSerial(port_AM_LOG, 9600) 
SSM_LOG = connectSerial(port_SSM_LOG, 9600) 

mainWindow.show()
# quitWindow.show()
# mainMenuWindow.show()

# window_settingsMenu.show()
app.exec_()