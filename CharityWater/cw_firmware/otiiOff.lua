otii.clear()

local args = otii.get_args()
assert(#args == 2, "Missing filename argument")
local filename = args[2]

local devices = otii.get_devices("Arc")
assert(#devices > 0, "No available devices")
local box = otii.open_device(devices[1].id)
assert(box ~= nil, "No available otii")

local file = io.open(filename, "w")
assert(file ~= nil, "Cannot create " .. filename)

local project = otii.create_project()
assert(project ~= nil, "Cannot create project")

box:set_main_voltage(0)
box:set_range("low")
box:enable_channel("mc", true)
project:enable_main_power(true)

project:start()
otii.msleep(1000)
project:stop()

-- Turn on the otti off, this is so the board can reset if the power value was wrong so we can try again 
-- box:set_main_voltage(0)
project:enable_main_power(false)

local recording_id = project:get_last_recording_id()
local count = project:get_channel_count(box, recording_id, "mc")
local data = project:get_channel_data(box, recording_id, "mc", 1, count)
file:write("timestamp,mc\n")
count = 0 
sum = 0 
for _, item in ipairs(data) do
    file:write(item.timestamp)
    file:write(",")
    file:write(item.sample)
    file:write("\n")
    count = count + 1 
    sum = sum + item.sample
end
result = sum/count 
file:close()

project:close()
box:close()
x = 10 
print(result)
