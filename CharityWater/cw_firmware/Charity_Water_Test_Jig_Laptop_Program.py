import os
import serial
import time
COM_PI_PORT = 'COM10'

#  _________Serial Functions_____________
# Will connect a serial port at a given baude Speed 
def connectSerial(serialPort, serialSpeed):
    try:
        serialObj = serial.Serial( 
            serialPort,
            baudrate = serialSpeed,
            bytesize=8,
            parity = 'N',
            stopbits = 1,
            timeout=5
        ) 
    except OSError:
        print( "ERROR: Could not set up serial connection on %s - exiting!" % serialPort )
        exit( 1 )

    print ( "Connected to %s at %i baud. Hit CTRL+C to exit at any time ..." % ( serialPort, serialSpeed ) )

    return serialObj

# Will write a txString to a serial port 
def writeSerial(serialPort, txString):
    try:
        if serialPort.write( txString ):
            print( "Write Complete" )
            print( "" )
        # while True:
        #     rxString = serialPort.readline() 
        #     rxString = rxString.rstrip()
        #     print(rxString)
        #     # ...
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# Will read the Serial port until the expected response is found 
def readUnitlSerial_otii(serialPort ,expectedResponse):
    rxString = b'xxxxyyyyxxxx' # needs to be set to something the board will never print out can't be blank "")
    counter = 0 
    isFound = 0
    try:
        while (isFound == 0):
            rxString = serialPort.readline() 
            rxString = rxString.rstrip()
            print(rxString)
            if(rxString != ""):
                if expectedResponse in rxString: 
                    isFound =1
                    return rxString
                else:
                    isFound = 0 
            
    except OSError:
        print( "ERROR:  on %s - exiting!" % serialPort )
        exit( 1 )


# _________________________________________________________


try:
    COM_PI_SERIAL = connectSerial(COM_PI_PORT, 9600)
except:
    print('Could not find serial connections')
    # To do - Force the user to restart the app after connecting the serial connections properly 
command = b'Power Cycling Board\r\n'

while(1):
    rxString = readUnitlSerial_otii(COM_PI_SERIAL, b'run')
    action_list = str(rxString).split("=")
    action = action_list[1].replace(" ", "").replace("'", "")
    print('action string = ', action)
    if(action == 'otiiTest'):
        print('Otti Test Running')
        result =os.popen('otiicli cl.lua data.csv').read()
        resultString = ("average current (A) =" + result)
        print(resultString)
        writeSerial(COM_PI_SERIAL, resultString.encode())
    if(action == 'otiiOff'):
        result =os.popen('otiicli otiiOff.lua data.csv').read()
        print('off')
    if(action == 'otiiOn'):
        result =os.popen('otiicli otiiOn.lua data.csv').read()
        print('on')


# # result = os.system("otiicli cl.lua data5.csv")
# result =os.popen('otiicli cl.lua data5.csv').read()
# print("result", result)

# import serial.tools.list_ports as port_list
# ports = list(port_list.comports())
# for p in ports:
#     print (p)


# import serial

# s = serial.Serial('COM3')
# res = s.read()
# print(res)